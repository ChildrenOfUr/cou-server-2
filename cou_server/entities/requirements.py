import dataclasses

from cou_server.common.serializable import serializable


@serializable
@dataclasses.dataclass
class SkillRequirements:
    required_skill_levels: dict[str, int] = dataclasses.field(default_factory=dict)
    error: str = "You don't have the required skill(s)"


@serializable
@dataclasses.dataclass
class ItemRequirements:
    any: list[str] = dataclasses.field(default_factory=list)
    all: dict[str, int] = dataclasses.field(default_factory=dict)
    error: str = "You don't have the required item(s)"


@serializable
@dataclasses.dataclass
class EnergyRequirements:
    energy_amount: int = 0
    error: str = f"You need at least {energy_amount} energy to perform this action"
