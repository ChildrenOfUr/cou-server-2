from datetime import datetime, timedelta, timezone

from sqlalchemy.future import select

from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.database import db_session_context
from cou_server.models.user import User


class Quoin:
    url = "https://childrenofur.com/assets/entityImages/quoin__x1_1_x1_2_x1_3_x1_4_x1_5_x1_6_x1_7_x1_8_png_1354829599.png"

    def __init__(self, id, x, y, type):
        self.id = id
        self.x = x
        self.y = y
        self.type = type
        self.respawn = None
        self.collected = False

    def update(self, simulate_tick=False):
        """Will check for quoin collection/spawn and send updates to clients if needed"""

        if self.respawn is not None and datetime.now(timezone.utc) >= self.respawn:
            self.collected = False

    async def set_collected(self, username):
        async with db_session_context() as db_session:
            user = (await db_session.scalars(select(User).filter(User.username == username))).first()
            if user and user.email is not None:
                await StatManager.add(user.email, Stat.quoins_collected)

        if self.type == "quarazy":
            # Quarazy quoin should never be set to 'collected'
            # to enable all users to collect it
            # it will not be shown to a player again one collected
            # (handled in the client)
            return

        self.collected = True
        duration = 90 if self.type == "mystery" else 30
        self.respawn = datetime.now(timezone.utc) + timedelta(seconds=duration)

    def get_map(self):
        return {
            "id": self.id,
            "url": self.url,
            "type": self.type,
            "remove": str(self.collected),
            "x": self.x,
            "y": self.y,
        }
