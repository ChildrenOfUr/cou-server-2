import random

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.trees.tree import Tree


class GasPlant(Tree):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Gas Plant"
        self.reward_item_type = "general_vapour"

        self.responses = {
            "harvest": [
                "You want gas? Dude, sure.",
                "Always happy to share, friend.",
                "Yeah, harvest away. Gas is a social thing, friend.",
                "Gas? For you? Yeah, man.",
                "You sure that's enough? Come back for a re-up anytime.",
            ],
            "pet": [
                "Awwww yeah.",
                "Hey, do you remember that time when... oh, no, wait, that was Eggy",
                "Petting gives me a sweet, sweet buzz, friend",
                "Good times, man, good times.",
                "Such good energy, man",
                "Oops, pardon me"
            ],
            "water": [
                "Woah. That's wet.",
                "Cool, man. Cool.",
                "Sweet can-tipping, friend.",
                "Ahhhh, that's the stuff, kid.",
                "Woah, man! Water! Like, TOTALLY unexpected.",
            ]
        }

        self.states = {
            "maturity_1" : SpriteSheet("maturity_1", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_1_seed_0_19191191_png_1354830873.png", 828, 1032, 276, 258, 10, False),
            "maturity_2" : SpriteSheet("maturity_2", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_2_seed_0_19191191_png_1354830875.png", 828, 1032, 276, 258, 10, False),
            "maturity_3" : SpriteSheet("maturity_3", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_3_seed_0_19191191_png_1354830877.png", 828, 1032, 276, 258, 10, False),
            "maturity_4" : SpriteSheet("maturity_4", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_4_seed_0_19191191_png_1354830880.png", 828, 1806, 276, 258, 19, False),
            "maturity_5" : SpriteSheet("maturity_5", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_5_seed_0_19191191_png_1354830883.png", 828, 2064, 276, 258, 24, False),
            "maturity_6" : SpriteSheet("maturity_6", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_6_seed_0_19191191_png_1354830888.png", 828, 2838, 276, 258, 31, False),
            "maturity_7" : SpriteSheet("maturity_7", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_7_seed_0_19191191_png_1354830895.png", 828, 3870, 276, 258, 45, False),
            "maturity_8" : SpriteSheet("maturity_8", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_8_seed_0_19191191_png_1354830902.png", 3312, 1032, 276, 258, 47, False),
            "maturity_9" : SpriteSheet("maturity_9", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_9_seed_0_19191191_png_1354830910.png", 3312, 1032, 276, 258, 47, False),
            "maturity_10" : SpriteSheet("maturity_10", "https://childrenofur.com/assets/entityImages/trant_gas__f_cap_10_f_num_10_h_10_m_10_seed_0_19191191_png_1354830919.png", 3864, 1032, 276, 258, 53, False),
        }

        self.maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{self.maturity}")
        self.state = random.randint(0, self.current_state.num_frames)
        self.max_state = self.current_state.num_frames - 1

    async def harvest(self, socket=None, email=None):
        success = await super().harvest(socket=socket, email=email)

        if success:
            harvested = await StatManager.add(email, Stat.gas_harvested)
            if harvested >= 5003:
                await Achievement.find("obsessive_gas_fancier").award_to(email)
            elif harvested >= 1009:
                await Achievement.find("dedicated_gas_fancier").award_to(email)
            elif harvested >= 503:
                await Achievement.find("hobbyist_gas_fancier").award_to(email)
            elif harvested >= 101:
                await Achievement.find("occasional_gas_fancier").award_to(email)

        return success

    async def pet(self, socket=None, email=None):
        success = await super().pet(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.gas_plants_petted)
            if stat >= 41:
                await Achievement.find("bush_whacker").award_to(email)

        return success

    async def water(self, socket=None, email=None):
        success = await super().water(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.gas_plants_watered)
            if stat >= 127:
                await Achievement.find("mayor_of_sprayerville").award_to(email)
            elif stat >= 11:
                await Achievement.find("little_squirt").award_to(email)

        return success
