import random

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.trees.tree import Tree


class FruitTree(Tree):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Fruit Tree"
        self.reward_item_type = "cherry"
        self.responses = {
            "harvest": [
                "Fruity!",
                "Ta-daaaaaaa…",
                "Yaaaaaay!",
                "Frooooot!",
                "C'est la!",
                "Oof. Take this. Heavy.",
            ],
            "pet": [
                "Huh?",
                "Oh.",
                "Whu?",
                "Ah.",
                "Pff.",
                "Together we make a great pear",
            ],
            "water": ["Hm?", "Ahh.", "Glug.", "Mm?", "Shhhlrp."],
        }

        self.states = {
            "maturity_1": SpriteSheet(
                "maturity_1",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_1_seed_0_111119119_png_1354830639.png",
                813,
                996,
                271,
                249,
                10,
                False,
            ),
            "maturity_2": SpriteSheet(
                "maturity_2",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_2_seed_0_111119119_png_1354830641.png",
                813,
                996,
                271,
                249,
                10,
                False,
            ),
            "maturity_3": SpriteSheet(
                "maturity_3",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_3_seed_0_111119119_png_1354830644.png",
                813,
                996,
                217,
                249,
                10,
                False,
            ),
            "maturity_4": SpriteSheet(
                "maturity_4",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_4_seed_0_111119119_png_1354830647.png",
                813,
                1992,
                271,
                249,
                22,
                False,
            ),
            "maturity_5": SpriteSheet(
                "maturity_5",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_5_seed_0_111119119_png_1354830651.png",
                813,
                2739,
                271,
                249,
                33,
                False,
            ),
            "maturity_6": SpriteSheet(
                "maturity_6",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_6_seed_0_111119119_png_1354830658.png",
                813,
                3735,
                271,
                249,
                43,
                False,
            ),
            "maturity_7": SpriteSheet(
                "maturity_7",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_7_seed_0_111119119_png_1354830664.png",
                3523,
                996,
                271,
                249,
                50,
                False,
            ),
            "maturity_8": SpriteSheet(
                "maturity_8",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_8_seed_0_111119119_png_1354830670.png",
                3794,
                996,
                271,
                249,
                53,
                False,
            ),
            "maturity_9": SpriteSheet(
                "maturity_9",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_9_seed_0_111119119_png_1354830677.png",
                4065,
                996,
                271,
                249,
                57,
                False,
            ),
            "maturity_10": SpriteSheet(
                "maturity_10",
                "https://childrenofur.com/assets/entityImages/trant_fruit__f_cap_10_f_num_10_h_10_m_10_seed_0_111119119_png_1354830686.png",
                4065,
                996,
                271,
                249,
                60,
                False,
            ),
        }

        self.maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{self.maturity}")
        self.state = random.randint(0, self.current_state.num_frames)
        self.max_state = self.current_state.num_frames - 1

    async def harvest(self, socket=None, email=None):
        success = await super().harvest(socket=socket, email=email)

        if success:
            harvested = await StatManager.add(email, Stat.cherries_harvested)
            if harvested >= 5003:
                await Achievement.find('president_and_ceo_of_fruit_tree_harvesting_inc').award_to(email)
            elif harvested >= 1009:
                await Achievement.find('overpaid_executive_fruit_tree_harvester').award_to(email)
            elif harvested >= 503:
                await Achievement.find('midmanagement_fruit_tree_harvester').award_to(email)
            elif harvested >= 101:
                await Achievement.find('entrylevel_fruit_tree_harvester').award_to(email)

        return success

    async def pet(self, socket=None, email=None):
        success = await super().pet(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.fruit_trees_petted)
            if stat >= 127:
                await Achievement.find('masterful_fruit_tree_pettifier').award_to(email)
            elif stat >= 41:
                await Achievement.find('betterthanmediocre_fruit_tree_pettifier').award_to(email)
            elif stat >= 11:
                await Achievement.find('newbie_fruit_tree_pettifier').award_to(email)

        return success

    async def water(self, socket=None, email=None):
        success = await super().water(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.fruit_trees_watered)
            if stat >= 127:
                await Achievement.find('super_duper_soaker').award_to(email)
            elif stat >= 41:
                await Achievement.find('super_soaker').award_to(email)
            elif stat >= 11:
                await Achievement.find('ok_soaker').award_to(email)

        return success
