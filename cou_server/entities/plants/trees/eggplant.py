import random

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.trees.tree import Tree


class EggPlant(Tree):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Egg Plant"
        self.reward_item_type = "egg"

        self.responses = {
            "harvest": [
                "This. For you.",
                "We grew this. You take.",
                "This harvest good. Have it.",
                "Ooooof. Take harvest. Heavy.",
                "We made this. You can have.",
            ],
            "pet": [
                "Petting approved.",
                "Think petting good. Builds brain.",
                "Much gooder. Egg Plant grows in body and brain.",
                "Egg plant grows stronger. Cleverer. And eggier.",
                "Yes. Petting makes brain and eggs biggerer.",
            ],
            "water": [
                "Ahhhhh. Better.",
                "Water good. We feel gratitude.",
                "Glug. Thanks.",
                "Yes. Liquid helps make harvests. Good.",
                "Good watering. But we still like petting too, comprende?",
            ]
        }

        self.states = {
            "maturity_1" : SpriteSheet("maturity_1", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_1_seed_0_11191191_png_1354829612.png", 888, 278, 296, 278, 3, False),
            "maturity_2" : SpriteSheet("maturity_2", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_2_seed_0_11191191_png_1354829613.png", 888, 278, 296, 278, 3, False),
            "maturity_3" : SpriteSheet("maturity_3", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_3_seed_0_11191191_png_1354829614.png", 592, 556, 296, 278, 4, False),
            "maturity_4" : SpriteSheet("maturity_4", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_4_seed_0_11191191_png_1354829616.png", 888, 1390, 296, 278, 14, False),
            "maturity_5" : SpriteSheet("maturity_5", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_5_seed_0_11191191_png_1354829618.png", 888, 1946, 296, 278, 19, False),
            "maturity_6" : SpriteSheet("maturity_6", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_6_seed_0_11191191_png_1354829621.png", 888, 2502, 296, 278, 26, False),
            "maturity_7" : SpriteSheet("maturity_7", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_7_seed_0_11191191_png_1354829624.png", 888, 3336, 296, 278, 34, False),
            "maturity_8" : SpriteSheet("maturity_8", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_8_seed_0_11191191_png_1354829628.png", 888, 3336, 296, 278, 34, False),
            "maturity_9" : SpriteSheet("maturity_9", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_9_seed_0_11191191_png_1354829632.png", 3256, 1112, 296, 278, 44, False),
            "maturity_10" : SpriteSheet("maturity_10", "https://childrenofur.com/assets/entityImages/trant_egg__f_cap_10_f_num_10_h_10_m_10_seed_0_11191191_png_1354829638.png", 3256, 1390, 296, 278, 55, False)
        }

        self.maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{self.maturity}")
        self.state = random.randint(0, self.current_state.num_frames - 1)
        self.max_state = self.current_state.num_frames - 1

    async def harvest(self, socket = None, email: str = None) -> bool:
        success = await super().harvest(socket=socket, email=email)

        if success:
            harvested = await StatManager.add(email, Stat.eggs_harveted)
            if harvested >= 5003:
                await Achievement.find("egg_freak").award_to(email)
            elif harvested >= 1009:
                await Achievement.find("egg_aficianado").award_to(email)
            elif harvested >= 503:
                await Achievement.find("egg_poacher").award_to(email)
            elif harvested >= 101:
                await Achievement.find("egg_enthusiast").award_to(email)

        return success

    async def pet(self, socket = None, email: str = None) -> bool:
        success = await super().pet(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.egg_plants_petted)
            if stat >= 127:
                await Achievement.find("super_supreme_egg_plant_coddler").award_to(email)
            elif stat >= 41:
                await Achievement.find("supreme_egg_plant_coddler").award_to(email)
            elif stat >= 11:
                await Achievement.find("egg_plant_coddler").award_to(email)

        return success

    async def water(self, socket = None, email: str = None) -> bool:
        success = await super().water(socket=socket, email=email)

        if success:
            stat = await StatManager.add(email, Stat.egg_plants_watered)
            if stat >= 41:
                await Achievement.find("about_average_irrigationist").award_to(email)

        return success
