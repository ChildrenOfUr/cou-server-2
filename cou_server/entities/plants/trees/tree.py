from datetime import datetime, timezone
import json
import random

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import toast, clamp
from cou_server.endpoints import mapdata, quest, weather
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.npcs.crab import Crab
from cou_server.entities.plants.plant import Plant
from cou_server.entities.requirements import EnergyRequirements, ItemRequirements
from cou_server.quests.messages import RequirementProgress
from cou_server.skills.skill_manager import SkillManager


class Tree(Plant):
    SKILL = "arborology"

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.reward_item_type = None
        self.last_weather_update = datetime.now(timezone.utc)
        self.maturity = None
        item_req = ItemRequirements()
        item_req.any = ["watering_can", "irrigator_9000"]
        item_req.error = (
            "Trees don't like to be peed on. Go find some clean water, please."
        )
        action_defs = {"pet": "petting", "water": "wattering", "harvest": "harvesting"}
        for action_name, action_word in action_defs.items():
            action = Action(actionName=action_name)
            action.actionWord = action_word
            action.description = f"{action_name.title()} this tree"
            action.timeRequired = self.action_time
            action.energyRequirements = EnergyRequirements(energy_amount=2)
            action.associatedSkill = self.SKILL
            self.actions.append(action)

    def restore_state(self, metadata):
        if "maturity" in metadata:
            maturity = json.loads(metadata["maturity"])
            self.set_state(f"maturity_{maturity}")
            self.max_state = self.current_state.num_frames - 1
        if "state" in metadata:
            self.state = json.loads(metadata["state"])

    def get_persist_metadata(self):
        return {"maturity": json.dumps(self.maturity), "state": json.dumps(self.state)}

    def update(self, simulate_tick=False):
        super().update()

        is_raining = weather.is_raining_in(mapdata.get_street_by_name(self.street_name)["tsid"])
        now = datetime.now(timezone.utc)
        if is_raining and (now - self.last_weather_update).total_seconds() > 23:
            # every 23 seconds while it's raining
            self.state = clamp(self.state + 1, 0, self.max_state)
            self.last_weather_update = now

        self.state = clamp(self.state, 0, self.max_state)

    async def harvest(self, socket=None, email=None):
        if self.state == 0:
            return False

        harvest_level = await SkillManager.get_level(self.SKILL, email)
        reward_multiplier = 1
        energy = 5
        img_boost = harvest_level

        if harvest_level == 5:
            reward_multiplier = 4 + (1 if random.randint(0, 1) == 1 else 0)
            img_boost += 1
        elif harvest_level >= 4:
            reward_multiplier = 4 + (1 if random.randint(0, 2) == 1 else 0)
        elif harvest_level >= 3:
            reward_multiplier = 3 + (1 if random.randint(0, 4) == 1 else 0)
        elif harvest_level > 1:
            reward_multiplier = 2

        if harvest_level >= 3:
            energy -= 2

        success = await self.try_set_metabolics(
            email,
            energy=-energy,
            mood=(1 + (reward_multiplier // 2)),
            img_min=(5 + img_boost),
            img_range=5,
        )
        if not success:
            return False

        # say a witty thing
        witty_index = random.randint(0, len(self.responses["harvest"]) - 1)
        await self.say(self.responses["harvest"][witty_index])

        self.state -= 1

        # give the player the 'fruits' of their labor
        await InventoryManager.add_item_to_user(email, cou_globals.items[self.reward_item_type], reward_multiplier, self.id)

        if self.state < 0:
            self.state = 0

        await SkillManager.learn(self.SKILL, email)

        # chance for a musicblock
        if random.randint(0, 15) == 7:
            musicblock = cou_globals.items[Crab.random_music_block()]
            await InventoryManager.add_item_to_user(email, musicblock, 1, self.id)
            await toast(f"You got a <musicblock>!", socket, on_click="iteminfo|<musicblock>")

        return True

    async def water(self, socket=None, email=None):
        # make sure the player has a watering can to water this tree
        water_action = [
            action for action in self.actions if action.actionName == "water"
        ][0]
        types = water_action.itemRequirements.any
        success = await InventoryManager.decrease_durability(email, types)
        if not success:
            return False

        if self.state == self.max_state:
            return False

        success = await self.try_set_metabolics(
            email, energy=-2, mood=2, img_min=3, img_range=2
        )
        if not success:
            return False

        # say a witty thing
        witty_index = random.randint(0, len(self.responses["water"]) - 1)
        await self.say(self.responses["water"][witty_index])

        # award achievements
        stats = await StatManager.get_all(email)
        total_watered = (
            stats[Stat.bean_trees_watered] + stats[Stat.bubble_trees_watered] +
            stats[Stat.egg_plants_watered] + stats[Stat.fruit_trees_watered] +
            stats[Stat.gas_plants_watered] + stats[Stat.spice_plants_watered] +
            stats[Stat.wood_trees_watered]
        )
        if total_watered >= 127:
            await Achievement.find('secondrate_rainmaker').award_to(email)
        elif total_watered >= 41:
            await Achievement.find('firstrate_rainmaker').award_to(email)
        elif total_watered >= 7:
            await Achievement.find('novice_precipator').award_to(email)

        self.state += 1

        await SkillManager.learn(self.SKILL, email)

        if self.state > self.max_state:
            self.state = self.max_state

        return True

    async def pet(self, socket=None, email=None):
        success = await self.try_set_metabolics(
            email, energy=-2, mood=2, img_min=3, img_range=2
        )
        if not success:
            return False

        # say a witty thing
        witty_index = random.randint(0, len(self.responses["pet"]) - 1)
        await self.say(self.responses["pet"][witty_index])

        # award achievements
        stats = await StatManager.get_all(email)
        total_petted = (
            stats[Stat.bean_trees_petted] + stats[Stat.bubble_trees_petted] +
            stats[Stat.egg_plants_petted] + stats[Stat.fruit_trees_petted] +
            stats[Stat.gas_plants_petted] + stats[Stat.spice_plants_petted] +
            stats[Stat.wood_trees_petted]
        )
        if total_petted >= 1009:
            await Achievement.find('finallyprettygood_tree_hugger').award_to(email)
        elif total_petted >= 503:
            await Achievement.find('definitelymuchbetter_tree_hugger').award_to(email)
        elif total_petted >= 283:
            await Achievement.find('extremely_promising_tree_hugger').award_to(email)
        elif total_petted >= 41:
            await Achievement.find('decent_tree_hugger').award_to(email)
        elif total_petted >= 7:
            await Achievement.find('okbutneedsimprovement_tree_hugger').award_to(email)

        if email in quest.quest_log_cache:
            await quest.quest_log_cache[email].offer_quest("Q2")

        MessageBus.publish(
            "requirement_progress", RequirementProgress(f"treePet{self.type}", email)
        )

        await SkillManager.learn(self.SKILL, email)

        return True

    async def customize_actions(self, email):
        arborology_level = await SkillManager.get_level(self.SKILL, email)
        personal_actions = []
        for action in self.actions:
            personal_action = Action.from_json(action.to_json())
            if action.actionName == "harvest":
                if arborology_level > 2:
                    personal_action.energy_requirements = EnergyRequirements(energy_amount=3)
                if self.state <= 0:
                    personal_action.enabled = False
                    personal_action.error = "There's nothing to harvest right now. Try giving me some water."
            if action.actionName == "water":
                if self.state >= self.max_state:
                    personal_action.enabled = False
                    personal_action.error = "I'm not thirsty right now."
                if False:  # TODO: is raining on this street
                    personal_action.enabled = False
                    personal_action.error = (
                        "It's already raining, I don't need any more water."
                    )
            personal_actions.append(personal_action)
        return personal_actions
