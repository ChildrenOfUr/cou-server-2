import random

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.trees.tree import Tree


class PaperTree(Tree):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Paper Tree"
        self.reward_item_type = "paper"

        self.responses = {
            "harvest": [
                "Take these sheets away. / Do with them as you want to. / I cannot use them.",
                "You want some paper?  / Take it then, it's yours to use. / Just don't waste it. Thanks.",
                "Giving you paper.  / I hope that's what you wanted.  / It's all you're getting.",
                "Only a few pieces  / of paper, I will give you.  / You expected more?",
                "Here you are: paper! / You harvest a paper tree…  / What do you expect?",
                "Covered in white stuff.  / I look like I've been TPed  / But no, it's my fruit.",
                "Your perfect harvest.  / Each branch shaken, and at last… / A few clean leaves fall.",
                "My leaves bow to you / My branches offer bounty / And now a leaf falls.",
                "You stretch to harvest  / Pinching, pulling until paf!  / A single leaf falls.",
                "Listen, here's a secret: / These aren't just leaves, they're paper! / For writing and stuff.",
                "Here, kid, is paper / Used for reading, writing, planes, or / Decorating walls.",
            ],
            "pet": [
                "Hugging trees tightly / a trickle of energy / yes, I like that. thanks",
                "Your action suggests / you haven't been at this long / but you're still not bad.",
                "I am Paper Tree / I think I might be useful / But for what? No clue.",
                "Paper trees are good / at making crinkling noises / when you hug their trunks.",
                "I am paper tree / I like it when you hug me hard / but so soon you leave.",
                "This petting pleases. / Are you a tree whisperer? / (If that is a thing…)",
                "This kind attention / Helps paper tree to grow big / We hope you feel proud.",
                "Such polished petting!  / That you took the time for this  / Makes Paper Tree smile.",
                "I like your petting  / You know how to please a tree.  / Not in a weird way.",
                "Didn't see you there / With your soft and kindly hands.  / You can stop now, though.",
            ],
            "water": [
                "That one watering / Can… have such stunning effect? / Hail, tiny raincloud.",
                "Even a trickle / From the right kind of can / Brings life to paper.",
                "Careful where you aim. / I don't want to turn into / Papier-mâché.",
                "Ahh, this welcome rain.  / It falls upon my branches.  / And makes me go \"Squeee!\"",
                "It's very nice, thanks  / That you have taken the time.  / To sprinkle on me.",
                "You made my roots wet.  / It's not that I'm complaining  / I'm just a bit damp.",
                "All this way you came.  / To seek me out and sprinkle.  / I think that you're nice.",
                "Watering paper? / Nice, thanks, but watch out there or / You'll make me soggy.",
                "The gentle patter / Of sprinkled Urland water / Brings joy to my roots.",
                "It's enjoyable, / But watch I don't turn into / Papier Mache.",
            ]
        }

        self.states = {
            "maturity_1" : SpriteSheet("maturity_1", "https://childrenofur.com/assets/entityImages/paper_tree_needs_pet_false_needs_water_false_paper_count_21_x22_1_png_1354832565.png", 928, 1296, 232, 216, 22, False)
        }
        self.maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{self.maturity}")
        self.state = random.randint(0, self.current_state.num_frames)
        self.max_state = self.current_state.num_frames - 1

    async def harvest(self, socket=None, email=None):
        success = await super().harvest(socket=socket, email=email)

        if success:
            harvested = await StatManager.add(email, Stat.paper_harvested)
            if harvested >= 1009:
                await Achievement.find('parchment_purloiner').award_to(email)
            elif harvested >= 503:
                await Achievement.find('pad_pincher').award_to(email)
            elif harvested >= 101:
                await Achievement.find('sheet_snatcher').award_to(email)
            elif harvested >= 73:
                await Achievement.find("paper_plucker").award_to(email)

        return success
