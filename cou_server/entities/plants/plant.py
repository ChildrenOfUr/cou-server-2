from datetime import datetime, timezone

from cou_server.entities.entity import Entity
from cou_server.entities.spritesheet import SpriteSheet


class Plant(Entity):
    """Will check for plant growth/decay and send updates to clients if needed

	The actions map key string should be equivalent to the name of a function
	as it will be dynamically called in street_update_handler when the client
	attempts to perform one of the available actions;
	"""

    def __init__(self, id: str, x: int, y: int, z: int, rotation: int, h_flip: bool, street_name: str):
        super().__init__()
        self.id = id
        self.x = x
        self.y = y
        self.z = z
        self.rotation = rotation
        self.h_flip = h_flip
        self.street_name = street_name
        self.respawn = datetime.now(timezone.utc)
        self.actions = []
        self.action_time = 3000
        self.states: dict[str, SpriteSheet] = None
        self.current_state: SpriteSheet = None
        self.state: int = None
        self.max_state: int = None
        self.type: str = None

    def restore_state(self, metadata):
        if "state" in metadata:
            self.state = int(metadata["state"])

        if "currentState" in metadata:
            self.set_state(metadata["currentState"])

    def get_persist_metadata(self):
        return {"state": str(self.state), "currentState": self.current_state.state_name}

    def update(self, simulate_tick=False):
        pass

    def get_map(self):
        map = super().get_map()
        map["url"] = self.current_state.url
        map["id"] = self.id
        map["type"] = self.type
        map["state"] = self.state
        map["numRows"] = self.current_state.num_rows
        map["numColumns"] = self.current_state.num_cols
        map["numFrames"] = self.current_state.num_frames
        map["actions"] = [action.to_json() for action in self.actions]
        map["x"] = self.x
        map["y"] = self.y
        map["z"] = self.z
        map["rotation"] = self.rotation
        map["h_flip"] = self.h_flip
        return map
