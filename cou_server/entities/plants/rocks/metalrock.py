import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.requirements import ItemRequirements, EnergyRequirements
from cou_server.entities.plants.rocks.rock import Rock
from cou_server.skills.skill_manager import SkillManager


class MetalRock(Rock):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Metal Rock"

        item_requirements = ItemRequirements(
            any=["fancy_pick"],
            error="You need a special pick to mine harder rocks."
        )
        mine_action = next(iter(action for action in self.actions if action.actionName == "mine"))
        mine_action.itemRequirements = item_requirements
        mine_action.energyRequirements = EnergyRequirements(energy_amount=10)

        self.states = {
            "5-4-3-2-1": SpriteSheet("5-4-3-2-1", "https://childrenofur.com/assets/entityImages/rock_metal_x1_5_x1_4_x1_3_x1_2_x1_1__1_png_1354832615.png", 685, 100, 137, 100, 5, False)
        }
        self.set_state("5-4-3-2-1")
        self.state = random.randint(0, self.current_state.num_frames)
        self.responses[f'mine_{self.type}'] = [
            "Slave to the GRIND, kid! ROCK ON!",
            "I'd feel worse if I wasn't under such heavy sedation.",
            "Sweet! Air pickaxe solo! C'MON!",
            "Yeah. Appetite for destruction, man. I feel ya.",
            "LET THERE BE ROCK!",
            "Those who seek true metal, we salute you!",
            "YEAH, man! You SHOOK me!",
            "All hail the mighty metal power of the axe!",
            "Metal, man! METAL!",
            "Wield that axe like a metal-lover, man!",
            "I Wanna Rock!"
        ]

    async def mine(self, socket = None, email: str = None) -> bool:
        success = await super().mine(socket=socket, email=email)

        if success:
            mining_level = await SkillManager.get_level(Rock.SKILL, email)
            qty = 1
            if mining_level == 4:
                qty = 3 if random.randint(0, 3) == 3 else 4
            elif mining_level >= 1:
                qty = 2

            # give the player the "fruits" of their labor
            await InventoryManager.add_item_to_user(email, cou_globals.items["chunk_metal"], qty, self.id)

        return success
