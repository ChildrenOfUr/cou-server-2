import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.rocks.rock import Rock
from cou_server.skills.skill_manager import SkillManager


class DulliteRock(Rock):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Dullite Rock"

        self.states = {
            "5-4-3-2-1": SpriteSheet("5-4-3-2-1", "https://childrenofur.com/assets/entityImages/rock_dullite_x1_5_x1_4_x1_3_x1_2_x1_1__1_png_1354831459.png", 655, 114, 131, 114, 5, False)
        }
        self.set_state("5-4-3-2-1")
        self.state = random.randint(0, self.current_state.num_frames)
        self.responses[f'mine_{self.type}'] = [
            "Ooof. I feel lighter already.",
            "Mmm, thanks, I've been itching there all day.",
            "Ow. Ow-hangover. Ow-my-head. Ow.",
            "Not bad. Work on your backswing.",
            "You're really picking this up.",
            "Nothing wrong with a sedimentary lifestyle, chum.",
            "I should have been a wrestler. I'm rock-hard! Hee!",
            "Ah. You've taken a lode off my mind.",
            "You sure have an apatite for this.",
            "Woah. I'm tuff. But you're tuffer."
        ]

    async def mine(self, socket = None, email: str = None) -> bool:
        success = await super().mine(socket=socket, email=email)

        if success:
            mining_level = await SkillManager.get_level(Rock.SKILL, email)
            qty = 1
            if mining_level == 4:
                qty = 3 if random.randint(0, 3) == 3 else 2
            elif mining_level >= 1:
                qty = 2

            # give the player the "fruits" of their labor
            await InventoryManager.add_item_to_user(email, cou_globals.items["chunk_dullite"], qty, self.id)

        return success
