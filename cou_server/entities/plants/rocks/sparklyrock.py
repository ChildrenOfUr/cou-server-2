import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.rocks.rock import Rock
from cou_server.skills.skill_manager import SkillManager


class SparklyRock(Rock):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Sparkly Rock"

        self.states = {
            "5-4-3-2-1": SpriteSheet("5-4-3-2-1", "https://childrenofur.com/assets/entityImages/rock_sparkly_x1_5_x1_4_x1_3_x1_2_x1_1__1_png_1354831467.png", 655, 127, 131, 127, 5, False)
        }
        self.set_state("5-4-3-2-1")
        self.state = random.randint(0, self.current_state.num_frames)
        self.responses[f'mine_{self.type}'] = [
            "You rock my world!",
            "I've taken a shine to you.",
            "Here! What's mined is yours!",
            "Pick me! Pick me!",
            "I sparkle! You sparkle! Sparkles!",
            "Oooh, you're cute. You into carbon-dating?",
            "Oh yeah! Who's your magma?!?",
            "Yay! You picked me!",
            "Hey, cutestuff! You make me sliver.",
            "You crack me up, Urchin!",
            "Yay! Everything should sparkle! Except maybe vampires.",
            "Together, we'll make the world sparkly, Urling!"
        ]

    async def mine(self, socket = None, email: str = None) -> bool:
        success = await super().mine(socket=socket, email=email)

        if success:
            mining_level = await SkillManager.get_level(Rock.SKILL, email)
            qty = 1
            if mining_level == 4:
                qty = 3 if random.randint(0, 3) == 3 else 2
            elif mining_level >= 1:
                qty = 2

            # give the player the "fruits" of their labor
            await InventoryManager.add_item_to_user(email, cou_globals.items["chunk_sparkly"], qty, self.id)

        return success
