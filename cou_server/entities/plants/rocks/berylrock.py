import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.plants.rocks.rock import Rock
from cou_server.skills.skill_manager import SkillManager


class BerylRock(Rock):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Beryl Rock"

        self.states = {
            "5-4-3-2-1": SpriteSheet("5-4-3-2-1", "https://childrenofur.com/assets/entityImages/rock_beryl_x1_5_x1_4_x1_3_x1_2_x1_1__1_png_1354831451.png", 670, 120, 134, 120, 5, False)
        }
        self.set_state("5-4-3-2-1")
        self.state = random.randint(0, self.current_state.num_frames)
        self.responses[f'mine_{self.type}'] = [
            "Hey! To the left a little next time.",
            "Ughh, you're so frikkin' picky.",
            "I wasn't cut out for this.",
            "Not in the face! Oh. Wait. No face.",
            "If you need any tips on technique, just axe.",
            "Pick on someone else, will you?",
            "You're on rocky ground, Urchin.",
            "I feel like you're taking me for granite.",
            "Well, at least that's a weight off me mined.",
            "You sure have one big axe to grind."
        ]

    async def mine(self, socket = None, email: str = None) -> bool:
        success = await super().mine(socket=socket, email=email)

        if success:
            mining_level = await SkillManager.get_level(Rock.SKILL, email)
            qty = 1
            if mining_level == 4:
                qty = 3 if random.randint(0, 3) == 3 else 2
            elif mining_level >= 1:
                qty = 2

            # give the player the "fruits" of their labor
            await InventoryManager.add_item_to_user(email, cou_globals.items["chunk_beryl"], qty, self.id)

        return success
