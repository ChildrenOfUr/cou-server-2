from datetime import datetime, timedelta, timezone
import random

from cou_server.achievements.stats import Stat, StatManager
from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.util import toast
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.plants.plant import Plant
from cou_server.entities.requirements import EnergyRequirements, ItemRequirements
from cou_server.skills.skill_manager import SkillManager


logger = get_logger(__name__)


class Rock(Plant):
    SKILL = "mining"

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.max_state = 0
        self.action_time = 5000

        item_requirements = ItemRequirements()
        item_requirements.any = ["pick", "fancy_pick"]
        item_requirements.error = "You need some type of pick to mine this."
        self.actions.append(Action(
            actionName="mine",
            actionWord="mining",
            timeRequired=self.action_time,
            itemRequirements=item_requirements,
            energyRequirements=EnergyRequirements(energy_amount=10),
            associatedSkill=self.SKILL,
        ))

        self.responses = {
            "gone": [
                "Oof, where'd I go?",
                "brb",
                "kbye",
                "A la peanut butter sammiches",
                "Alakazam!",
                "*poof*",
                "I'm all mined out!",
                "Gone to the rock quarry in the sky",
                "Yes. You hit rock bottom",
                "All rocked out for now"
            ]
        }

    def update(self, simulate_tick: bool = False):
        now = datetime.now(timezone.utc)

        if self.state >= self.current_state.num_frames:
            self.set_action_enabled("mine", False)

        if self.respawn and now > self.respawn:
            self.state = 0
            self.set_action_enabled("mine", True)
            self.respawn = None

        if self.state < self.max_state:
            self.state = self.max_state

    async def mine(self, socket=None, email: str = None) -> bool:
        if self.state >= self.current_state.num_frames:
            await toast("There's not much left to mine", socket)
            return False

        # make sure the player has a pick that can mine this rock
        dig_action = next(iter(action for action in self.actions if action.actionName == "mine"))
        types = dig_action.itemRequirements.any
        mining_skill_level = await SkillManager.get_level(self.SKILL, email)
        success = await InventoryManager.decrease_durability(email, types)
        if not success:
            return False

        # make sure the player has 10 energy to perform this action
        # if so, allow the action and subtract 10 from their energy

        energy_used = 10
        if mining_skill_level >= 3:
            energy_used //= 4
        elif mining_skill_level > 1:
            energy_used -= 2

        img_reward = 5
        img_min = 10
        if mining_skill_level == 4:
            img_reward *= 2
            img_min *= 2
        elif mining_skill_level >= 3:
            img_reward += 2
            img_min += 2

        success = await self.try_set_metabolics(email, energy=-energy_used, img_min=img_min, img_range=img_reward)
        if not success:
            return False

        # rocks spritesheets go from full to empty which is the opposite of trees
        # so mining the rock will actually increase its state number
        witty_index = random.randint(0, len(self.responses[f"mine_{self.type}"]) - 1)
        await self.say(self.responses[f"mine_{self.type}"][witty_index])

        await StatManager.add(email, Stat.rocks_mined);

        self.state += 1
        if self.state >= self.current_state.num_frames:
            witty_index = random.randint(0, len(self.responses["gone"]) - 1)
            await self.say(self.responses["gone"][witty_index])
            self.respawn = datetime.now(timezone.utc) + timedelta(minutes=2)

        # chances to get gems:
        # amber = 1 in 7
        # sapphire = 1 in 9
        # ruby = 1 in 11
        # moonstone = 1 in 17
        # diamond = 1 in 20

        chance_increaser = 0
        if mining_skill_level == 4:
            chance_increaser = 2
        elif mining_skill_level >= 2:
            chance_increaser = 3

        if random.randint(0, 7 - chance_increaser) == 5 - chance_increaser:
            await InventoryManager.add_item_to_user(email, cou_globals.items["pleasing_amber"], 1, self.id)
        elif random.randint(0, 9 - chance_increaser) == 5 - chance_increaser:
            await InventoryManager.add_item_to_user(email, cou_globals.items["showy_sapphire"], 1, self.id)
        elif random.randint(0, 11 - chance_increaser) == 5 - chance_increaser:
            await InventoryManager.add_item_to_user(email, cou_globals.items["modestly_sized_ruby"], 1, self.id)
        elif random.randint(0, 17 - chance_increaser) == 5 - chance_increaser:
            await InventoryManager.add_item_to_user(email, cou_globals.items["luminous_moonstone"], 1, self.id)
        elif random.randint(0, 20 - chance_increaser) == 5 - chance_increaser:
            await InventoryManager.add_item_to_user(email, cou_globals.items["walloping_big_diamond"], 1, self.id)

        # award skill points
        await SkillManager.learn(self.SKILL, email)

        return True
