from datetime import datetime, timedelta, timezone

from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.plants.plant import Plant


_DATE_FORMAT = "%Y-%m-%d %H:%M:%S.%f%z"


class RespawningItem(Plant):
    def __init__(self, id: str, x: int, y: int, z: int, rotation: int, h_flip: bool, street_name: str):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.item_type: str = None
        self.respawn_time: datetime = None
        self.actions.append(Action(actionName="pick up", actionWord="picking up", description="Take it for yourself"))
        self.state = 0
        self.max_state = 0

    @property
    def hidden(self) -> bool:
        return self.respawn != None

    def update(self, simulate_tick: bool = False) -> None:
        if self.hidden and self.respawn < datetime.now(timezone.utc):
            # respawn now
            self.show()
            self.respawn = None

    def get_persist_metadata(self):
        return super().get_persist_metadata() | {"respawn": datetime.strftime(self.respawn, _DATE_FORMAT)}

    def restore_state(self, metadata):
        super().restore_state(metadata)

        try:
            self.respawn = datetime.strptime(metadata["respawn"], _DATE_FORMAT)
        except Exception:
            self.respawn = datetime.now(timezone.utc)

    def show(self) -> None:
        self.state = 0
        self.set_action_enabled("pick up", True)

    def hide(self, respawn_time: timedelta = None) -> None:
        # default to the class-defined respawn time if one is not set here
        if respawn_time is None and self.respawn_time is not None:
            respawn_time = self.respawn_time

        self.state = self.max_state + 1
        self.set_action_enabled("pick up", False)

        if respawn_time is not None:
            self.respawn = datetime.now(timezone.utc) + respawn_time

    async def pickUp(self, socket = None, email: str = None) -> None:
        added = await InventoryManager.add_item_to_user(email, cou_globals.items[self.item_type], 1)
        if added == 1:
            self.hide(timedelta(minutes=1))
            return True
        return False
