from datetime import timedelta

from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.util import toast
from cou_server.entities.action import Action
from cou_server.entities.plants.respawning_items.respawning_item import RespawningItem
from cou_server.entities.spritesheet import SpriteSheet


class HellGrapes(RespawningItem):
    ENERGY_AWARD = 3
    ENERGY_REQ = 9

    def __init__(self, id: str, x: int, y: int, z: int, rotation: int, h_flip: bool, street_name: str):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Hellish Grapes"
        self.item_type = "bunch_of_grapes"
        self.action_time = 3000

        self.actions.append(Action(actionName="squish", actionWord="squishing", description="You have to work to get out."))
        self.states = {
            "1-2-3-4": SpriteSheet("1-2-3-4", "https://childrenofur.com/assets/entityImages/bunch_of_grapes__x1_1_x1_2_x1_3_x1_4_png_1354829730.png", 228, 30, 57, 30, 4, False),
        }
        self.set_state("1-2-3-4")
        self.max_state = 3

    def show(self) -> None:
        super().show()
        self.set_action_enabled("squish", True)

    def hide(self, respawn_time: timedelta = None) -> None:
        super().hide(respawn_time)
        self.set_action_enabled("squish", False)

    async def squish(self, socket = None, email: str = None) -> bool:
        if self.hidden:
            return False

        energy = (await self.get_metabolics(email)).energy
        if not await self.try_set_metabolics(email, energy=self.ENERGY_AWARD):
            return False

        remain = int((self.ENERGY_REQ - (energy + self.ENERGY_AWARD)) / self.ENERGY_AWARD)
        await toast("Ur done!" if remain == 0 else f"{remain} bunch{'' if remain == 1 else 'es'} of grapes to go!", socket)

        # update global stat
        await StatManager.add(email, Stat.grapes_squished)

        # prevent further actions
        self.hide(timedelta(minutes=2))

        return True
