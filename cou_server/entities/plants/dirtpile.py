from datetime import datetime, timedelta, timezone
import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.plants.plant import Plant
from cou_server.entities.requirements import EnergyRequirements, ItemRequirements
from cou_server.entities.spritesheet import SpriteSheet


class DirtPile(Plant):
    def __init__(self, id: str, x: float, y: float, z: float, rotation: float, h_flip: bool, street_name: str):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.action_time = 3000
        self.type = "Dirt Pile"

        self.actions.append(Action(
            actionName="dig", actionWord="digging", timeRequired=self.action_time,
            energyRequirements=EnergyRequirements(energy_amount=8),
            itemRequirements=ItemRequirements(any=["shovel", "ace_of_spades"]),
        ))
        self.states = {
            "maturity_1": SpriteSheet(
                "maturity_1",
                "https://childrenofur.com/assets/entityImages/dirt_pile_dirt_state_x11_1_variant_dirt1_1_png_1354833756.png",
                780, 213, 195, 71, 11, False,
            ),
            "maturity_2": SpriteSheet(
                "maturity_2",
                "https://childrenofur.com/assets/entityImages/dirt_pile_dirt_state_x11_1_variant_dirt2_1_png_1354833757.png",
                780, 213, 195, 71, 11, False,
            )
        }

        maturity = random.randint(1, len(self.states))
        self.set_state(f"maturity_{maturity}")
        self.state = random.randint(0, self.current_state.num_frames - 1)
        self.max_state = 0

    def update(self, simulate_tick: bool = False):
        if self.state >= self.current_state.num_frames:
            self.set_action_enabled("dig", False)

        if self.respawn is not None and datetime.now(timezone.utc) > self.respawn:
            self.state = 0
            self.set_action_enabled("dig", True)
            self.respawn = None

        if self.state < self.max_state:
            self.state = self.max_state

    async def dig(self, socket, email: str) -> bool:
        # make sure the player has a shovel that can dig this dirt
        dig_action = next(a for a in self.actions if a.actionName == "dig")
        types = dig_action.itemRequirements.any
        success = await InventoryManager.decrease_durability(email, types)
        success = True
        if not success:
            return False

        self.state += 1
        if self.state >= self.current_state.num_frames:
            self.respawn = datetime.now(timezone.utc) + timedelta(minutes=2)

        # give the player the "fruits" of their labor
        await InventoryManager.add_item_to_user(email, cou_globals.items["earth"], 1, self.id)

        # 1 in 10 chance to get a lump of loam as well
        if random.randint(1, 10) == 5:
            await InventoryManager.add_item_to_user(email, cou_globals.items["loam"], 1, self.id)

        dug = await StatManager.add(email, Stat.dirt_dug)
        if dug >= 503:
            await Achievement.find("dirt_diggler").award_to(email)
        elif dug >= 251:
            await Achievement.find("dirtomancer").award_to(email)
        elif dug >= 127:
            await Achievement.find("loamist").award_to(email)
        elif dug >= 61:
            await Achievement.find("dirt_monkey").award_to(email)
        elif dug >= 29:
            await Achievement.find("shovel_jockey").award_to(email)

        return True