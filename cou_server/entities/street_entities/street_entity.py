import dataclasses
import json

from cou_server.common.serializable import serializable


@serializable
@dataclasses.dataclass
class StreetEntity:
    id: str
    type: str
    tsid: str  # must start with L
    x: float = 0
    y: float = 0
    z: float = 0
    h_flip: bool = False
    rotation: float = 0
    metadata_json: str = None
    username: str = None

    def __post_init__(self):
        if self.username is not None and self.metadata.get("creator") is None:
            self.metadata = self.metadata | {"creator": self.username}

    @property
    def metadata(self) -> dict[str, str]:
        json.loads(self.metadata_json or '{}')

    @metadata.setter
    def metadata(self, new_metadata: dict[str, str]) -> None:
        self.metadata_json = json.dumps(new_metadata)
