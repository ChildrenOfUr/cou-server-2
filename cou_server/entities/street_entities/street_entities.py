from sqlalchemy.sql import text

from cou_server.common import get_logger
from cou_server.common.database import remote_engine
from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints import mapdata
from cou_server.entities.street_entities.street_entity import StreetEntity


LOGGER = get_logger(__name__)


class StreetEntities:
    TABLE = "street_entities"

    @classmethod
    async def get_entities(cls, tsid: str) -> list[StreetEntity]:
        if tsid is None:
            LOGGER.error("Error getting entities for tsid 'None'")
            return []

        tsid = mapdata.tsid_l(tsid)

        try:
            async with remote_engine.connect() as conn:
                query = text(f"SELECT * FROM {cls.TABLE} WHERE tsid = :tsid")
                return [StreetEntity(**row) for row in (await conn.execute(query, parameters={"tsid": tsid})).mappings()]
        except Exception:
            LOGGER.exception(f"Could not get entities for {tsid}")
            return []

    @classmethod
    async def get_entity(cls, entity_id) -> StreetEntity:
        try:
            async with remote_engine.connect() as conn:
                query = text(f"SELECT * FROM {cls.TABLE} WHERE id = :entity_id")
                return StreetEntity(**(await conn.execute(query, parameters={"entity_id": entity_id})).mappings().first())
        except Exception:
            LOGGER.exception(f"Could not get street entity {entity_id}")
            return None

    @classmethod
    async def set_entity(cls, entity: StreetEntity, load_now: bool = True, load_db: bool = True) -> bool:
        async def _set_in_db(entity: StreetEntity) -> bool:
            try:
                async with remote_engine.connect() as conn:
                    query = text(
                        f"INSERT INTO {cls.TABLE} (id, type, tsid, x, y, z, h_flip, rotation, metadata_json)"
                        " VALUES (:id, :type, :tsid, :x, :y, :z, :h_flip, :rotation, :metadata_json)"
                        " ON CONFLICT (id) DO UPDATE"
                        " SET tsid = :tsid, x = :x, y = :y, z = :z, h_flip = :h_flip, rotation = :rotation, metadata_json = :metadata_json"
                    )
                    return (await conn.execute(query, parameters=entity.to_json())).rowcount == 1
            except Exception:
                LOGGER.exception(f"Could not edit entity {entity}")
                return False

        def _set_in_memory(entity: StreetEntity) -> bool:
            street = mapdata.get_street_by_tsid(entity.tsid)
            if street:
                # if the street isn't currently loaded, then just return
                if street["label"] not in cou_globals.streets:
                    LOGGER.warning(f"Tried to set entity <id={entity.id}> on unloaded street <tsid={entity.tsid}>")
                    return False
                return cou_globals.streets[street["label"]].put_entities_in_memory([entity])

            return True

        if load_db and not (await _set_in_db(entity)):
            return False

        if load_now and not (await _set_in_memory(entity)):
            return False

        return True

    @classmethod
    async def delete_entity(cls, entity_id: str) -> bool:
        from cou_server.endpoints import street_update

        async def _delete_from_db() -> bool:
            try:
                async with remote_engine.connect() as conn:
                    query = text(f"DELETE FROM {cls.TABLE} WHERE id = :id")
                    return (await conn.execute(query, parameters={"id": entity_id})).rowcount == 1
            except Exception:
                LOGGER.exception(f"Could not delete entity {entity_id} from database")
                return False

        async def _delete_from_memory() -> bool:
            try:
                street_update.queue_npc_remove(entity_id)
                return True
            except Exception:
                LOGGER.exception(f"Could not delete entity {entity_id} from memory")
                return False

        if not (await _delete_from_db()):
            return False
        else:
            return await _delete_from_memory()
