import dataclasses
import json
from typing import Any

from cou_server.common.serializable import serializable
from cou_server.entities.requirements import ItemRequirements, SkillRequirements, EnergyRequirements


@serializable
@dataclasses.dataclass
class Action:
    actionName: str = None
    actionWord: str = None
    error: str = None
    associatedSkill: str = None
    dropMap: dict[str, Any] = None
    enabled: bool = True
    multiEnabled: bool = False
    groundAction: bool = False
    description: str = ""
    timeRequired: int = 0
    itemRequirements: ItemRequirements = dataclasses.field(default_factory=ItemRequirements)
    skillRequirements: SkillRequirements = dataclasses.field(default_factory=SkillRequirements)
    energyRequirements: EnergyRequirements = dataclasses.field(default_factory=EnergyRequirements)

    def __post_init__(self):
        self.actionWord = self.actionName.lower()

    @classmethod
    def with_name(cls, action_name: str) -> "Action":
        return Action(actionName=action_name)

    @classmethod
    def clone(cls, action: "Action") -> "Action":
        if action is None:
            return None

        return Action.from_json(json.loads(json.dumps(action.to_json())))

    def __str__(self) -> str:
        return_string = (
            f"{self.actionName} requires any of {self.itemRequirements.any},"
            f" all of {self.itemRequirements.all} and at least "
        )
        skills = [
            f"{level} level of {skill}"
            for skill, level in self.skillRequirements.required_skill_levels.items()
        ]
        return_string += ", ".join(skills)
        return return_string
