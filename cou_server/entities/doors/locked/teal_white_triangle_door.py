from cou_server.entities.spritesheet import SpriteSheet
from .locked_door import LockedDoor

INTERIOR = SpriteSheet(
    state_name="door_bh_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_bureaucratic_hall_int.svg",
    sheet_width=132,
    sheet_height=312,
    frame_width=132,
    frame_height=312,
    num_frames=1,
    loops=True,
)


class TealWhiteTriangleLockedDoor(LockedDoor):
    def __init__(self, entity_id, street_name, x, y):
        self.required_key = "teal_white_triangle_key"
        self.outside = street_name != "Ajaya Bliss"

        if self.outside:
            # Enter Ajaya Bliss
            self.to_location = "LA9154LI9R22R7A"
            self.outside = True
        else:
            # Exit to Subarna Spells
            self.to_location = "LA9B6PJ3NM22M0D"
            self.outside = False

        super().__init__(entity_id, street_name, x, y)

        self.current_state = INTERIOR
