from cou_server.common.util import toast
from cou_server.entities.action import Action
from cou_server.entities.doors.door import Door, ENTER_ACTION
from cou_server.entities.requirements import ItemRequirements


class LockedDoor(Door):
    required_key: str

    def __init__(self, entity_id, street_name, x, y):
        super().__init__(entity_id, street_name, x, y)
        self.type = "Locked Door"

        if not self.required_key:
            raise ValueError("Cannot instantiate this locked door "
                             "because a key was not set.")

        if self.outside:
            key_req = ItemRequirements()
            key_req.any = list(self.required_key)
            key_req.error = "You need a key to unlock this door."

            locked_enter_action = Action(ENTER_ACTION)
            locked_enter_action.itemRequirements = key_req
            self.actions = [locked_enter_action]


    async def enter(self, user_socket, email):
        success = False  # InventoryV2.takeAnyItemsFromUser(email, requiredKey, 1) == 1

        if success:
            self.use_door(user_socket, email)
        else:
            toast("You need the correct key for this door.", user_socket)

    def __dict__(self):
        return super().__dict__() + {"key": self.required_key}
