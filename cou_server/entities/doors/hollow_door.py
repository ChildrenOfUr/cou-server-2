from cou_server.common.util import toast
from cou_server.entities.spritesheet import SpriteSheet
from .door import Door, ENTER_ACTION, EXIT_ACTION

_DATA = [
    {
        "outside": {"tsid": "LCR195Q63RK143M", "streetName": "Grimssea Bottom"},
        "inside": {"tsid": "LDODG1KQLOS2GNO", "streetName": "Grimssea Hollow"},
    }
]

INTERIOR = SpriteSheet(
    state_name="door_shoppe_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_heights.png",
    sheet_width=154,
    sheet_height=175,
    frame_width=154,
    frame_height=175,
    num_frames=1,
    loops=True,
)

EXTERIOR = SpriteSheet(
    state_name="door_shoppe_int",
    url="https://childrenofur.com/assets/entityImages/door_asset_mini_door_01a_g1.png",
    sheet_width=55,
    sheet_height=120,
    frame_width=55,
    frame_height=20,
    num_frames=1,
    loops=True,
)


def _find_state(street_name):
    outside = False
    data = dict()

    for hollow_door_data in _DATA:
        if hollow_door_data["outside"]["streetName"] == street_name:
            outside = True
            data = hollow_door_data
            break
        elif hollow_door_data["inside"]["streetName"] == street_name:
            outside = False
            data = hollow_door_data
            break

    return {"data": data, "outside": outside}


class HollowDoor(Door):
    def __init__(self, entity_id, street_name, x, y):
        super().__init__(entity_id, street_name, x, y)
        data_state = _find_state(street_name)
        outside = data_state["outside"]

        if outside:
            self.type = data_state["data"]["inside"]["streetName"]
            self.to_location = data_state["data"]["inside"]["tsid"]
            self.actions = list(ENTER_ACTION)
            self.current_state = EXTERIOR
        else:
            self.type = data_state["data"]["outside"]["streetName"]
            self.to_location = data_state["data"]["outside"]["tsid"]
            self.actions = list(EXIT_ACTION)
            self.current_state = INTERIOR

    async def enter(self, user_socket, email):
        toast("You can't fit in there!", user_socket)
