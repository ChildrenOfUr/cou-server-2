from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.shrines.shrine import Shrine


class Spriggan(Shrine):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_spriggan__x1_close_png_1354831248.png", 906, 752, 151,188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_spriggan__x1_open_png_1354831246.png", 906, 752, 151, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_spriggan__x1_open_png_1354831246.png", 906, 752, 151, 188, 1, False),
        }
        self.set_state("still")
        self.type = "Spriggan"
        self.description = "This is a shrine to Spriggan. Sure, Spriggan is the most taciturn and humorless of all the giants. You would be, too, if you had sole dominion over the trees. Trees are serious business, you know."


class SprigganFirebog(Spriggan):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_spriggan__x1_close_png_1354832843.png", 984, 848, 164, 212, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_spriggan__x1_open_png_1354832841.png", 984, 848, 164, 212, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_spriggan__x1_open_png_1354832841.png", 984, 848, 164, 212, 1, False),
        }
        self.set_state("still")


class SprigganIx(Spriggan):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_spriggan__x1_close_png_1354831304.png", 840, 864, 168, 216, 20, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_spriggan__x1_open_png_1354831303.png", 840, 864, 168, 216, 24, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_spriggan__x1_open_png_1354831303.png", 840, 864, 168, 216, 1, False),
        }
        self.set_state("still")


class SprigganUralia(Spriggan):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_spriggan__x1_close_png_1354831918.png", 756, 752, 126, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_spriggan__x1_open_png_1354831915.png", 756, 752, 126, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_spriggan__x1_open_png_1354831915.png", 756, 752, 126, 188, 1, False),
        }
        self.set_state("still")
