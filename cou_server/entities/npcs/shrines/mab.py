from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.shrines.shrine import Shrine


class Mab(Shrine):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_mab__x1_close_png_1354831238.png", 906, 752, 151,188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_mab__x1_open_png_1354831237.png", 906, 752, 151, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_mab__x1_open_png_1354831237.png", 906, 752, 151, 188, 1, False),
        }
        self.set_state("still")
        self.type = "Mab"
        self.description = "This is a shrine to Mab, the giant who holds sway over the harvest. She honors industriousness, and rightfully so. Sometimes, however, industriousness can turn to greed. This is a problem."


class MabFirebog(Mab):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_mab__x1_close_png_1354832830.png", 984, 848, 164, 212, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_mab__x1_open_png_1354832828.png", 984, 848, 164, 212, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_mab__x1_open_png_1354832828.png", 984, 848, 164, 212, 1, False),
        }
        self.set_state("still")


class MabIx(Mab):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_mab__x1_close_png_1354831294.png", 840, 864, 168, 216, 20, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_mab__x1_open_png_1354831292.png", 840, 864, 168, 216, 24, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_mab__x1_open_png_1354831292.png", 840, 864, 168, 216, 1, False),
        }
        self.set_state("still")


class MabUralia(Mab):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_mab__x1_close_png_1354831904.png", 756, 752, 126, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_mab__x1_open_png_1354831902.png", 756, 752, 126, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_mab__x1_open_png_1354831902.png", 756, 752, 126, 188, 1, False),
        }
        self.set_state("still")
