from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.shrines.shrine import Shrine


class Friendly(Shrine):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_friendly__x1_close_png_1354831218.png", 906, 752, 151,188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_friendly__x1_open_png_1354831217.png", 906, 752, 151, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_friendly__x1_open_png_1354831217.png", 906, 752, 151, 188, 1, False),
        }
        self.set_state("still")
        self.type = "Friendly"
        self.description = "This is a shrine to Friendly, the giant who oversees all things celestial, nocturnal, lunar, stygian and murky. Despite this, he is, as his name implies, considered by many to be the nicest of the giants and the one most likely to loan you twenty currants till payday."


class FriendlyFirebog(Friendly):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_friendly__x1_close_png_1354832801.png", 984, 848, 164, 212, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_friendly__x1_open_png_1354832798.png", 984, 848, 164, 212, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_friendly__x1_open_png_1354832798.png", 984, 848, 164, 212, 1, False),
        }
        self.set_state("still")


class FriendlyIx(Friendly):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_friendly__x1_close_png_1354831273.png", 840, 864, 168, 216, 20, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_friendly__x1_open_png_1354831272.png", 840, 864, 168, 216, 24, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_friendly__x1_open_png_1354831272.png", 840, 864, 168, 216, 1, False),
        }
        self.set_state("still")


class FriendlyUralia(Friendly):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_friendly__x1_close_png_1354831876.png", 756, 752, 126, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_friendly__x1_open_png_1354831874.png", 756, 752, 126, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_friendly__x1_open_png_1354831874.png", 756, 752, 126, 188, 1, False),
        }
        self.set_state("still")
