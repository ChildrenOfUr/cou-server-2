from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.shrines.shrine import Shrine


class Zille(Shrine):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_zille__x1_close_png_1354831253.png", 906, 752, 151, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_zille__x1_open_png_1354831251.png", 906, 752, 151, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_zille__x1_open_png_1354831251.png", 906, 752, 151, 188, 1, False),
        }
        self.set_state("still")
        self.type = "Zille"
        self.description = "This is a shrine to Zille, the giant whose domain is the mountains. Hills, too. Also hillocks, pingos, drumlins and buttes. It's safe to consider that any bump in the ground is Zille's turf. She takes no responsibility, however, for volcanoes."


class ZilleFirebog(Zille):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_zille__x1_close_png_1354832857.png", 984, 848, 164, 212, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_zille__x1_open_png_1354832855.png", 984, 848, 164, 212, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_zille__x1_open_png_1354832855.png", 984, 848, 164, 212, 1, False),
        }
        self.set_state("still")


class ZilleIx(Zille):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_zille__x1_close_png_1354831310.png", 840, 864, 168, 216, 20, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_zille__x1_open_png_1354831308.png", 840, 864, 168, 216, 24, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_zille__x1_open_png_1354831308.png", 840, 864, 168, 216, 1, False),
        }
        self.set_state("still")


class ZilleUralia(Zille):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_zille__x1_close_png_1354831931.png", 756, 752, 126, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_zille__x1_open_png_1354831929.png", 756, 752, 126, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_zille__x1_open_png_1354831929.png", 756, 752, 126, 188, 1, False),
        }
        self.set_state("still")
