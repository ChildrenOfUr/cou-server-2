from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.shrines.shrine import Shrine


class Lem(Shrine):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_lem__x1_close_png_1354831233.png", 906, 752, 151,188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_lem__x1_open_png_1354831232.png", 906, 752, 151, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_lem__x1_open_png_1354831232.png", 906, 752, 151, 188, 1, False),
        }
        self.set_state("still")
        self.type = "Lem"
        self.description = "This is a shrine to Lem, the giant of travel and navigation. If you've ever found yourself somewhere you didn't plan to be, chances are it was a Lemish practical joke, for which he is utterly unrepentant."


class LemFirebog(Lem):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_lem__x1_close_png_1354832823.png", 984, 848, 164, 212, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_lem__x1_open_png_1354832820.png", 984, 848, 164, 212, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_firebog_lem__x1_open_png_1354832820.png", 984, 848, 164, 212, 1, False),
        }
        self.set_state("still")


class LemIx(Lem):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_lem__x1_close_png_1354831289.png", 840, 864, 168, 216, 20, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_lem__x1_open_png_1354831287.png", 840, 864, 168, 216, 24, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_ix_lem__x1_open_png_1354831287.png", 840, 864, 168, 216, 1, False),
        }
        self.set_state("still")


class LemUralia(Lem):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.states = {
            "close" : SpriteSheet("close", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_lem__x1_close_png_1354831897.png", 756, 752, 126, 188, 23, False),
            "open" : SpriteSheet("open", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_lem__x1_open_png_1354831895.png", 756, 752, 126, 188, 22, False),
            "still" : SpriteSheet("still", "https://childrenofur.com/assets/entityImages/npc_shrine_uralia_lem__x1_open_png_1354831895.png", 756, 752, 126, 188, 1, False),
        }
        self.set_state("still")
