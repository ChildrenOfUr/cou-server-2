from datetime import timedelta
import json

from cou_server.common import get_logger, util
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.util import Timer
from cou_server.endpoints import quest
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.npcs.npc import NPC


LOGGER = get_logger(__name__)


class Shrine(NPC):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.description = None
        self.commune_count = 0
        self.action_time = 0
        self.actions.append(Action(actionName="Commune With", timeRequired=self.action_time))

    def update(self, simulate_tick: bool = False) -> None:
        return

    async def close(self, socket = None, email: str = None) -> None:
        self.commune_count -= 1
        # if no one else has them open
        if self.commune_count <= 0:
            self.commune_count = 0
            self.set_state("close")
            length = int(self.current_state.num_frames / 30 * 1000)
            def set_still():
                self.current_state = self.states["still"]
            Timer(timedelta(milliseconds=length), set_still)

    async def communeWith(self, socket = None, email: str = None) -> None:
        metabolics = await self.get_metabolics(email)

        giant_name = self.type[0].upper() + self.type[1:]
        giant_favor = getattr(metabolics, f"{giant_name}favor".lower())
        max_amt = getattr(metabolics, f"{giant_name}favor_max".lower())

        map = {
            "giantName": giant_name,
            "favor": giant_favor,
            "maxFavor": max_amt,
            "id": self.id,
        }
        await socket.send(json.dumps(map))

        self.commune_count += 1
        self.set_state("open")

    async def donate(self, socket = None, itemType: str = None, qty: int = None, email: str = None) -> None:
        # import pudb; pudb.set_trace()
        taken = await InventoryManager.take_any_items_from_user(email, itemType, qty)
        if not taken == qty:
            LOGGER.warning(f"Failed to donate {qty} x {itemType} to {self.type} from <email={email}>")
            return

        item = cou_globals.items[itemType]
        giant_name = self.type[0].upper() + self.type[1:]

        # add favor
        fav_amt = int(item.price * qty * .35)
        metabolics = await self.try_set_favor(email, giant_name, fav_amt)

        # add iMG
        img_amt = util.clamp(int(fav_amt / 2), 1, max(item.price, 1))
        await self.try_set_metabolics(email, img_min=img_amt)

        added_favor_map = {
            "favorUpdate": True,
            "favor": getattr(metabolics, f"{giant_name}favor".lower()),
            "maxFavor": getattr(metabolics, f"{giant_name}favor_max".lower()),
        }
        await socket.send(json.dumps(added_favor_map))

        # offer 'get an emblem with a giant' quest
        await quest.quest_log_cache[email].offer_quest("Q3")
        # offer icon quest (only after you complete the emblem quest)
        await quest.quest_log_cache[email].offer_quest("Q5")
