from importlib import resources
import random
import json

from cou_server.achievements.stats import Stat, StatManager
from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints import quest
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.items.item import Item
from cou_server.entities.npcs.npc import NPC
from cou_server.metabolics.metabolics_change import MetabolicsChange


VENDOR_TYPES: dict[str, str] = {}
LOGGER = get_logger(__name__)


class Vendor(NPC):
    @classmethod
    def load_vendor_types(cls) -> int:
        global VENDOR_TYPES

        with resources.files("data.vendors").joinpath("vendors.json").open() as vendors_file:
            VENDOR_TYPES = json.loads(vendors_file.read())
        LOGGER.debug(f"[Vendor] Loaded {len(VENDOR_TYPES)} vendor types")
        return len(VENDOR_TYPES)

    ## FYI
     # 'type' is the NPC type that is passed to the client for rendering, and is displayed to the user
     # 'vendor_type' decides which items to sell, and is never displayed to the user
    ##

    def __init__(self, id, x, y, z, rotation, h_flip, street_name, tsid):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.items_for_sale: list[dict] = []
        self.vendor_type = None
        self.items_to_sell: list[Item] = None
        self.items_predefined = False

        # vendor actions are instant
        self.action_time = 0
        self.type = "Street Spirit"
        self.actions.extend([Action.with_name("buy"), Action.with_name("sell")])

        if not self.items_predefined:
            self.items_for_sale.clear()

            vendor_type = VENDOR_TYPES.get(street_name, self.get_random_vendor_type())
            match vendor_type:
                case "alchemical":
                    self.type = "Street Spriit: Alchemical Goods"
                    self.items_for_sale = [
                        cou_globals.items["still"].to_json(),
                        cou_globals.items["tincturing_kit"].to_json(),
                        cou_globals.items["cauldron"].to_json(),
                        cou_globals.items["elemental_pouch"].to_json(),
                        cou_globals.items["alchemistry_kit"].to_json(),
                        cou_globals.items["alchemical_tongs"].to_json(),
                        cou_globals.items["test_tube"].to_json(),
                        cou_globals.items["beaker"].to_json(),
                        cou_globals.items["pick"].to_json(),
                        cou_globals.items["fancy_pick"].to_json(),
                        cou_globals.items["scraper"].to_json(),
                        cou_globals.items["grinder"].to_json(),
                        cou_globals.items["smelter"].to_json(),
                        cou_globals.items["crystalmalizing_chamber"].to_json(),
                        cou_globals.items["firefly_jar"].to_json(),
                    ]
                case "animal":
                    self.type = "Street Spirit: Animal Goods"
                    self.items_for_sale = [
                        cou_globals.items["spindle"].to_json(),
                        cou_globals.items["loomer"].to_json(),
                        cou_globals.items["pig_stick"].to_json(),
                        cou_globals.items["butterfly_stick"].to_json(),
                        cou_globals.items["chicken_stick"].to_json(),
                        cou_globals.items["butterfly_lotion"].to_json(),
                        cou_globals.items["pig_bait"].to_json(),
                        cou_globals.items["quill"].to_json(),
                        cou_globals.items["egg_seasoner"].to_json(),
                        cou_globals.items["butterfly_milker"].to_json(),
                        cou_globals.items["piggy_feeder"].to_json(),
                        cou_globals.items["meat_collector"].to_json()
                    ]
                case "gardening":
                    self.type = "Street Spirit: Gardening Goods"
                    self.items_for_sale = [
                        cou_globals.items["hoe"].to_json(),
                        cou_globals.items["watering_can"].to_json(),
                        cou_globals.items["hatchet"].to_json(),
                        cou_globals.items["bean_seasoner"].to_json(),
                        cou_globals.items["shovel"].to_json(),
                        cou_globals.items["garden_gnome"].to_json(),
                        cou_globals.items["broccoli_seed"].to_json(),
                        cou_globals.items["cabbage_seed"].to_json(),
                        cou_globals.items["carrot_seed"].to_json(),
                        cou_globals.items["corn_seed"].to_json(),
                        cou_globals.items["cucumber_seed"].to_json(),
                        cou_globals.items["onion_seed"].to_json(),
                        cou_globals.items["parsnip_seed"].to_json(),
                        cou_globals.items["potato_seed"].to_json(),
                        cou_globals.items["pumpkin_seed"].to_json(),
                        cou_globals.items["rice_seed"].to_json(),
                        cou_globals.items["spinach_seed"].to_json(),
                        cou_globals.items["tomato_seed"].to_json(),
                        cou_globals.items["zucchini_seed"].to_json()
                    ]
                case "groceries":
                    self.type = "Street Spirit: Groceries"
                    self.items_for_sale = [
                        cou_globals.items["coffee"].to_json(),
                        cou_globals.items["honey"].to_json(),
                        cou_globals.items["mushroom"].to_json(),
                        cou_globals.items["mustard"].to_json(),
                        cou_globals.items["oats"].to_json(),
                        cou_globals.items["oily_dressing"].to_json(),
                        cou_globals.items["olive_oil"].to_json(),
                        cou_globals.items["sesame_oil"].to_json(),
                        cou_globals.items["birch_syrup"].to_json(),
                        cou_globals.items["beer"].to_json(),
                        cou_globals.items["garlic"].to_json(),
                        cou_globals.items["bun"].to_json()
                    ]
                case "hardware":
                    self.type = "Street Spirit: Hardware"
                    self.items_for_sale = [
                        cou_globals.items["gassifier"].to_json(),
                        cou_globals.items["bubble_tuner"].to_json(),
                        cou_globals.items["pick"].to_json(),
                        cou_globals.items["fancy_pick"].to_json(),
                        cou_globals.items["focusing_orb"].to_json(),
                        cou_globals.items["grinder"].to_json(),
                        cou_globals.items["tinkertool"].to_json(),
                        cou_globals.items["hatchet"].to_json(),
                        cou_globals.items["emotional_bear"].to_json(),
                        cou_globals.items["lips"].to_json(),
                        cou_globals.items["moon"].to_json(),
                        cou_globals.items["smelter"].to_json(),
                        cou_globals.items["alchemical_tongs"].to_json(),
                        cou_globals.items["scraper"].to_json(),
                        cou_globals.items["shovel"].to_json(),
                        cou_globals.items["garden_gnome"].to_json(),
                        cou_globals.items["crystalmalizing_chamber"].to_json(),
                        cou_globals.items["machine_stand"].to_json(),
                        cou_globals.items["blockmaker_chassis"].to_json(),
                        cou_globals.items["blockmaker_plates"].to_json(),
                        cou_globals.items["machine_engine"].to_json(),
                        cou_globals.items["fuelmaker_case"].to_json(),
                        cou_globals.items["fuelmaker_core"].to_json(),
                        cou_globals.items["cauldron"].to_json(),
                        cou_globals.items["tincturing_kit"].to_json(),
                        cou_globals.items["still"].to_json(),
                        cou_globals.items["metalmaker_mechanism"].to_json(),
                        cou_globals.items["metalmaker_tooler"].to_json(),
                        cou_globals.items["woodworker_chassis"].to_json(),
                        cou_globals.items["spindle"].to_json(),
                        cou_globals.items["loomer"].to_json(),
                        cou_globals.items["construction_tool"].to_json(),
                        cou_globals.items["bulb"].to_json()
                    ]
                    self.items_for_sale.extend(self.pick_items(["Storage"]))
                case "kitchen":
                    self.type = "Street Spirit: Kitchen Tools"
                    self.items_for_sale = [
                        cou_globals.items["knife_and_board"].to_json(),
                        cou_globals.items["blender"].to_json(),
                        cou_globals.items["frying_pan"].to_json(),
                        cou_globals.items["saucepan"].to_json(),
                        cou_globals.items["cocktail_shaker"].to_json(),
                        cou_globals.items["famous_pugilist_grill"].to_json(),
                        cou_globals.items["awesome_pot"].to_json(),
                        cou_globals.items["fruit_changing_machine"].to_json(),
                        cou_globals.items["spice_mill"].to_json(),
                        cou_globals.items["spicerack"].to_json()
                    ]
                case "mining":
                    self.type = "Street Spirit: Mining"
                    self.items_for_sale = [
                        cou_globals.items["earthshaker"].to_json(),
                        cou_globals.items["face_smelter"].to_json(),
                        cou_globals.items["flaming_humbaba"].to_json(),
                        cou_globals.items["pick"].to_json(),
                        cou_globals.items["fancy_pick"].to_json(),
                        cou_globals.items["grinder"].to_json(),
                        cou_globals.items["grand_ol_grinder"].to_json(),
                        cou_globals.items["smelter"].to_json(),
                        cou_globals.items["tinkertool"].to_json(),
                        cou_globals.items["elemental_pouch"].to_json(),
                        cou_globals.items["alchemical_tongs"].to_json()
                    ]
                case "produce":
                    self.type = "Street Spirit: Produce"
                    self.items_for_sale = [
                        cou_globals.items["garlic"].to_json(),
                        cou_globals.items["broccoli"].to_json(),
                        cou_globals.items["carrot"].to_json(),
                        cou_globals.items["cabbage"].to_json(),
                        cou_globals.items["corn"].to_json(),
                        cou_globals.items["cucumber"].to_json(),
                        cou_globals.items["onion"].to_json(),
                        cou_globals.items["parsnip"].to_json(),
                        cou_globals.items["potato"].to_json(),
                        cou_globals.items["rice"].to_json(),
                        cou_globals.items["spinach"].to_json(),
                        cou_globals.items["tomato"].to_json(),
                        cou_globals.items["zucchini"].to_json(),
                        cou_globals.items["bubble_tuner"].to_json(),
                        cou_globals.items["gassifier"].to_json()
                    ]
                case "toy":
                    self.type = "Street Spirit: Toys"
                    self.items_for_sale = [
                        cou_globals.items["dice"].to_json(),
                        cou_globals.items["12_sided_die"].to_json(),
                        cou_globals.items["fortune_cookie"].to_json(),
                        cou_globals.items["cubimal_series_1_box"].to_json(),
                        cou_globals.items["cubimal_series_2_box"].to_json(),
                        cou_globals.items["cubimal_case"].to_json(),
                        cou_globals.items["emotional_bear"].to_json(),
                        cou_globals.items["lips"].to_json(),
                        cou_globals.items["moon"].to_json(),
                        cou_globals.items["garden_gnome"].to_json(),
                        cou_globals.items["glitchmas_cracker"].to_json(),
                        cou_globals.items["glitchmas_card"].to_json(),
                        cou_globals.items["party_aquarius"].to_json(),
                        cou_globals.items["party_double_rainbow"].to_json(),
                        cou_globals.items["party_mazzala_gala"].to_json(),
                        cou_globals.items["party_nylon_phool"].to_json(),
                        cou_globals.items["party_pitchen_lilliputt"].to_json(),
                        cou_globals.items["party_taster_aquarius"].to_json(),
                        cou_globals.items["party_taster_double_rainbow"].to_json(),
                        cou_globals.items["party_taster_mazzala_gala"].to_json(),
                        cou_globals.items["party_taster_nylon_phool"].to_json(),
                        cou_globals.items["party_taster_pitchen_lilliputt"].to_json(),
                        cou_globals.items["party_taster_toxic_moon"].to_json(),
                        cou_globals.items["party_taster_val_holla"].to_json(),
                        cou_globals.items["party_taster_winter_wingding"].to_json(),
                        cou_globals.items["party_toxic_moon"].to_json(),
                        cou_globals.items["party_val_holla"].to_json(),
                        cou_globals.items["party_winter_wingding"].to_json(),
                        cou_globals.items["wrappable_gift_box"].to_json(),
                        cou_globals.items["camera"].to_json(),
                        cou_globals.items["million_currant_trophy"].to_json()
                    ]

    def get_random_vendor_type(self) -> str:
        return random.choice(list(VENDOR_TYPES.keys()))

    async def buy(self, socket = None, email: str = None) -> None:
        map = {
            "vendorName": self.type,
            "id": self.id,
            "itemsForSale": self.items_for_sale,
        }
        await socket.send(json.dumps(map))

    async def sell(self, socket = None, email: str = None) -> None:
        # prepare the buy window at the same time
        map = {
            "vendorName": self.type,
            "id": self.id,
            "itemsForSale": self.items_for_sale,
            "openWindow": "vendorSell",
        }
        await socket.send(json.dumps(map))

    async def buyItem(self, socket = None, itemType: str = None, num: int = None, email: str = None) -> None:
        if not itemType in cou_globals.items:
            return

        item = Item.clone(cou_globals.items[itemType])
        m = await MetabolicsChange.get_metabolics(email)

        if m.currants >= self.calc_price(item) * num:
            m.currants -= self.calc_price(item) * num
            await MetabolicsChange.set_metabolics(m)
            await InventoryManager.add_item_to_user(email, item, num, self.id)

            if item.itemType == "knife_and_board" and email in quest.quest_log_cache:
                await quest.quest_log_cache[email].offer_quest("Q1")

        await StatManager.add(email, Stat.items_from_vendors, increment=num)

    async def sellItem(self, socket = None, itemType: str = None, num: int = None, email: str = None) -> None:
        if itemType not in cou_globals.items:
            return

        success = await InventoryManager.take_any_items_from_user(email, itemType, num) == num

        if success:
            item = cou_globals.items[itemType]

            m = await MetabolicsChange.get_metabolics(email)
            m.currants += int(item.price * num * .7)
            await MetabolicsChange.set_metabolics(m)

    def calc_price(self, item: Item) -> int:
        return int(item.price * item.discount)

    def pick_items(self, categories: list[str]) -> list[dict]:
        self.items_to_sell = [m for m in cou_globals.items.values() if m.category in categories]

        return [item.to_json() for item in self.items_to_sell]
