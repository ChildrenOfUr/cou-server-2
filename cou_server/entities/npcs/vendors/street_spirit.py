from datetime import datetime, timedelta, timezone

from cou_server.entities.npcs.vendors.vendor import Vendor
from cou_server.endpoints.time import Clock


class StreetSpirit(Vendor):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name, tsid):
        super().__init__(id, x, y, z, rotation, h_flip, street_name, tsid)
        self.open_count = 0
        self.clock = Clock()
        self.current_bob = 0
        self.min_bob = -15
        self.max_bob = 15
        self.bobbing_up = True
        self.speed = 75

    def update(self, simulate_tick: bool = False) -> None:
        super().update()

        def y_action():
            # bob up and down a bit
            if self.bobbing_up:
                self.y -= 1
                self.current_bob -= 1
                if self.current_bob < self.min_bob:
                    self.bobbing_up = False
            else:
                self.y += 1
                self.current_bob += 1
                if self.current_bob > self.max_bob:
                    self.bobbing_up = True

        if self.current_state.state_name == "idle_move":
            self.move_xy(y_action=y_action)

        if self.current_state.state_name in ["still", "idle_hold", "turn", "hover_idle", "hover_talk", "idle_cry", "talk"]:
            self.move_xy(x_action=lambda: ..., y_action=y_action)

    async def buy(self, socket = None, email: str = None) -> None:
        # don't go to another state until closed
        self.respawn = datetime.now(timezone.utc) + timedelta(days=50)
        self.open_count += 1
        await super().buy(socket=socket, email=email)

    async def sell(self, socket = None, email: str = None) -> None:
        # don't go to another state until closed
        self.respawn = datetime.now(timezone.utc) + timedelta(days=50)
        self.open_count += 1
        await super().sell(socket=socket, email=email)

    async def close(self, socket = None, email: str = None) -> None:
        self.open_count -= 1
        # if no one else has them open
        if self.open_count <= 0:
            self.open_count = 0
            self.set_state("close")
