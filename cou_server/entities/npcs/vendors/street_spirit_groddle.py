import asyncio
from datetime import datetime, timezone
import random

from cou_server.common import get_logger
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.vendors.street_spirit import StreetSpirit
from cou_server.models.entity import Entity


LOGGER = get_logger(__name__)


class StreetSpiritGroddle(StreetSpirit):
    VARIANTS: dict[str, dict[str, dict[str, SpriteSheet]]] = {
        "alph": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_close_png_1354835031.png", 792, 591, 132, 197, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_idle_hold_png_1354835020.png", 924, 2561, 132, 197, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_idle_move_png_1354835036.png", 924, 3349, 132, 197, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_open_png_1354835030.png", 924, 591, 132, 197, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_open_png_1354835030.png", 924, 591, 132, 197, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_talk_png_1354835025.png", 924, 2167, 132, 197, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LeafSprout_x1_turn_png_1354835028.png", 924, 1182, 132, 197, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_close_png_1354835067.png", 792, 591, 132, 197, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_idle_hold_png_1354835055.png", 924, 2561, 132, 197, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_idle_move_png_1354835072.png", 924, 3349, 132, 197, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_open_png_1354835065.png", 924, 591, 132, 197, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_open_png_1354835065.png", 924, 591, 132, 197, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_talk_png_1354835060.png", 924, 2167, 132, 197, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LeafSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LeafSprout_x1_turn_png_1354835063.png", 924, 1182, 132, 197, 37, False)
            }
        },
        "cosma": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_close_png_1354834586.png", 882, 300, 98, 150, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_idle_hold_png_1354834580.png", 980, 1350, 98, 150, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_idle_move_png_1354834588.png", 980, 1800, 98, 150, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_open_png_1354834585.png", 980, 300, 98, 150, 19, False),
                "still": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_open_png_1354834585.png", 980, 300, 98, 150, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_talk_png_1354834582.png", 980, 1200, 98, 150, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes2_skull_skull_L0dirt_top_none_x1_turn_png_1354834584.png", 980, 600, 98, 150, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_close_png_1354834609.png", 882, 300, 98, 150, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_idle_hold_png_1354834601.png", 980, 1350, 98, 150, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_idle_move_png_1354834611.png", 980, 1800, 98, 150, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_open_png_1354834608.png", 980, 300, 98, 150, 19, False),
                "still": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_open_png_1354834608.png", 980, 300, 98, 150, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_talk_png_1354834604.png", 980, 1200, 98, 150, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L0dirt_bottom_none_eyes_eyes_L0eyes3_skull_skull_L0dirt_top_none_x1_turn_png_1354834606.png", 980, 600, 98, 150, 37, False)
            }
        },
        "friendly": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_close_png_1354834923.png", 672, 549, 112, 183, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_idle_hold_png_1354834913.png", 896, 2013, 112, 183, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_idle_move_png_1354834926.png", 896, 2745, 112, 183, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_open_png_1354834922.png", 784, 549, 112, 183, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_open_png_1354834922.png", 784, 549, 112, 183, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_talk_png_1354834918.png", 896, 1830, 112, 183, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1LotusTop_x1_turn_png_1354834920.png", 896, 915, 112, 183, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_close_png_1354834963.png", 672, 549, 112, 183, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_idle_hold_png_1354834943.png", 896, 2013, 112, 183, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_idle_move_png_1354834967.png", 896, 2745, 112, 183, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_open_png_1354834953.png", 784, 549, 112, 183, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_open_png_1354834953.png", 784, 549, 112, 183, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_talk_png_1354834948.png", 896, 1830, 112, 183, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1LotusBottom_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1LotusTop_x1_turn_png_1354834951.png", 896, 915, 112, 183, 37, False)
            }
        },
        "grendaline": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_close_png_1354834655.png", 918, 328, 102, 164, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_idle_hold_png_1354834647.png", 918, 1640, 102, 164, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_idle_move_png_1354834657.png", 918, 2296, 102, 164, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_open_png_1354834654.png", 714, 492, 102, 164, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_open_png_1354834654.png", 714, 492, 102, 164, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_talk_png_1354834651.png", 918, 1476, 102, 164, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_none_x1_turn_png_1354834652.png", 816, 820, 102, 164, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_close_png_1354834678.png", 918, 328, 102, 164, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_idle_hold_png_1354834671.png", 918, 1640, 102, 164, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_idle_move_png_1354834681.png", 918, 2296, 102, 164, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_open_png_1354834677.png", 714, 492, 102, 164, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_open_png_1354834677.png", 714, 492, 102, 164, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_talk_png_1354834674.png", 918, 1476, 102, 164, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_none_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_none_x1_turn_png_1354834676.png", 816, 820, 102, 164, 37, False)
            }
        },
        "humbaba": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_close_png_1354834738.png", 999, 344, 111, 172, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_idle_hold_png_1354834727.png", 999, 1720, 111, 172, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_idle_move_png_1354834740.png", 999, 2408, 111, 172, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_open_png_1354834736.png", 777, 516, 111, 172, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_open_png_1354834736.png", 777, 516, 111, 172, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_talk_png_1354834731.png", 999, 1548, 111, 172, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1Grass_x1_turn_png_1354834735.png", 888, 860, 111, 172, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_close_png_1354834766.png", 999, 344, 111, 172, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_idle_hold_png_1354834756.png", 999, 1720, 111, 172, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_idle_move_png_1354834769.png", 999, 2408, 111, 172, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_open_png_1354834765.png", 777, 516, 111, 172, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_open_png_1354834765.png", 777, 516, 111, 172, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_talk_png_1354834761.png", 999, 1548, 111, 172, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1grassSkirt_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1Grass_x1_turn_png_1354834763.png", 888, 860, 111, 172, 37, False)
            }
        },
        "lem": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_close_png_1354835264.png", 930, 582, 155, 194, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_hold_png_1354835252.png", 930, 2910, 155, 194, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_move_png_1354835269.png", 930, 3880, 155, 194, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835263.png", 775, 776, 155, 194, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835263.png", 775, 776, 155, 194, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_talk_png_1354835257.png", 930, 2522, 155, 194, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_turn_png_1354835260.png", 930, 1358, 155, 194, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_close_png_1354835298.png", 930, 582, 155, 194, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_hold_png_1354835285.png", 930, 2910, 155, 194, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_move_png_1354835302.png", 930, 3880, 155, 194, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835296.png", 775, 776, 155, 194, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835296.png", 775, 776, 155, 194, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_talk_png_1354835291.png", 930, 2522, 155, 194, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_turn_png_1354835294.png", 930, 1358, 155, 194, 37, False)
            }
        },
        "mab": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_close_png_1354835466.png", 756, 525, 126, 175, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_idle_hold_png_1354835454.png", 882, 2275, 126, 175, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_idle_move_png_1354835471.png", 882, 2975, 126, 175, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_open_png_1354835464.png", 882, 525, 126, 175, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_open_png_1354835464.png", 882, 525, 126, 175, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_talk_png_1354835460.png", 882, 1925, 126, 175, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1Feathers_x1_turn_png_1354835462.png", 882, 1050, 126, 175, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_close_png_1354835503.png", 756, 525, 126, 175, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_idle_hold_png_1354835490.png", 882, 2275, 126, 175, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_idle_move_png_1354835507.png", 882, 2975, 126, 175, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_open_png_1354835501.png", 882, 525, 126, 175, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_open_png_1354835501.png", 882, 525, 126, 175, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_talk_png_1354835496.png", 882, 1925, 126, 175, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1FallLeaves_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1Feathers_x1_turn_png_1354835499.png", 882, 1050, 126, 175, 37, False)
            }
        },
        "pot": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_close_png_1354835579.png", 864, 525, 144, 175, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_idle_hold_png_1354835568.png", 864, 2625, 144, 175, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_idle_move_png_1354835587.png", 864, 3500, 144, 175, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_open_png_1354835577.png", 720, 700, 144, 175, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_open_png_1354835577.png", 720, 700, 144, 175, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_talk_png_1354835572.png", 864, 2275, 144, 175, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes2_skull_skull_L1wood_top_top_L1woodTwig_x1_turn_png_1354835575.png", 864, 1225, 144, 175, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_close_png_1354835621.png", 864, 525, 144, 175, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_idle_hold_png_1354835608.png", 864, 2625, 144, 175, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_idle_move_png_1354835628.png", 864, 3500, 144, 175, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_open_png_1354835619.png", 720, 700, 144, 175, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_open_png_1354835619.png", 720, 700, 144, 175, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_talk_png_1354835614.png", 864, 2275, 144, 175, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1wood_bottom_bottom_L1WoodAcornBranch_eyes_eyes_L1eyes3_skull_skull_L1wood_top_top_L1woodTwig_x1_turn_png_1354835617.png", 864, 1225, 144, 175, 37, False)
            }
        },
        "spriggan": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_close_png_1354835264.png", 930, 582, 155, 194, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_hold_png_1354835252.png", 930, 2910, 155, 194, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_move_png_1354835269.png", 930, 3880, 155, 194, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835263.png", 775, 776, 155, 194, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835263.png", 775, 776, 155, 194, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_talk_png_1354835257.png", 930, 2522, 155, 194, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_turn_png_1354835260.png", 930, 1358, 155, 194, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_close_png_1354835298.png", 930, 582, 155, 194, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_hold_png_1354835285.png", 930, 2910, 155, 194, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_idle_move_png_1354835302.png", 930, 3880, 155, 194, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835296.png", 775, 776, 155, 194, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_open_png_1354835296.png", 775, 776, 155, 194, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_talk_png_1354835291.png", 930, 2522, 155, 194, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1Branches_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSpikey_x1_turn_png_1354835294.png", 930, 1358, 155, 194, 37, False)
            }
        },
        "tii": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_close_png_1354834823.png", 990, 374, 110, 187, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_idle_hold_png_1354834814.png", 990, 1870, 110, 187, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_idle_move_png_1354834826.png", 990, 2618, 110, 187, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_open_png_1354834822.png", 770, 561, 110, 187, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_open_png_1354834822.png", 770, 561, 110, 187, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_talk_png_1354834818.png", 990, 1683, 110, 187, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1FlowerTop_x1_turn_png_1354834820.png", 880, 935, 110, 187, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_close_png_1354834853.png", 990, 374, 110, 187, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_idle_hold_png_1354834842.png", 990, 1870, 110, 187, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_idle_move_png_1354834856.png", 990, 2618, 110, 187, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_open_png_1354834852.png", 770, 561, 110, 187, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_open_png_1354834852.png", 770, 561, 110, 187, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_talk_png_1354834847.png", 990, 1683, 110, 187, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1FlowerBush_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1FlowerTop_x1_turn_png_1354834850.png", 880, 935, 110, 187, 37, False)
            }
        },
        "zille": {
            "day": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_close_png_1354835359.png", 672, 534, 112, 178, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_idle_hold_png_1354835349.png", 896, 1958, 112, 178, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_idle_move_png_1354835362.png", 896, 2670, 112, 178, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_open_png_1354835357.png", 784, 534, 112, 178, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_open_png_1354835357.png", 784, 534, 112, 178, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_talk_png_1354835353.png", 896, 1780, 112, 178, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes2_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_turn_png_1354835356.png", 896, 890, 112, 178, 37, False)
            },
            "night": {
                "close": SpriteSheet("close", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_close_png_1354835388.png", 672, 534, 112, 178, 17, False),
                "idle_hold": SpriteSheet("idle_hold", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_idle_hold_png_1354835378.png", 896, 1958, 112, 178, 85, True),
                "idle_move": SpriteSheet("idle_move", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_idle_move_png_1354835392.png", 896, 2670, 112, 178, 119, True),
                "open": SpriteSheet("open", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_open_png_1354835387.png", 784, 534, 112, 178, 19, False),
                "still": SpriteSheet("still", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_open_png_1354835387.png", 784, 534, 112, 178, 1, False),
                "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_talk_png_1354835382.png", 896, 1780, 112, 178, 73, False),
                "turn": SpriteSheet("turn", "https://childrenofur.com/assets/entityImages/street_spirit_groddle_base_base_L1dirt_bottom_bottom_L1flower_eyes_eyes_L1eyes3_skull_skull_L1dirt_top_top_L1dirtSeedling_x1_turn_png_1354835385.png", 896, 890, 112, 178, 37, False)
            }
        },
    }

    def __init__(self, id, x, y, z, rotation, h_flip, street_name, tsid):
        super().__init__(id, x, y, z, rotation, h_flip, street_name, tsid)
        self.speed = -75
        self.items_predefined = False
        self.states = self.VARIANTS[self._random_giant][self._day_night]
        self.set_state("idle_hold")
        async def set_state():
            try:
                giant = await self._get_giant_name(tsid)
                light = self._day_night
                self.states = self.VARIANTS[giant][light]
                self.set_state("idle_hold")
            except Exception:
                LOGGER.exception(f"Could not get {type(self).__name__} state for <giant={giant}> and <light={light}>")
        asyncio.create_task(set_state())


    def update(self, simulate_tick: bool = False) -> None:
        super().update()

        if self.respawn and self.respawn < datetime.now(timezone.utc):
            # if we just turned, we should say we"re facing the other way
            # then we should start moving (that"s why we turned around after all)
            if self.current_state.state_name == "turn":
                self.facing_right = not self.facing_right
                self.set_state("idle_move")
                return
            # sometimes use still so that the blinking isn"t predictable
            roll = random.randint(0, 2)
            if roll == 1:
                self.set_state("still")
            else:
                self.set_state("idle_hold")
                self.respawn = None
            return

        if self.respawn is None:
            # sometimes move around
            roll = random.randint(0, 19)
            if roll == 3:
                self.set_state("turn")

    async def buy(self, socket = None, email: str = None) -> None:
        self.set_state("open")
        await super().buy(socket=socket, email=email)

    async def sell(self, socket = None, email: str = None) -> None:
        self.set_state("open")
        await super().sell(socket=socket, email=email)

    async def close(self, socket = None, email: str = None) -> None:
        self.open_count -= 1
        # if no one else has them open
        if self.open_count <= 0:
            self.open_count = 0
            self.set_state("close")

    async def _get_giant_name(self, tsid: str) -> str:
        try:
            # find shrine entity type
            for entity in await Entity.get_entities(tsid):
                type = entity.type.lower()
                if type in self.VARIANTS:
                    # found a shrine
                    giant_name = type
            assert giant_name is not None
        except Exception:
            # default to random
            giant_name = self._random_giant
            LOGGER.warning(f"No giant defined for {self.type} on <tsid={tsid}>, using {giant_name} randomly")
        return giant_name

    @property
    def _random_giant(self) -> str:
        return random.choice(list(self.VARIANTS.keys()))

    @property
    def _day_night(self) -> str:
        am = "am" in self.clock.time
        hour_min = self.clock.time[:-2].split(":")
        hour = int(hour_min[0])
        if not am:
            if 5 <= hour < 7:
                # daylight to sunset
                time = "day"
            elif 7 <= hour < 12:
                # sunset to night
                time = "night"
            else:
                time = "day"
        else:
            if hour < 5 or hour == 12:
                time = "night"
            elif 5 <= hour < 7:
                # night to sunrise
                time = "night"
            elif 7 <= hour < 9:
                # sunrise to daylight
                time = "day"
            else:
                time = "day"

        return time
