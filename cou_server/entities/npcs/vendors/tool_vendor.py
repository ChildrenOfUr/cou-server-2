from datetime import datetime, timedelta, timezone
import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.vendors.vendor import Vendor



class ToolVendor(Vendor):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name, tsid):
        super().__init__(id, x, y, z, rotation, h_flip, street_name, tsid)
        self.type = "Tool Vendor"
        self.open_count = 0
        self.items_for_sale = [
            cou_globals.items["pig_bait"].to_json(),
            cou_globals.items["butterfly_lotion"].to_json(),
            cou_globals.items["meat_collector"].to_json(),
            cou_globals.items["piggy_feeder"].to_json(),
            cou_globals.items["butterfly_milker"].to_json(),
            cou_globals.items["egg_seasoner"].to_json(),
            cou_globals.items["bean_seasoner"].to_json(),
            cou_globals.items["hatchet"].to_json(),
            cou_globals.items["hoe"].to_json(),
            cou_globals.items["watering_can"].to_json(),
            cou_globals.items["bubble_tuner"].to_json(),
            cou_globals.items["gassifier"].to_json(),
            cou_globals.items["fruit_changing_machine"].to_json(),
            cou_globals.items["spice_mill"].to_json(),
            cou_globals.items["alchemistry_kit"].to_json(),
            cou_globals.items["elemental_pouch"].to_json(),
            cou_globals.items["test_tube"].to_json(),
            cou_globals.items["beaker"].to_json(),
            cou_globals.items["pick"].to_json(),
            cou_globals.items["fancy_pick"].to_json(),
            cou_globals.items["grinder"].to_json(),
            cou_globals.items["tinkertool"].to_json(),
            cou_globals.items["focusing_orb"].to_json(),
            cou_globals.items["alchemical_tongs"].to_json(),
            cou_globals.items["smelter"].to_json(),
            cou_globals.items["shovel"].to_json(),
            cou_globals.items["scraper"].to_json(),
            cou_globals.items["quill"].to_json(),
            cou_globals.items["machine_stand"].to_json(),
            cou_globals.items["blockmaker_chassis"].to_json(),
            cou_globals.items["machine_engine"].to_json(),
            cou_globals.items["blockmaker_plates"].to_json(),
            cou_globals.items["fuelmaker_case"].to_json(),
            cou_globals.items["fuelmaker_core"].to_json(),
            cou_globals.items["cauldron"].to_json(),
            cou_globals.items["tincturing_kit"].to_json(),
            cou_globals.items["still"].to_json(),
            cou_globals.items["metalmaker_mechanism"].to_json(),
            cou_globals.items["metalmaker_tooler"].to_json(),
            cou_globals.items["woodworker_fuser"].to_json(),
            cou_globals.items["woodworker_chassis"].to_json(),
            cou_globals.items["spindle"].to_json(),
            cou_globals.items["loomer"].to_json(),
            cou_globals.items["construction_tool"].to_json(),
            cou_globals.items["bulb"].to_json(),
        ]
        self.items_for_sale.extend(self.pick_items(["Storage"]))
        self.items_predefined = True
        self.speed = 75
        self.states = {
            "attract": SpriteSheet("attract", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_attract_png_1354831448.png", 925, 2500, 185, 250, 50, False),
            "idle_stand": SpriteSheet("idle_stand", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_idle_stand_png_1354831438.png", 4070, 3750, 185, 250, 329, True),
            "talk": SpriteSheet("talk", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_talk_png_1354831442.png", 925, 1500, 185, 250, 26, False),
            "turn_left": SpriteSheet("turn_left", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_turn_left_png_1354831414.png", 925, 500, 185, 250, 10, False),
            "turn_right": SpriteSheet("turn_right", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_turn_right_png_1354831419.png", 740, 750, 185, 250, 11, False),
            "walk_left": SpriteSheet("walk_left", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_walk_left_png_1354831417.png", 925, 1250, 185, 250, 25, True),
            "walk": SpriteSheet("walk", "https://childrenofur.com/assets/entityImages/npc_tool_vendor__x1_walk_png_1354831412.png", 925, 1250, 185, 250, 24, True),
        }
        self.set_state("idle_stand")

    def update(self, simulate_tick: bool = False) -> None:
        super().update()

        # update x and y
        if self.current_state.state_name == "walk":
            self.move_xy()

        if self.respawn and self.respawn < datetime.now(timezone.utc):
            # if we just turned, we should say we're facing the other way, then we should start moving (that's why we turned around after all)
            if self.current_state.state_name == "turn_left":
                # if we turned left, we are no longer facing right
                self.facing_right = False
                # start walking left
                self.set_state("walk")
            elif self.current_state.state_name == "turn_right":
                # if we turned right, we are now facing right
                self.facing_right = True
                # start walking right
                self.set_state("walk")
            else:
                # if we haven't just turned
                # 1 in 10 that we turn around and start walking
                if random.randint(0, 9) == 8:
                    if self.facing_right:
                        self.set_state("turn_left")
                    else:
                        self.set_state("turn_right")
                elif random.randint(0, 1) == 1:
                    self.set_state("walk", repeat=5)
                else:
                    if random.randint(0, 3) > 2:
                        # 50% chance of trying to attract buyers
                        self.set_state("attract")
                    elif random.randint(0, 1) == 1:
                        # wait
                        self.set_state("idle_stand")

    async def buy(self, socket = None, email: str = None) -> None:
        self.set_state("idle_stand")
        # don't go to another state until closed
        self.respawn = datetime.now(timezone.utc) + timedelta(days=50)
        self.open_count += 1
        await super().buy(socket=socket, email=email)

    async def sell(self, socket = None, email: str = None) -> None:
        self.set_state("talk")
        # don't go to another state until closed
        self.respawn = datetime.now(timezone.utc) + timedelta(days=50)
        self.open_count += 1
        await super().sell(socket=socket, email=email)

    async def close(self, socket = None, email: str = None) -> None:
        self.open_count -= 1
        # if no one else has them open
        if self.open_count <= 0:
            self.open_count = 0
            self.set_state("idle_stand")
