import asyncio
from datetime import datetime, timedelta, timezone
from json import dumps
import random

from cou_server.common.global_gobbler import cou_globals
from cou_server.common.util import toast
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action, ItemRequirements
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC


ERR_NO_MUSIC = "You're musicblock-broke, yo."
ERR_BUSY = "Go away, I'm busy right now!"
WARN_HEADPHONES = "You stole my headphones! No juice for you!"

MUSICBLOCK_TYPES = [
    "musicblock_bb_1",
    "musicblock_bb_2",
    "musicblock_bb_3",
    "musicblock_bb_4",
    "musicblock_bb_5",
    "musicblock_db_1",
    "musicblock_db_2",
    "musicblock_db_3",
    "musicblock_db_4",
    "musicblock_db_5",
    "musicblock_dg_1",
    "musicblock_dg2",
    "musicblock_dg3",
    "musicblock_dg4",
    "musicblock_dg5",
    "musicblock_dr_1",
    "musicblock_dr_2",
    "musicblock_dr_3",
    "musicblock_dr_4",
    "musicblock_dr_5",
    "musicblock_xs_1",
    "musicblock_xs_2",
    "musicblock_xs_3",
    "musicblock_xs_4",
    "musicblock_xs_5",
]

MUSICBLOCK_RARES = ["musicblock_gng", "musicblock_stoot", "musicblock_trumpets"]

ALL_MUSICBLOCK_TYPES = MUSICBLOCK_TYPES + MUSICBLOCK_RARES

MUSICBLOCK_ITEM_REQS = ItemRequirements()
MUSICBLOCK_ITEM_REQS.any = ALL_MUSICBLOCK_TYPES
MUSICBLOCK_ITEM_REQS.error = ERR_NO_MUSIC

PLAY = Action("play for", actionWord="crabbing")
PLAY.itemRequirements = MUSICBLOCK_ITEM_REQS

BUY = Action("buy crabpack", actionWord="buying")
BUY.description = "A 3,000-currant bag for 18 musicblocks"

STATES = {
    "dislike_off": SpriteSheet(
        "dislike_off",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_dislike_off_png_1354831193.png",
        786,
        516,
        131,
        129,
        22,
        True,
    ),
    "dislike_on": SpriteSheet(
        "dislike_on",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_dislike_on_png_1354831191.png",
        786,
        516,
        131,
        129,
        30,
        True,
    ),
    "idle0": SpriteSheet(
        "idle0",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle0_png_1354831199.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "idle1": SpriteSheet(
        "idle1",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle1_png_1354831200.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "idle2": SpriteSheet(
        "idle2",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_idle2_png_1354831201.png",
        786,
        645,
        131,
        129,
        30,
        True,
    ),
    "like_off": SpriteSheet(
        "like_off",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_like_off_png_1354831189.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "like_on": SpriteSheet(
        "like_on",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_like_on_png_1354831187.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "listen": SpriteSheet(
        "listen",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_listen_png_1354831185.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
    "talk": SpriteSheet(
        "talk",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_talk_png_1354831203.png",
        917,
        1161,
        131,
        129,
        58,
        True,
    ),
    "walk": SpriteSheet(
        "walk",
        "https://childrenofur.com/assets/entityImages/npc_crab__x1_walk_png_1354831183.png",
        786,
        516,
        131,
        129,
        24,
        True,
    ),
}


def rand_song_length():
    """Find how long a song will play for (5-15 seconds)."""
    return timedelta(seconds=random.randint(5, 15))


def rand_react_length():
    """Find how long the crab dances (3-8 seconds)."""
    return timedelta(seconds=random.randint(3, 8))


class Crab(NPC):
    @classmethod
    def random_music_block(cls) -> str:
        """Get a random music block itemType. Will not include rare types."""
        return random.choice(MUSICBLOCK_TYPES)


    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)

        self.listen_history: list[str] = []
        self.busy_with_email = ""

        # Choose a random type of crab (there are different styles)
        self.idle_type = random.randint(0, 2)

        self.type = "Crab"
        self.speed = 60  # px/s
        self.action_time = 0
        self.actions += [PLAY, BUY]
        self.states = STATES
        self.idle()

    def idle(self):
        self.set_state(f"idle{self.idle_type}")

    def update(self, simulate_tick: bool = False) -> None:
        if self.busy_with_email:
            return

        super().update(simulate_tick)

        walking = self.current_state.state_name == "walk"
        if walking:
            self.move_xy()

        if self.respawn and datetime.now(timezone.utc) > self.respawn:
            # 1 in 8 chance to change direction
            if random.randint(1, 8) == 1:
                self.turn_around()

            chance = random.randint(1, 5)
            if chance > 3 or (chance > 2 and walking):
                self.set_state("walk")
            else:
                self.idle()

    async def playFor(self, socket=None, email: str = None):
        await socket.send(
            dumps(
                {
                    "action": "playMusic",
                    "id": self.id,
                    "openWindow": "itemChooser",
                    "filter": "itemType=" + "|".join(ALL_MUSICBLOCK_TYPES),
                    "windowTitle": "Play what for Crab?",
                }
            )
        )

    async def buyCrabpack(self, socket=None, email: str = None):
        metabolics = await self.get_metabolics(email)
        jukebox = cou_globals.items["musicblock_bag"]
        if metabolics.currants >= jukebox.price:
            metabolics.currants -= jukebox.price
            await self.set_metabolics(metabolics)
            await InventoryManager.add_item_to_user(email, jukebox, 1, self.id)
        else:
            await toast("You can't afford to do that", socket)

    def add_to_history(self, music):
        """
        Add a song to the history of the crab.
        If it is already in the list, it is moved to the end.
        """
        if music in self.listen_history:
            self.listen_history.remove(music)

        self.listen_history.append(music)

    def likes_song(self, music) -> bool:
        """
        "A crab likes a song if it is in the first half of the list
        (sorted oldest to newest).
        """
        return (
            music not in self.listen_history
            or self.listen_history.index(music) < len(self.listen_history) // 2
        )

    def until_respawn(self) -> timedelta:
        return self.respawn - datetime.now(timezone.utc)

    async def playMusic(self, socket = None, email = None, itemType = None, count = None, slot = None, subSlot = None):
        if self.busy_with_email:
            # Only 1 player at a time
            await self.say(ERR_BUSY)
            return

        assert socket
        assert email
        assert itemType and itemType in ALL_MUSICBLOCK_TYPES

        self.busy_with_email = email
        headphones = cou_globals.items["crabpod_headphones"]

        async def _take_musicblock():
            return await InventoryManager.take_item_from_user(email, slot, subSlot, 1)

        async def _give_musicblock():
            return await InventoryManager.add_item_to_user(email, cou_globals.items[itemType], 1)

        async def _give_headphones():
            return await InventoryManager.add_item_to_user(email, headphones, 1, self.id)

        async def _take_headphones() -> int:
            return await InventoryManager.take_any_items_from_user(email, headphones.itemType, 1)

        is_rare = itemType in MUSICBLOCK_RARES

        if not await _take_musicblock():
            # Could not take musicblock from player
            await self.say(ERR_NO_MUSIC)
            self.busy_with_email = ""
            return

        await _give_headphones()
        self.set_state("listen")

        await asyncio.sleep((rand_song_length() + self.until_respawn()).total_seconds())

        # Reward player
        if self.likes_song(itemType):
            # Dance for a bit
            self.set_state("like_on")
            await asyncio.sleep((rand_react_length() + self.until_respawn()).total_seconds())

            self.set_state("like_off")
            await _give_musicblock()

            if await _take_headphones() < 1:
                # Collectible headphones were stolen
                await self.say(WARN_HEADPHONES)
            else:
                # Headphones returned, award crabato juice
                await InventoryManager.add_item_to_user(email, cou_globals.items["crabato_juice"], 1, self.id)
        else:
            # Be crabby
            self.set_state("dislike_on")
            await asyncio.sleep((rand_react_length() + self.until_respawn()).total_seconds())

            self.set_state("like_off")
            await _give_musicblock()
            await _take_headphones()

        await self.try_set_metabolics(email, energy=-1, mood=1, img_min=(5 if is_rare else 1), img_range=2)

        # Affect future listens
        self.add_to_history(itemType)
        self.busy_with_email = ""
