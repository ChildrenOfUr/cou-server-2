from datetime import datetime, timedelta, timezone
import random
import json

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints import quest
from cou_server.endpoints.inventory import InventoryManager
from cou_server.endpoints.time import Clock
from cou_server.entities.action import Action, ItemRequirements, EnergyRequirements
from cou_server.entities.items.item import Item
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC
from cou_server.quests import messages
from cou_server.skills.skill_manager import SkillManager


class Piggy(NPC):
    SKILL = "animal_kinship"
    NIBBLE_ENERGY = -5
    PET_ENERGY = -4

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.pet_counts: dict[str, int] = {}
        self.nibble_counts: dict[str, int] = {}
        self.last_reset = datetime.now(timezone.utc)

        item_req = ItemRequirements(
            any=["broccoli", "cabbage", "carrot", "corn", "cucumber", "onion", "parsnip", "potato", "pumpkin", "rice", "spinach", "tomato", "zucchini"],
            error="You don't have anything that looks good right now",
        )
        self.actions.extend([
            Action(
                actionName="nibble", timeRequired=self.action_time, actionWord="nibbling", description="Have a little nibble",
                energyRequirements=EnergyRequirements(energy_amount=self.NIBBLE_ENERGY),
                associatedSkill=self.SKILL,
            ),
            Action(
                actionName="pet", timeRequired=self.action_time, actionWord="petting", description="Give the piggy a pet",
                energyRequirements=EnergyRequirements(energy_amount=self.PET_ENERGY),
                associatedSkill=self.SKILL,
            ),
            Action(
                actionName="feed", description="Feed the piggy some produce and see what it produces",
                itemRequirements=item_req, associatedSkill=self.SKILL,
            )
        ])
        self.type = "Piggy"
        self.speed = 75 # pixels per second
        self.renamable = True

        self.states = {
            "chew" : SpriteSheet("chew", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_chew_png_1354829433.png", 968, 310, 88, 62, 53, True),
            "look_screen" : SpriteSheet("look_screen", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_look_screen_png_1354829434.png", 880, 310, 88, 62, 48, False),
            "nibble" : SpriteSheet("nibble", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_nibble_png_1354829441.png", 880, 372, 88, 62, 60, False),
            "rooked1" : SpriteSheet("rooked1", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_rooked1_png_1354829442.png", 880, 62, 88, 62, 10, True),
            "rooked2" : SpriteSheet("rooked2", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_rooked2_png_1354829443.png", 704, 186, 88, 62, 24, False),
            "too_much_nibble" : SpriteSheet("too_much_nibble", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_too_much_nibble_png_1354829441.png", 968, 372, 88, 62, 65, False),
            "walk" : SpriteSheet("walk", "https://childrenofur.com/assets/entityImages/npc_piggy__x1_walk_png_1354829432.png", 704, 186, 88, 62, 24, True)
        }

        self.set_state("walk")

        self.responses = {
            "nibble": [
                "Ya bacon me crazy!",
            ],
            "pet": [
                "Do I boar you?",
            ],
        }

        MessageBus.subscribe("clock_on_new_day", lambda _: self._reset_lists())

    def _reset_lists(self) -> None:
        self.pet_counts.clear()
        self.nibble_counts.clear()
        self.last_reset = datetime.now(timezone.utc)

    def restore_state(self, metadata: dict) -> None:
        super().restore_state(metadata)

        if "petCounts" in self.metadata:
            self.pet_counts = json.loads(self.metadata["petCounts"])

        if "nibbleCounts" in self.metadata:
            self.nibble_counts = json.loads(self.metadata["nibbleCounts"])

        if "lastRest" in self.metadata:
            last_reset_ms = int(self.metadata["lastRest"])
            self.last_reset = datetime.utcfromtimestamp(last_reset_ms/1000)
            last_reset_clock = Clock(self.last_reset, stopped=True)
            current_clock = Clock(datetime.now(timezone.utc), stopped=True)
            if (last_reset_clock.day_int < current_clock.day_int) or (last_reset_clock.hour_int < 6 and current_clock.hour_int >= 6):
                self._reset_lists()

    def get_persist_metadata(self) -> dict[str, str]:
        persist_data = super().get_persist_metadata()
        persist_data["petCounts"] = json.dumps(self.pet_counts)
        persist_data["nibbleCounts"] = json.dumps(self.nibble_counts)
        persist_data["lastReset"] = self.last_reset.timestamp() * 1000

    async def _set_level_based_metabolics(self, level: int, action: str, email: str) -> bool:
        mood = 2
        img_min = 5
        energy = 0

        if action == "pet":
            energy = self.PET_ENERGY
        elif action == "nibble":
            energy = self.NIBBLE_ENERGY

        if level > 0:
            mood *= level + 1
            img_min *= level + 1
            energy //= level

        return await self.try_set_metabolics(email, energy=energy, mood=mood, img_min=img_min, img_range=4)

    async def nibble(self, socket = None, email: str = None) -> bool:
        level = await SkillManager.get_level(self.SKILL, email)
        success = await self._set_level_based_metabolics(level, "nibble", email)
        if not success:
            return False

        await StatManager.add(email, Stat.piggies_nibbled)
        await SkillManager.learn(self.SKILL, email)
        self.nibble_counts[email] = self.nibble_counts.get(email, 0) + 1
        MessageBus.publish("requirement_progress", messages.RequirementProgress("piggyNibble", email))
        if email in quest.quest_log_cache:
            # piggy nibbler quest
            await quest.quest_log_cache[email].offer_quest("Q11")

        # give the player the "fruits" of their labor
        odds = 100000
        count = 1
        if level == 7:
            count = 4
            odds = 3
        elif level > 4:
            count = 3
            odds = 10
        elif level > 3:
            odds = 20
        elif level > 1:
            count = 2
        if random.randint(0, odds - 1) == 7:
            count += 5

        await InventoryManager.add_item_to_user(email, cou_globals.items["meat"], count, self.id)

        self.set_state("nibble")
        await self.say(self.responses["nibble"][random.randint(0, len(self.responses["nibble"]) - 1)])

        # award achievements
        total_nibbled = await StatManager.get(email, Stat.piggies_nibbled)

        if total_nibbled >= 503:
            await Achievement.find("transrational_meat_aficionado").award_to(email)
        elif total_nibbled >= 137:
            await Achievement.find("ham_hocker").award_to(email)
        elif total_nibbled >= 41:
            await Achievement.find("bacon_biter").award_to(email)
        elif total_nibbled >= 17:
            await Achievement.find("piggy_nibbler").award_to(email)

        return True


    async def pet(self, socket = None, email: str = None) -> bool:
        level = await SkillManager.get_level(self.SKILL, email)
        success = await self._set_level_based_metabolics(level, "pet", email)
        if not success:
            return False

        await StatManager.add(email, Stat.piggies_petted)
        await SkillManager.learn(self.SKILL, email)
        self.pet_counts[email] = self.pet_counts.get(email, 0) + 1

        await self.say(self.responses["pet"][random.randint(0, len(self.responses["pet"]) - 1)])

        if email in quest.quest_log_cache:
            # piggy nibbler quest
            await quest.quest_log_cache[email].offer_quest("Q9")

        # award achievements
        total_petted = await StatManager.get(email, Stat.piggies_petted)

        if total_petted >= 137:
            await Achievement.find("pork_petter_extraordinaire").award_to(email)
        elif total_petted >= 41:
            await Achievement.find("swine_snuggler").award_to(email)
        elif total_petted >= 17:
            await Achievement.find("pork_fondler").award_to(email)

        return True

    async def feed(self, socket = None, email: str = None) -> bool:
        map = {
            "action": "feedItem",
            "id": self.id,
            "openWindow": "itemChooser",
            "filter": "category=Croppery & Gardening Supplies|||itemType=^(?!.+(?:_seed|_bean)).+$",
            "windowTitle": "Feed Piggy What?",
        }
        await socket.send(json.dumps(map))
        return True

    async def feedItem(self, socket = None, itemType: str = None, count: int = None, email: str = None, slot: int = None, subSlot: int = None):
        success = True # TODO Inventory.take_item_from_user(email, slot, sub_slot, count) is not None
        if not success:
            return False

        await StatManager.add(email, Stat.piggies_fed)
        await SkillManager.learn(self.SKILL, email)

        item: Item = Item.clone(cou_globals.items["piggy_plop"])
        item.metadata["seedType"] = itemType
        item.put_on_ground(self.x, self.y, self.street_name)
        self.set_state("chew", repeat=2)
        return True

    def update(self, simulate_tick: bool = False) -> None:
        """Will simulate piggy movement and send updates to clients if needed"""
        super().update()

        # update x and y
        if self.current_state.state_name == "walk":
            self.move_xy()

        # if respawn is in the past, it is time to choose a new animation
        if self.respawn is not None and self.respawn < datetime.now(timezone.utc):
            # 1 in 8 chance to change direction
            if random.randint(0, 7) == 1:
                self.facing_right = not self.facing_right

            num = random.randint(0, 19)
            if num == 6:
                self.set_state("look_screen")
            else:
                self.set_state("walk")

    async def customize_actions(self, email: str) -> list[Action]:
        ak_level = await SkillManager.get_level(self.SKILL, email)
        personal_actions: list[Action] = []
        for action in await super().customize_actions(email):
            personal_action = Action.clone(action)
            if action.actionName == "nibble":
                max_nibbles = 2 if ak_level >= 6 else 1

                # player must have petted first unless their AK level is at least 6
                if self.nibble_counts.get(email, 0) >= max_nibbles:
                    personal_action.enabled = False
                    personal_action.error = f"You can only nibble this piggy {'twice' if max_nibbles == 2 else 'once'} per day"
                elif ak_level < 6 and self.pet_counts.get(email, 0) == 0:
                    personal_action.enabled = False
                    personal_action.error = "Try petting first"

                if ak_level > 5:
                    personal_action.energyRequirements.energy_amount = 2
                elif ak_level > 2:
                    personal_action.energyRequirements.energy_amount = 4
            personal_actions.append(personal_action)

        return personal_actions
