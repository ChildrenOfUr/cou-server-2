from datetime import datetime
from random import randint
from json import dumps

from cou_server.common.global_gobbler import cou_globals
from cou_server.entities.action import Action
import cou_server.entities.items.item as items
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

FEED = Action("feed", actionWord="feeding")

MAX_BOB = 50
MIN_BOB = -MAX_BOB

STATES = {
    "chew": SpriteSheet(
        "chew",
        "https://childrenofur.com/assets/entityImages/npc_batterfly__x1_chew_png_1354831854.png",
        999,
        1344,
        111,
        96,
        120,
        False,
    ),
    "front_turned": SpriteSheet(
        "front_turned",
        "https://childrenofur.com/assets/entityImages/npc_batterfly__x1_front_turned_png_1354831847.png",
        888,
        480,
        111,
        96,
        40,
        True,
    ),
    "front_waiting": SpriteSheet(
        "front_waiting",
        "https://childrenofur.com/assets/entityImages/npc_batterfly__x1_front_waiting_png_1354831849.png",
        888,
        480,
        111,
        96,
        40,
        True,
    ),
    "fly_profile": SpriteSheet(
        "fly_profile",
        "https://childrenofur.com/assets/entityImages/npc_batterfly__x1_profile_png_1354831844.png",
        888,
        480,
        111,
        96,
        40,
        True,
    ),
    "fly_profile_turned": SpriteSheet(
        "fly_profile_turned",
        "https://childrenofur.com/assets/entityImages/npc_batterfly__x1_profile_turned_png_1354831846.png",
        888,
        480,
        111,
        96,
        40,
        True,
    ),
}


class Batterfly(NPC):
    current_bob: int = 0
    bobbing_up: bool = False

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.type = "Batterfly"
        self.speed = 75
        self.can_rename = True
        self.actions += [FEED]
        self.states = STATES
        self.set_state("fly_profile")
        self.facing_right = True

    async def feed(self, socket=None, email: str = None):
        await socket.send(
            dumps(
                {
                    "id": self.id,
                    "action": "feedItem",
                    "openWindow": "itemChooser",
                    "filter": "consumeValues={.*energy:.*}",
                    "windowTitle": "Feed Batterfly what?",
                }
            )
        )
        return True

    def feed_item(
        self,
        item_type: str,
        count: int,
        socket=None,
        email: str = None,
        slot: int = None,
        sub_slot: int = None,
    ):
        # TODO: await InventoryV2.takeAnyItemsFromUser(email, itemType, count) != null
        success = True

        if not success:
            return False

        energy_worth = (cou_globals.items[item_type].consume_values["energy"] or 0) * count
        guano_count = 0

        if 15 <= energy_worth < 50:
            guano_count = 1
        elif 50 <= energy_worth < 100:
            guano_count = 2
        elif energy_worth >= 100:
            guano_count = 3

        for i in range(0, guano_count):
            item = items.Item.clone(cou_globals.items["guano"])
            item.put_on_ground(self.x, self.y, self.street_name)

        self.set_state("chew", repeat=2)
        return True

    def update(self, simulate_tick: bool = False) -> None:
        super().update(simulate_tick)

        if "fly" in self.current_state.state_name:
            self.move_xy(y_action=self.y_action, ledge_action=self.ledge_action)

        # If respawn is in the past, it is time to choose a new animation
        if self.respawn and self.respawn > datetime.utcnow():
            # 1 in 4 chance to change direction
            if randint(1, 4) == 1:
                self.turn_around()

            self.set_state("fly_profile", repeat=randint(0, 5))

    def y_action(self):
        # Bob up and down a bit

        if self.bobbing_up:
            self.y -= 1
            self.current_bob -= 1

            if self.current_bob < MIN_BOB:
                self.bobbing_up = False
        else:
            self.y += 1
            self.current_bob += 1

            if self.current_bob > MAX_BOB:
                self.bobbing_up = True

    def ledge_action(self):
        pass
