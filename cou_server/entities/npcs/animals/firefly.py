from datetime import datetime, timedelta, timezone
from random import randint

from cou_server.common import get_logger
from cou_server.common.util import toast
from cou_server.endpoints.inventory import InventoryManager
from cou_server.endpoints.time import Clock
from cou_server.entities.action import ItemRequirements, EnergyRequirements, Action
from cou_server.entities.spritesheet import SpriteSheet
from cou_server.entities.npcs.npc import NPC

logger = get_logger(__name__)

REQ_ITEM = ItemRequirements()
REQ_ITEM.any = ["firefly_jar"]
REQ_ITEM.error = "Fireflies won't stay in your hands. You need a jar."

REQ_ENERGY = EnergyRequirements(3)
REQ_ENERGY.error = "Chasing fireflies is hard work, so you'll need at least 3 energy."

COLLECT = Action("collect", actionWord="chasing")
COLLECT.itemRequirements = REQ_ITEM
COLLECT.energyRequirements = REQ_ENERGY


STATES = {
    "fullPath": SpriteSheet(
        "fullPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_fullPath_png_1354833043.png",
        870,
        360,
        87,
        40,
        89,
        True,
    ),
    "halfPath": SpriteSheet(
        "halfPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_halfPath_png_1354833044.png",
        870,
        160,
        87,
        40,
        40,
        True,
    ),
    "smallPath": SpriteSheet(
        "smallPath",
        "https://childrenofur.com/assets/entityImages/npc_firefly__x1_smallPath_png_1354833044.png",
        870,
        80,
        87,
        40,
        20,
        True,
    ),
}


class Firefly(NPC):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.clock = Clock()
        self.type = "Firefly"
        self.action_time = 4000
        self.actions += [COLLECT]
        self.speed = 5  # px/s
        self.states = STATES
        self.set_state("fullPath")

    async def collect(self, socket = None, email: str = None) -> bool:
        adding = randint(1, 4)
        skipped = adding

        try:
            await InventoryManager.add_firefly_to_jar(email, socket, amount=adding)
            skipped = 0
        except Exception:
            logger.warning(f"Could not collect fireflies for <email={email}>")

        added = adding - skipped

        if added == 0:
            await toast("You don't have any room in your jar!", socket)
            return False
        else:
            await toast(
                f"You caught {added} firefl{'y' if added == 1 else 'ies'}", socket
            )

            await self.try_set_metabolics(email, energy=-COLLECT.energyRequirements.energy_amount)

            # Small flight path for 10 seconds
            self.set_state("smallPath")

            return True

    def update(self, simulate_tick: bool = False):
        super().update(simulate_tick)

        am = "am" in self.clock.time
        hour = int(self.clock.time.split(":")[0])
        minute = int(self.clock.time.split(":")[1][0:2])

        if (am and hour < 6) or (not am and hour > 8 and minute >= 30):
            # Firefly time is 8:30 PM to 6:00 AM

            # If respawn is in the past, it is time to choose a new animation
            if self.respawn and datetime.now(timezone.utc) > self.respawn:
                # 50% chance to move the other way...gradually
                if randint(0, 1) == 0:
                    self.turn_around()

                path_size = randint(0, 4)
                if 0 <= path_size <= 1:
                    self.set_state("fullPath")
                elif 2 <= path_size <= 3:
                    self.set_state("halfPath")
                elif path_size == 4:
                    self.set_state("smallPath")

                # Stay for 10 seconds
                length = 10000 * (self.current_state.num_frames / 30 * 1000) // 1
                self.respawn = datetime.now(timezone.utc) + timedelta(milliseconds=length)
