from datetime import datetime, timedelta
from random import choice, randint

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action, EnergyRequirements
from cou_server.entities.npcs.npc import NPC, SpriteSheet
from cou_server.skills.skill_manager import SkillManager


MASSAGE = Action("massage", actionWord="massaging")
MASSAGE.energyRequirements = EnergyRequirements(7)

MILK = Action("milk", actionWord="milking")
MILK.energyRequirements = EnergyRequirements(5)

MAX_BOB = 50
MIN_BOB = -MAX_BOB

MASSAGE_INTERVAL = timedelta(minutes=5)

RESPONSES = {
    "massage": [
        "Mmmf. Not bad.",
        "Massage ok. 'Spose.",
        "Like, whatever",
        "K, thanx.",
        "Askimble ubble gite nud razzafrazza rhubarb",
        "Ruttle snottfig squeebug fallgite schmee rugger",
        "Nookle flubber wezzent tie lupose rhubarb flap",
        "Meh fat strop portoff frite pice",
        "Flitup pickr rhubarb tokayai poze",
        "Yeah. So can I go hang with my butterfriends now?",
        "Unmf",
        "Ok, so, great massage. Can I, like, go?",
        "Yeah, so, thanks. I guess.",
        "Yeah, so-so, whatevs, k bai.",
        "rutter flubbtoo spose quite nice rhubarb twonk",
        "buffrum gish flih not bad frakock kweezle",
        "nickpu flotter meh not unpleasant frumk",
        "croopy picklr so thass okay iffoo mumble nunk",
        "rhubarb tosstpottee I like it soafcan dowitmoar",
        "So um I dunno? Like, I guess I liked that? Kinda?",
        "Yeah, so that was like, not bad? I suppose? Thanks?",
        "So, like, that was quite good?",
        "I mean, whatever and everything? But that was ok and stuff?",
        "Well, totally not unpleasant? So yeah? whatever?",
        "Like, whatever?…",
        "Yeah, so, um. Ok? Thanks?",
        "Yeah? So nice massage? Whatever?",
        "So, I mean, the massage? It was, like, ok? Thanks?",
        "Right, so I liked your massage? So, like, thanks?",
        "Thnx, Bigthing.",
        "OMG ur like, gr8",
        "AWSUM msrg.",
        "i <3 u + ur msrgs",
        "K thnx ttyl :)",
        "Msrg! I <3 msrgs! \\o/",
        "\\o/",
        "LOL! Ur, like, all msrgy!",
        "i <333 u!!!!1!",
        "Thnx!",
        "K THNX 4 MSRG BAI!",
        "Ur nice 4 msrgin me. xoxo",
        "OMG UR AWSM! \\o/",
        "<333 msrgs",
        "Yay thnx bigthing. ttyl…",
        "So I'm like, happy? So, um, you should be happy too?",
        "Yeah, so, massages make everyone happy, right?",
        "Like, I can totes be nice too, yeah?",
        "So yeah? You're nice? Be, um, happy, yeah?",
        "Ok, so, ping!?! Are you, like, happier too now?",
        "Ugh. Good moods are so uncool? You have mine.",
        "Like, whatever? But, so, you're awesome?",
        "So, yeah. You should be, like, happy?",
        "You're, like, awesome. Cheer up, yeah?",
        "Whatever? Yeah? Ok so, like, thanks!",
        "OMG ur gr8! Mood++!",
        "TY! i <3 ur msrg! ur awsm!",
        "U wnt ++mood? U gets!",
        "Yay, thnx! I <3 msrgs!",
        "LOL! I likes you LOADS!",
        ":) +++++++++",
        "i <3 ur msrgs <------------------> mch!",
        "i <3333333333 u!",
        "Mood+++!!!!!!!1!",
        "\\o/ Yaaaaaaaay!!!! R u happy?",
    ],
    "massageFail": [
        "Ow! Why not just rub me with gravel?!",
        "OMFG! 2 DRY! UR rubbish.",
        "Ooo, ow, no, bad, stoppit.",
        "Jeez, if you can't do it properly…",
        "Git ur dry hands off! Yow!",
        "ruzzle fruzza bugroff mumble",
        "noliyk frakkig soddov rhubarble",
        "watta grunkle peff",
        "razzafrazzin digdassurdy",
        "Yeahright.",
    ],
    "milk": [
        "Whoa milky!",
        "Here: milx",
        "Milx 4 U",
        "Got milx?",
        "Milky milky",
        "Here. S'good for growing teeth and stuff. And tails.",
        "Fruzzup air oogoh merp",
        "Kruffin ilx ans uff",
        "Toffuzzin rappat ulk",
        "Rufflin bilky mong. Urk.",
        "Pufflunk norky tonk rnmrnmrnm",
        "I got some containers too!",
        "OMG you again! Take milk. K?",
        "There. Milx. Happy? Bai.",
        "UR OK. U can has milx.",
        "K, so I has milx. You wants?",
        "Here! Made you beer! I kiddin'. Is milx.",
        "Pikgug fup here y'are urk fopple",
        "Snuggurp have these enflurkle",
        "Runkle some milk then flub rmnrnmrnmrm",
        "Glubfoo milk you wanted rhuburble bunk",
        "Snurfle milk, yeah? Ok, ruffgrm mnurmnur.",
        "So you wanted milk, yeah? Like, whatever?",
        "Yeah, like, you can have this milk? Like, totally?",
        "Whatever, yeah? So I made you milk?",
        "Right, so, like, milk? There? Have it?",
        "So like you can have milk again sometime? Or whatever?",
        "Right, so here's your milk?",
        "Alright so yeah, whatever? Here's your milk?",
        "So like, somehow you milked me, and I made this?",
        "Hey, you want this? I guess? 'Cos you milked me?",
        "Whatever and that, but take this milk, yeah?",
        "OMG I TOTES DID U MILX!",
        "i got milx 4 u!!!!!1!!",
        "Milx r awsum! U r awsum!",
        "i <3 u! milx! ttyl!!!1!",
        "gt milx? Ys! ROFL!!!!1!!",
        "You needs milx! You totes HAZ milx!!!",
        "1t milx? Gotz milx! YAY!",
        "I maded milx.",
        "Look @ my milx! U can haz!!!!!!!!!!",
        "U <3 milx? I <3 u!!!",
        "YAY I MADEZ YOU MILX!!!1!",
        "Here iz milx! l8r!!!!",
        "U likez ur milx? YAY! Bai!!!",
        "All theez milxez r 4 u! <3333",
        "Milx! Enjoi! xoxo",
    ],
    "milkExtra": [
        "Don't let it go to your head, yeah? I just have extra.",
        "Yeah, extra milk, whatevs. Doesn't mean we're friends.",
        "So I made you extra milk? Like? Whatever?",
        "Yeah, so, like, you want some extra or what?",
        "Like, you want extra? I made extra.",
        "You can totes have this extra milk? I don't want it.",
        "You want extra milks? Like, whatevs.",
        "I'm totally super-milky right now. Don't wanna talk about it.",
        "K, so whatevs, but I made you extra today?",
        "You can have extra, I guess, 'cos you're ok.",
        "Yeah, so I'm, like, super-milky? TMI? Want extra?",
        "I have, like, too much milk. You can have more.",
        "Xtra-milx 4 u 2day!",
        "OMG! 8-O Supr-milx!!!!!",
        "Yayz! Multi-milx!",
        "o_O!!!!!!1! So many milx!",
        "O rly? Moar milx? OKAYZ!",
        "i <<<3 u THS much milx!",
        "++++++++++milx. Srsly.",
        "u <3 milx? i <3 u.",
        "OMG! +++++Milx!",
        "Lotsamilx. k? kewl. ttyl!!!!1!!",
    ],
    "milkFail": [
        (
            "What? No warmup? No preamble?"
            "You just walk up to a butterfly with your clammy hands and try to milk it?"
            "You have a lot to learn about charming butterflies."
        )
    ],
}

STATES = {
    "fly-angle1": SpriteSheet(
        "fly-angle1",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_fly-angle1_png_1354829526.png",
        840,
        195,
        70,
        65,
        34,
        True,
    ),
    "fly-angle2": SpriteSheet(
        "fly-angle2",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_fly-angle2_png_1354829527.png",
        700,
        130,
        70,
        65,
        20,
        True,
    ),
    "fly-rooked": SpriteSheet(
        "fly-rooked",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_fly-rooked_png_1354829525.png",
        980,
        65,
        70,
        65,
        14,
        True,
    ),
    "fly-side": SpriteSheet(
        "fly-side",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_fly-side_png_1354829525.png",
        980,
        390,
        70,
        65,
        84,
        True,
    ),
    "fly-top": SpriteSheet(
        "fly-top",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_fly-top_png_1354829528.png",
        910,
        455,
        70,
        65,
        87,
        True,
    ),
    "rest-angle1": SpriteSheet(
        "rest-angle1",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_rest-angle1_png_1354829530.png",
        420,
        65,
        70,
        65,
        6,
        True,
    ),
    "rest-angle2": SpriteSheet(
        "rest-angle2",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_rest-angle2_png_1354829531.png",
        700,
        65,
        70,
        65,
        10,
        True,
    ),
    "rest-top": SpriteSheet(
        "rest-top",
        "https://childrenofur.com/assets/entityImages/npc_butterfly__x1_rest-top_png_1354829532.png",
        980,
        195,
        70,
        65,
        42,
        True,
    ),
}


class Butterfly(NPC):
    interacting: bool = False
    num_milks: int = 0
    current_bob: int = 0
    bobbing_up: bool = True
    last_massage: datetime = None

    def __init__(self, entity_id, x, y, z, rotation, h_flip, street_name):
        super().__init__(entity_id, x, y, z, rotation, h_flip, street_name)

        self.type = "Butterfly"
        self.actions += [MASSAGE, MILK]
        self.speed = 75  # px/s
        self.can_rename = True
        self.states = STATES

    @property
    def massaged(self):
        return (
            self.last_massage
            and datetime.utcnow() < self.last_massage + MASSAGE_INTERVAL
        )

    async def massage(self, socket=None, email: str = None):
        success = await self.try_set_metabolics(email, energy=-7, mood=3, img_min=5, img_range=3)

        if not success:
            return False

        self.interacting = True

        has_lotion = True  # TODO: InventoryV2.hasItem(email, 'butterfly_lotion', 1)
        if not has_lotion:
            await self.say(choice(RESPONSES["massageFail"]))
        else:
            # Increment stat
            await StatManager.add(email, Stat.butterflies_massaged)

            # Say a witty thing
            await self.say(choice(RESPONSES["massage"]))
            self.last_massage = datetime.utcnow()
            self.num_milks = 0

            # Award achievements
            await StatManager.get(email, Stat.butterflies_massaged)
            total_massaged = 0

            if total_massaged >= 503:
                await Achievement.find("nighmystical_lepidopteral_manipulator").award_to(email)
            elif total_massaged >= 137:
                await Achievement.find("master_lepidopteral_manipulator").award_to(email)
            elif total_massaged >= 41:
                await Achievement.find("practical_lepidopteral_manipulator").award_to(email)
            elif total_massaged >= 23:
                await Achievement.find("apprentice_lepidopteral_manipulator").award_to(email)
            elif total_massaged >= 3:
                await Achievement.find("butterfly_whisperer").award_to(email)

            self.interacting = False

    async def milk(self, socket=None, email: str = None):
        skilled = await SkillManager.get_level('animal_kinship', email) >= 6
        if self.massaged or skilled:
            success = await self.try_set_metabolics(email, energy=-5, mood=5, img_min=5, img_range=6)

            if not success:
                return False

            self.interacting = True
            qty: int

            if randint(1, 10) == 1:
                # Bonus milk
                qty = 3
                await self.say(choice(RESPONSES["milkExtra"]))
            else:
                qty = 3
                await self.say(choice(RESPONSES["milk"]))

            await InventoryManager.add_item_to_user(email, cou_globals.items["butterfly_milk"], qty, self.id)
            await StatManager.add(email, Stat.butterflies_milked)
        else:
            # Not massaged
            success = await self.try_set_metabolics(email, energy=-5, mood=-2, img_min=5, img_range=2)

            if not success:
                return False

            await self.say(choice(RESPONSES["milkFail"]))

        self.interacting = False
        return True

    def update(self, simulate_tick: bool = False):
        super().update(simulate_tick)

        if self.current_state.state_name == "fly-side" and not self.interacting:
            self.move_xy(y_action=self.y_action, ledge_action=self.ledge_action)

        # If respawn is in the past, it is time to choose a new animation
        if self.respawn and datetime.utcnow() > self.respawn:
            # 1 in 4 chance to change direction
            if randint(1, 4) == 1:
                self.turn_around()

            self.set_state("fly-side", repeat=randint(0, 5))

    def y_action(self):
        # Bob up and down a bit

        if self.bobbing_up:
            self.y -= 1
            self.current_bob -= 1

            if self.current_bob < MIN_BOB:
                self.bobbing_up = False
        else:
            self.y += 1
            self.current_bob += 1

            if self.current_bob > MAX_BOB:
                self.bobbing_up = True

    def ledge_action(self):
        pass
