import json

from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.util import toast
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.action import Action
from cou_server.entities.entity import create_id
from cou_server.entities.npcs.npc import NPC
from cou_server.entities.street_entities.street_entity import StreetEntity
from cou_server.entities.street_entities.street_entities import StreetEntities
from cou_server.models.user import User


LOGGER = get_logger(__name__)


class EntityItem(NPC):
    """Used to convert items to street entities. For example, stills and gnomes"""

    # Maps item type to entity type
    ITEM_ENTITIES: dict[str, str] = {
        "cubimal_batterfly": "RacingCubimal_batterfly",
        "cubimal_bureaucrat": "RacingCubimal_bureaucrat",
        "cubimal_butler": "RacingCubimal_butler",
        "cubimal_butterfly": "RacingCubimal_butterfly",
        "cubimal_cactus": "RacingCubimal_cactus",
        "cubimal_chick": "RacingCubimal_chick",
        "cubimal_crab": "RacingCubimal_crab",
        "cubimal_craftybot": "RacingCubimal_craftybot",
        "cubimal_deimaginator": "RacingCubimal_deimaginator",
        "cubimal_dustbunny": "RacingCubimal_dustbunny",
        "cubimal_emobear": "RacingCubimal_emobear",
        "cubimal_factorydefect_chick": "RacingCubimal_factorydefect_chick",
        "cubimal_firebogstreetspirit": "RacingCubimal_firebogstreetspirit",
        "cubimal_firefly": "RacingCubimal_firefly",
        "cubimal_fox": "RacingCubimal_fox",
        "cubimal_foxranger": "RacingCubimal_foxranger",
        "cubimal_frog": "RacingCubimal_frog",
        "cubimal_gardeningtoolsvendor": "RacingCubimal_gardeningtoolsvendor",
        "cubimal_gnome": "RacingCubimal_gnome",
        "cubimal_greeterbot": "RacingCubimal_greeterbot",
        "cubimal_groddlestreetspirit": "RacingCubimal_groddlestreetspirit",
        "cubimal_gwendolyn": "RacingCubimal_gwendolyn",
        "cubimal_helga": "RacingCubimal_helga",
        "cubimal_hellbartender": "RacingCubimal_hellbartender",
        "cubimal_ilmenskiejones": "RacingCubimal_ilmenskiejones",
        "cubimal_juju": "RacingCubimal_juju",
        "cubimal_magicrock": "RacingCubimal_magicrock",
        "cubimal_maintenancebot": "RacingCubimal_maintenancebot",
        "cubimal_mealvendor": "RacingCubimal_mealvendor",
        "cubimal_phantom": "RacingCubimal_phantom",
        "cubimal_piggy": "RacingCubimal_piggy",
        "cubimal_rook": "RacingCubimal_rook",
        "cubimal_rube": "RacingCubimal_rube",
        "cubimal_scionofpurple": "RacingCubimal_scionofpurple",
        "cubimal_senorfunpickle": "RacingCubimal_senorfunpickle",
        "cubimal_sloth": "RacingCubimal_sloth",
        "cubimal_smuggler": "RacingCubimal_smuggler",
        "cubimal_snoconevendor": "RacingCubimal_snoconevendor",
        "cubimal_squid": "RacingCubimal_squid",
        "cubimal_toolvendor": "RacingCubimal_toolvendor",
        "cubimal_trisor": "RacingCubimal_trisor",
        "cubimal_unclefriendly": "RacingCubimal_unclefriendly",
        "cubimal_uraliastreetspirit": "RacingCubimal_uraliastreetspirit",
        "cubimal_yeti": "RacingCubimal_yeti",
        "icon_of_alph": "Icon_Alph",
        "icon_of_cosma": "Icon_Cosma",
        "icon_of_friendly": "Icon_Friendly",
        "icon_of_grendaline": "Icon_Grendaline",
        "icon_of_humbaba": "Icon_Humbaba",
        "icon_of_lem": "Icon_Lem",
        "icon_of_mab": "Icon_Mab",
        "icon_of_pot": "Icon_Pot",
        "icon_of_spriggan": "Icon_Spriggan",
        "icon_of_tii": "Icon_Tii",
        "icon_of_zille": "Icon_Zille",
        "still": "Still",
    }

    ACTION_PICKUP = Action.with_name("pick up")

    @classmethod
    def get_class_for_item(cls, item_type: str) -> str:
        return cls.ITEM_ENTITIES.get(item_type)

    @classmethod
    def get_item_for_class(cls, class_name: str) -> str:
        return next((item_type for item_type in cls.ITEM_ENTITIES if item_type == class_name), None)

    @classmethod
    async def place(cls, email: str, itemType: str, tsid: str) -> bool:
        # find entity type for this item type
        type = cls.ITEM_ENTITIES.get(itemType)

        # save owner
        user_id = await User.get_id_from_email(email)
        metadata = json.dumps({"ownerId": user_id})

        # find street and position
        player = cou_globals.users[await User.get_username_from_email(email)]
        id = create_id(player.current_x, player.current_y, type, tsid)

        # save to database and load into game
        entity = StreetEntity(
            id, type, tsid, x=player.current_x, y=player.current_y, metadata_json=metadata,
        )
        return await StreetEntities.set_entity(entity)

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.item_type = None
        self.strict_pickup = False
        self.owner_id = -1
        self.speed = 0
        self.facing_right = True

    def update(self, simulate_tick: bool = False) -> None:
        # fall to platforms
        super().update()
        self.move_xy()

    async def customize_actions(self, email: str) -> list:
        if self.owner_id == -1 or (await User.get_id_from_email(email)) == self.owner_id:
            return [self.ACTION_PICKUP]
        return self.actions

    def get_persist_metadata(self) -> dict:
        return super().get_persist_metadata() | {"ownerId": str(self.owner_id), "itemType": self.item_type}

    def restore_state(self, metadata: dict):
        super().restore_state(metadata)
        self.owner_id = int(metadata.get("ownerId", -1))
        self.item_type = metadata.get("itemType", self.item_type)

    async def pickUp(self, socket = None, email: str = None) -> bool:
        if email != User.get_email_from_id(self.owner_id):
            await toast("That's not yours!", socket)
            return False

        try:
            if self.strict_pickup:
                # delete from street, then add to inventory
                if await StreetEntities.delete_entity(self.id):
                    return await InventoryManager.add_item_to_user(email, self.item_type, 1) == 1
            else:
                # add to inventory, then delete from street
                if await InventoryManager.add_item_to_user(email, self.item_type, 1) == 1:
                    return await StreetEntities.delete_entity(self.id)
            return False
        except Exception as e:
            LOGGER.warning(f"Could not pick up <ownerId={self.owner_id}> entity for item <itemType={self.item_type}>: {e}")
            return False
