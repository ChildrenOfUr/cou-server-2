import asyncio

from cou_server.common import get_logger
from cou_server.common.util import toast
from cou_server.entities.action import Action
from cou_server.entities.entity import SpriteSheet
from cou_server.entities.npcs.items.entity_item import EntityItem
from cou_server.metabolics.metabolics_change import MetabolicsChange
from cou_server.models.user import User


LOGGER = get_logger(__name__)


class Icon(EntityItem, MetabolicsChange):
    SPRITESHEETS = {
        "Alph": SpriteSheet("Alph", "https://childrenofur.com/assets/entityImages/icon_alph__x1_1_png_1354836409.png", 86, 103, 86, 103, 1, True),
        "Cosma": SpriteSheet("Cosma", "https://childrenofur.com/assets/entityImages/icon_cosma__x1_1_png_1354836412.png", 94, 92, 94, 92, 1, True),
        "Friendly": SpriteSheet("Friendly", "https://childrenofur.com/assets/entityImages/icon_friendly__x1_1_png_1354836414.png", 85, 87, 85, 87, 1, True),
        "Grendaline": SpriteSheet("Grendaline", "https://childrenofur.com/assets/entityImages/icon_grendaline__x1_1_png_1354836417.png", 81, 96, 81, 96, 1, True),
        "Humbaba": SpriteSheet("Humbaba", "https://childrenofur.com/assets/entityImages/icon_humbaba__x1_1_png_1354836421.png", 79, 93, 79, 93, 1, True),
        "Lem": SpriteSheet("Lem", "https://childrenofur.com/assets/entityImages/icon_lem__x1_1_png_1354836425.png", 88, 93, 88, 93, 1, True),
        "Mab": SpriteSheet("Mab", "https://childrenofur.com/assets/entityImages/icon_mab__x1_1_png_1354836427.png", 78, 95, 78, 95, 1, True),
        "Pot": SpriteSheet("Pot", "https://childrenofur.com/assets/entityImages/icon_pot__x1_1_png_1354836430.png", 104, 89, 104, 89, 1, True),
        "Spriggan": SpriteSheet("Spriggan", "https://childrenofur.com/assets/entityImages/icon_spriggan__x1_1_png_1354836433.png", 69, 94, 69, 94, 1, True),
        "Tii": SpriteSheet("Tii", "https://childrenofur.com/assets/entityImages/icon_tii__x1_1_png_1354836435.png", 86, 77, 86, 77, 1, True),
        "Zille": SpriteSheet("Zille", "https://childrenofur.com/assets/entityImages/icon_zille__x1_1_png_1354836438.png", 97, 90, 97, 90, 1, True),
    }

    PRICE_OF_TITHE = 100
    PRICE_OF_ACTION = 30

    ACTION_TITHE = Action(actionName="tithe", description=f"Insert {PRICE_OF_TITHE} currants to support the Icon")
    ACTION_RUMINATE = Action(actionName="ruminate", description="Soak up the happysauce emanating from the Icon", timeRequired=11000)
    ACTION_REVERE = Action(actionName="revere", description="Let the Icon replenish you while you adore it", timeRequired=11000)
    ACTION_REFLECT = Action(actionName="reflect", description="Dwell a while on the true meaning of the Icon", timeRequired=11000)

    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.username = None
        self.email = None
        self.currants = 0
        self.states = self.SPRITESHEETS
        self.actions = []
        self.speed = 0
        self.item_type = EntityItem.get_item_for_class(self.__class__.__name__)

        def init_done(result: asyncio.Future):
            try:
                if exception := task.exception():
                    raise exception
            except Exception as e:
                LOGGER.warning(f"Could not load Icon <id={self.id}>: {e}")

        task = asyncio.create_task(self._init())
        task.add_done_callback(init_done)

    async def _init(self) -> None:
        # fill in missing info
        self.username = await User.get_username_from_id(self.owner_id)
        self.email = await User.get_email_from_id(self.owner_id)

    def get_persist_metadata(self) -> dict:
        return super().get_persist_metadata() | {"currants": str(self.currants)}

    def restore_state(self, metadata: dict):
        super().restore_state(metadata)
        self.currants = int(metadata.get("currants", 0))

    async def customize_actions(self, email: str) -> list:
        custom_actions = await super().customize_actions(email)
        actions = [Action.clone(action) for action in custom_actions]

        if self.currants > self.PRICE_OF_ACTION:
            actions.extend([self.ACTION_RUMINATE, self.ACTION_REVERE, self.ACTION_REFLECT])
        elif (await self.get_metabolics(email)).currants >= self.PRICE_OF_TITHE:
            actions.append(self.ACTION_TITHE)

        return actions

    async def tithe(self, socket = None, email: str = None) -> bool:
        success = await self.try_set_metabolics(email, currants=-self.PRICE_OF_TITHE)

        if success:
            self.currants += self.PRICE_OF_TITHE

        return success

    async def ruminate(self, socket = None, email: str = None) -> bool:
        success = await self.try_set_metabolics(email, mood=50)

        if success:
            self.currants -= self.PRICE_OF_ACTION
            mood = self.gains["mood"]
            if mood > 0:
                await toast(f"The {self.type} has bestowed {mood} mood upon you.", socket)
            else:
                await toast(f"The {self.type} tried to bestow mood upon you, but you didn't need any.", socket)

        return success

    async def revere(self, socket = None, email: str = None) -> bool:
        success = await self.try_set_metabolics(email, energy=50)

        if success:
            self.currants -= self.PRICE_OF_ACTION
            energy = self.gains["energy"]
            if energy > 0:
                await toast(f"The {self.type} has bestowed {energy} energy upon you.", socket)
            else:
                await toast(f"The {self.type} tried to bestow energy upon you, but you didn't need any.", socket)

        return success

    async def reflect(self, socket = None, email: str = None) -> bool:
        success = await self.try_set_metabolics(email, img_min=50)

        if success:
            self.currants -= self.PRICE_OF_ACTION
            await toast(f"The {self.type} has bestowed {self.gains['img']} iMG upon you.", socket)

        return success


class Icon_Alph(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Alph"
        self.set_state("Alph")


class Icon_Cosma(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Cosma"
        self.set_state("Cosma")


class Icon_Friendly(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Friendly"
        self.set_state("Friendly")


class Icon_Grendaline(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Grendaline"
        self.set_state("Grendaline")


class Icon_Humbaba(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Humbaba"
        self.set_state("Humbaba")


class Icon_Lem(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Lem"
        self.set_state("Lem")


class Icon_Mab(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Mab"
        self.set_state("Mab")


class Icon_Pot(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Pot"
        self.set_state("Pot")


class Icon_Spriggan(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Spriggan"
        self.set_state("Spriggan")


class Icon_Tii(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Tii"
        self.set_state("Tii")


class Icon_Zille(Icon):
    def __init__(self, id, x, y, z, rotation, h_flip, street_name):
        super().__init__(id, x, y, z, rotation, h_flip, street_name)
        self.type = "Icon of Zille"
        self.set_state("Zille")
