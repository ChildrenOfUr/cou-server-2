import asyncio
import dataclasses
from datetime import datetime
import json

from quart import Blueprint, jsonify
from quart_cors import cors
from sqlalchemy.sql import text

from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.serializable import serializable
from cou_server.common.database import remote_engine
from cou_server.endpoints import mapdata
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.items.item import Item
from cou_server.models.user import User
from cou_server.skills.skill_manager import SkillManager


notes_blueprint = Blueprint("notes_blueprint", __name__)
cors(notes_blueprint)


LOGGER = get_logger(__name__)


@serializable
@dataclasses.dataclass
class Note:
    id: int = None
    author_id: int = None
    username: str = None
    title: str = None
    body: str = None
    timestamp: datetime = None

    def __post_init__(self):
        self.title = self.title.strip()
        if len(self.title) > NoteManager.TITLE_LENGTH_MAX:
            self.title = self.title[:NoteManager.TITLE_LENGTH_MAX]

    async def get_map(self) -> dict:
        if self.username is None:
            self.username = await User.get_username_from_id(self.author_id)
        return self.to_json()


class NoteManager:
    TITLE_LENGTH_MAX = 30
    TOOL_ITEM = "quill"
    PAPER_ITEM = "paper"
    NOTE_ITEM = "note"
    FORTUNE_COOKIE_ITEM = "fortune_cookie"
    FORTUNE_COOKIE_WITHFORTUNE_ITEM = "fortune_cookie_withfortune"
    FORTUNE_ITEM = "fortune"
    WRITING_SKILL = "penpersonship"

    @classmethod
    async def find(cls, id: int) -> Note:
        try:
            async with remote_engine.connect() as conn:
                query = text("SELECT * FROM notes WHERE id = :id")
                data = (await conn.execute(query, parameters={"id": id})).mappings().first()
                if not data:
                    return None
                return Note(**data)
        except Exception:
            LOGGER.exception(f"Could not find note {id}")

    @classmethod
    async def add(cls, note: Note) -> Note:
        if note.id is None or note.id == -1:
            # Adding a new note
            try:
                user_id = await User.get_id_from_username(note.username.strip())
                async with remote_engine.connect() as conn:
                    query = text(
                        "INSERT INTO notes (author_id, title, body)"
                        " VALUES (:author_id, :title, :body) RETURNING *"
                    )
                    params = {"author_id": user_id, "title": note.title.strip(), "body": note.body}
                    data = (await conn.execute(query, parameters=params)).mappings().first()
                    await conn.commit()
                    return Note(**data)
            except Exception:
                LOGGER.exception(f"Could not add note {note}")
                return None
        else:
            # Updating an existing note
            try:
                async with remote_engine.connect() as conn:
                    query = text("UPDATE notes SET title = :title, body = :body WHERE id = :id RETURNING *")
                    params = {"id": note.id, "title": note.title.strip(), "body": note.body}
                    data = (await conn.execute(query, parameters=params)).mappings().first()
                    await conn.commit()
                    return Note(**data)
            except Exception:
                LOGGER.exception(f"Could not edit note {note}")
                return None

    @classmethod
    async def add_from_client(cls, note_data: dict) -> dict:
        try:
            email = await User.get_email_from_username(note_data["username"])
            created = Note(**note_data)
            if created.id != -1 and SkillManager.get_level(cls.WRITING_SKILL, email) == 0:
                # Trying to edit a note without the required skill
                return {"error": "You don't know enough about Penpersonship to edit notes yet!"}

            # add data to database
            added = await cls.add(created)

            if created.id == -1:
                # a new note!

                # add item to inventory
                if await InventoryManager.take_any_items_from_user(email, cls.PAPER_ITEM, 1) != 1:
                    # missing paper from inventory (dropped between quill action and saving note?)
                    return {"error": "You ran out of paper!"}

                new_note_item = cls.get_item(added)

                if await InventoryManager.add_item_to_user(email, new_note_item, 1) != 1:
                    # no empty slot to fit new note, refund paper
                    await InventoryManager.add_item_to_user(email, cou_globals.items[cls.PAPER_ITEM], 1)
                    return {"error": "There's no room for this note in your inventory!"}

                # increase skill
                await SkillManager.learn(cls.WRITING_SKILL, email)

            # send OK to client
            return await added.get_map()
        except Exception:
            LOGGER.exception(f"Couldn't create note with {note_data}")
            return {"error": "Something went wrong :("}

    @classmethod
    def get_item(cls, note: Note) -> Item:
        note_item = Item.clone(cou_globals.items[cls.NOTE_ITEM])
        note_item.metadata.update({"note_id": str(note.id), "title": note.title})
        return note_item


@notes_blueprint.route("/note/find/<int:id>")
async def app_find(id: int):
    note = await NoteManager.find(id)
    if not note:
        return {}
    return jsonify(await note.get_map())


@notes_blueprint.route("/note/dropped")
async def dropped():
    notes = []
    street_lookups = []

    async def get_notes(item: Item, street_id: str):
        try:
            if item.itemType != "note":
                return
            note_id = int(item.metadata["note_id"])
            note = await (await NoteManager.find(note_id)).get_map()
            note["street_tsid"] = mapdata.tsid_l(street_id)
            note["street_label"] = mapdata.get_street_by_tsid(street_id)["label"]
            notes.append(note)
        except Exception:
            LOGGER.warning(f"Could not find dropped note for <item={item}>")

    async with remote_engine.connect() as conn:
        query = text("SELECT id, items FROM streets WHERE items LIKE '%note%'")
        for result in (await conn.execute(query)).mappings().all():
            for item_map in json.loads(result["items"]):
                street_lookups.append(get_notes(Item.from_json(item_map), result["id"]))
        await asyncio.gather(*street_lookups)

    return jsonify(notes)
