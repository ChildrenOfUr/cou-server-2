from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.entities.entity import create_id


LOGGER = get_logger(__name__)


class FoxBaitItem:
    async def placeBait(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        from cou_server.endpoints.inventory import InventoryManager
        from cou_server.entities.npcs.animals.fox import FoxBait


        # take item
        taken = await InventoryManager.take_any_items_from_user(email, "fox_bait", 1)
        if taken != 1:
            return False

        # create entity
        try:
            player = cou_globals.users[username]
            x = player.current_x + 60
            y = player.current_y
            id = create_id(x, y, "FoxBait", street_name)
            FoxBait(id, x, y, 0, 0, False, street_name)
            return True
        except Exception:
            LOGGER.exception("placeBait failed to create entity")
            return False

