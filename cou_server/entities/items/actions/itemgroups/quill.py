import json

from cou_server.common.global_gobbler import cou_globals


class Quill:
    @classmethod
    async def openNote(cls, socket = None, note_id: int = None) -> None:
        await socket.send(json.dumps({
            "note_read": str(note_id),  # client is expecting to parse an int from a string
        }))

    async def openNoteEditor(cls, socket = None) -> None:
        await socket.send(json.dumps({
            "note_write": True,
        }))

    async def writeNote(self, socket = None, map: dict = None, street_name: str = None, email: str = None, username: str = None) -> None:
        await self.openNoteEditor(socket)

    async def readNote(self, socket = None, map: dict = None, street_name: str = None, email: str = None, username: str = None) -> None:
        # if this action is coming from a ground item
        if map.get("id") and map.get("streetName") and cou_globals.streets.get(map["streetName"]).entity_maps["groundItem"].get(map["id"]):
            note = cou_globals.streets[map["streetName"]].entity_maps["groundItem"][map["id"]]
            note_id = int(note.metadata["note_id"])
        else:
            note_id = int(map["itemdata"]["note_id"])

        await self.openNote(socket, note_id)


# part of item;

# abstract class Quill {

# 	// Fortune

# 	Future readFortune({WebSocket userSocket, Map map, String streetName, String email, String username}) async {
# 		openNote(userSocket, map["itemdata"]["note_id"]);
# 	}

# 	// Fortune Cookie

# 	Future insertNote({WebSocket userSocket, Map map, String streetName, String email, String username}) async {
# 		if (map["itemdata"]["note_id"] != null) {
# 			toast("That cookie already has a note in it!", userSocket);
# 		} else {
# 			userSocket.add(JSON.encode({
# 				"action": "insertNote2",
# 				"id": "fortune_cookie",
# 				"openWindow": "itemChooser",
# 				"filter": "itemType=note",
# 				"windowTitle": "Which note do you want to insert?"
# 			}));
# 		}
# 	}

# 	Future insertNote2({WebSocket userSocket, Map map, String streetName, String email, String username}) async {
# 		// Find info about the note to put in the cookie
# 		Item noteTaken = await InventoryV2.takeItemFromUser(email, map["slot"], map["subSlot"], 1);
# 		assert (noteTaken != null && noteTaken.itemType == NoteManager.note_item);
# 		int noteId = int.parse(noteTaken.metadata["note_id"]);

# 		// Add the note id to the cookie
# 		Item cookieItem = new Item.clone(NoteManager.fortune_cookie_withfortune_item)
# 			..metadata["note_id"] = noteId.toString()
# 			..metadata["title"] = (await NoteManager.find(noteId)).title;

# 		// Replace the empty cookie with the new cookie in their inventory
# 		if ((await InventoryV2.takeAnyItemsFromUser(email, NoteManager.fortune_cookie_item, 1)) != null) {
# 			InventoryV2.addItemToUser(email, cookieItem.getMap(), 1);
# 		}
# 	}

# 	Future breakCookie({WebSocket userSocket, Map map, String streetName, String email, String username}) async {
# 		if (map["itemdata"]["note_id"] == null) {
# 			toast("That cookie is empty!", userSocket);
# 		} else {
# 			// Find info about the note inside the cookie
# 			int noteId = map["itemdata"]["note_id"];

# 			// Create a fortune item
# 			Item fortuneItem = new Item.clone(NoteManager.fortune_item)
# 				..metadata["note_id"] = noteId.toString()
# 				..metadata["title"] = (await NoteManager.find(noteId)).title;

# 			// Replace the cookie with the fortune in their inventory
# 			if ((await InventoryV2.takeItemFromUser(email, map["slot"], map["subSlot"], 1)) != null) {
# 				InventoryV2.addItemToUser(email, fortuneItem.getMap(), 1);
# 			}

# 			// Open the note in the client
# 			openNote(userSocket, noteId);
# 		}
# 	}
# }