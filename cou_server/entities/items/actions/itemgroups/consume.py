from datetime import timedelta

from cou_server.buffs.buff_manager import BuffManager
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import Timer, toast
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from cou_server.entities.items.item import Item
from cou_server.metabolics.metabolics_change import MetabolicsChange
from cou_server.quests import messages


class Consumable(MetabolicsChange):
    """Takes away item and gives the stats specified in data/items/actions/consume.json"""

    async def eat(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        consumed = await self.consume(
            street_name=street_name, map=map, socket=socket, email=email, username=username,
        )

        if consumed:
            MessageBus.publish(
                "requirement_progress", messages.RequirementProgress(
                    f"eat_{consumed.itemType}", email, count=map["count"],
                ),
            )

        return consumed is not None

    async def drink(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        consumed = await self.consume(
            street_name=street_name, map=map, socket=socket, email=email, username=username,
        )

        if consumed:
            MessageBus.publish(
                "requirement_progress", messages.RequirementProgress(
                    f"drink_{consumed.itemType}", email, count=map["count"],
                ),
            )

            if consumed.itemType in ["beer", "hooch"]:
                # give smashed buff
                await BuffManager.add_to_user("smashed", email, socket)

                # give hungover buff when smashed ends
                async def am_hungover():
                    if not await BuffManager.player_has_buff("smashed", email):
                        # only if they haven't had another one
                        await BuffManager.add_to_user("hungover", email, socket)
                Timer(timedelta(seconds=BuffManager.buffs["smashed"].length), am_hungover)

        return consumed is not None


    async def activate(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        from cou_server.endpoints.inventory import InventoryManager

        consumed = await InventoryManager.take_item_from_user(email, map["slot"], map["subSlot"], map["count"])

        if consumed is not None and consumed.itemType == "spinach":
            await BuffManager.add_to_user("spinach", email, socket)

        return consumed is not None

    async def taste(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        result = await self.try_set_metabolics(email, mood=-5)

        if result:
            await toast("Maybe you should use the lotion on a butterfly and not your tounge?", socket)

        return result

    async def consume(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> "Item":
        from cou_server.endpoints.inventory import InventoryManager

        consumed = await InventoryManager.take_item_from_user(email, map["slot"], map["subSlot"], map["count"])
        if consumed is None:
            return None

        count = map["count"]
        energy_award = consumed.consumeValues["energy"] * count
        mood_award = consumed.consumeValues["mood"] * count
        img_award = consumed.consumeValues["img"] * count

        # cap the energy and mood as appropriate
        m = await MetabolicsChange.get_metabolics(email)
        energy_award = min(energy_award, m.max_energy - m.energy)
        mood_award = min(mood_award, m.max_mood - m.mood)

        message = f"Consuming that {consumed.name} gave you "
        if energy_award > 0:
            message += f"{energy_award} energy"

        if mood_award > 0:
            if energy_award > 0 and img_award == 0:
                message += " and "
            elif energy_award > 0:
                message += ", "

            message += f"{mood_award} mood"

            if img_award > 0:
                message += ", "
            else:
                message += " "

        if img_award > 0:
            if energy_award > 0 or mood_award > 0:
                message += " and "
            message += f" {img_award} iMG"

        message = message.strip() + "."

        await toast(message, socket)

        if consumed.itemType == "pumpkin_pie":
            await BuffManager.add_to_user("full_of_pie", email, socket)

        await self.try_set_metabolics(email, energy=energy_award, mood=mood_award, img_min=img_award)
        return consumed
