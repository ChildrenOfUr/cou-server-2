import random

from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import toast
from cou_server.metabolics.metabolics_change import MetabolicsChange
from cou_server.quests import messages


class Emblem(MetabolicsChange):
    async def caress(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        amt = random.randint(5, 14)
        await StatManager.add(email, Stat.emblems_caressed)
        await toast(f"+{amt} mood for caressing", socket)
        await self.try_set_metabolics(email, mood=amt)

    async def consider(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        amt = random.randint(5, 14)
        await StatManager.add(email, Stat.emblems_considered)
        await toast(f"+{amt} energy for considering", socket)
        await self.try_set_metabolics(email, energy=amt)

    async def contemplate(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        amt = random.randint(5, 14)
        await StatManager.add(email, Stat.emblems_contemplated)
        await toast(f"+{amt} iMG for contemplating", socket)
        await self.try_set_metabolics(email, img_min=amt)

    async def iconize(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        from cou_server.endpoints.inventory import InventoryManager

        item_in_slot = await InventoryManager.get_item_in_slot(email)
        emblem_type = item_in_slot.itemType
        icon_type = f"icon_of_{emblem_type[10:]}"
        success1 = await InventoryManager.take_any_items_from_user(email, emblem_type, 11) == 11
        if not success1:
            return False
        success2 = await InventoryManager.add_item_to_user(email, cou_globals.items[icon_type], 1)
        if success2 == 0:
            return False
        else:
            MessageBus.publish("requirement_progress", messages.RequirementProgress("iconGet", email))
            await StatManager.add(email, Stat.icons_collected)
            return True


class Icon(MetabolicsChange):
    async def tithe(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        await StatManager.add(email, Stat.icons_tithed)
        await self.try_set_metabolics(email, currants=-100)

    async def ruminate(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        await StatManager.add(email, Stat.icons_ruminated)
        await self.try_set_metabolics(email, mood=50)

    async def revere(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        await StatManager.add(email, Stat.icons_revered)
        await self.try_set_metabolics(email, currants=50)

    async def reflect(self, street_name: str = None, map: dict = None, socket = None, email: str = None, username: str = None) -> bool:
        await self.try_set_metabolics(email, img_min=50)
