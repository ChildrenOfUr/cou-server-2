import dataclasses
from random import randint
from typing import Any

from cou_server.common import get_logger
from cou_server.common.util import toast
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.serializable import serializable
from cou_server.common.global_gobbler import cou_globals
from cou_server.endpoints import mapdata
from cou_server.entities.entity import create_id
from cou_server.entities.action import Action
from cou_server.entities.items.actions.itemgroups.consume import Consumable
from cou_server.entities.items.actions.itemgroups.emblems_icons import Emblem, Icon
from cou_server.entities.items.actions.itemgroups.foxbait import FoxBaitItem
from cou_server.entities.items.actions.itemgroups.quill import Quill


LOGGER = get_logger(__name__)

# Discounts, stored as itemType: part paid out of 1 (eg. 0.8 for 20% off)
DISCOUNTS = {}

DROP = Action.with_name("Drop")
DROP.description = "Drop this item on the ground."
DROP.multiEnabled = True

PICK_UP = Action.with_name("Pickup")
PICK_UP.description = "Put this item in your inventory."
PICK_UP.multiEnabled = True


@serializable
@dataclasses.dataclass
class Item(Consumable, Emblem, FoxBaitItem, Icon, Quill):
    category: str = None
    iconUrl: str = None
    spriteUrl: str = None
    animSpriteUrl: str = None
    brokenUrl: str = None
    toolAnimation: str = None
    name: str = None
    recipeName: str = None
    description: str = None
    itemType: str = None
    item_id: str = None
    entity: str = None

    price: int = None
    stacksTo: int = None
    iconNum: int = 4
    durability: int = None
    subSlots: int = 0
    gains: dict[str, int] = dataclasses.field(default_factory=dict)

    x: float = None
    y: float = None

    onGround: bool = False
    isContainer: bool = False

    subSlotFilter: list[str] = dataclasses.field(default_factory=list)

    consumeValues: dict[str, int] = dataclasses.field(default_factory=dict)
    metadata: dict[str, Any] = dataclasses.field(default_factory=dict)

    @property
    def discount(self) -> float:
        if self.itemType in DISCOUNTS:
            return DISCOUNTS[self.itemType]

        return 1.0

    @property
    def actions(self) -> list[Action]:
        result: list[Action] = []
        if not hasattr(self, "_actions"):
            self._actions = []

        if self.onGround:
            for action in self._actions:
                if action.groundAction:
                    result.append(action)
            result.append(PICK_UP)
        else:
            # Make sure the drop action is last
            drop_instance = None
            for action in self._actions:
                if action.actionName == DROP.actionName:
                    drop_instance = action
                    break
            if drop_instance:
                self._actions.remove(drop_instance)

            result = self._actions
            result.append(DROP)

        return result

    @actions.setter
    def actions(self, value: list[Action]) -> None:
        self._actions = value

    def filter_allows(self, test_item: "Item" = None, item_type: str = None):
        # Allow an empty slot
        if not test_item and not item_type:
            return True

        # Bags accept empty item types (this is an empty slot)
        if item_type is not None and len(item_type) == 0:
            return True

        if not test_item:
            test_item = cou_globals.items[item_type]

        if len(self.subSlotFilter) == 0:
            return not test_item.isContainer
        else:
            return test_item.itemType in self.subSlotFilter

    async def pickup(self, socket = None, email: str = None, username: str = None, count: int = 1) -> None:
        # Client: ground -> inventory
        from cou_server.endpoints.inventory import InventoryManager

        self.onGround = False

        item = Item.clone(self)
        item.onGround = False
        item.metadata = self.metadata

        await InventoryManager.add_item_to_user(email, item, count, self.item_id)
        await StatManager.add(email, Stat.items_picked_up, increment=count)

    async def drop(self, socket = None, map: dict = None, street_name: str = None, email: str = None, username: str = None) -> None:
        # Client: inventory -> ground
        from cou_server.endpoints.inventory import InventoryManager

        dropped_item = await InventoryManager.take_item_from_user(email, map["slot"], map["subSlot"], map["count"])
        player_id = cou_globals.users.get(username)

        if dropped_item is None or player_id is None:
            return

        for index in range(map["count"]):
            dropped_item.put_on_ground(player_id.current_x + 40, player_id.current_y, street_name)

        await StatManager.add(email, Stat.items_dropped, increment=map["count"])

    async def place(self, socket = None, map: dict = None, street_name: str = None, email: str = None, username: str = None) -> None:
        # Item -> EntityItem
        from cou_server.endpoints.inventory import InventoryManager

        tsid = mapdata.get_street_by_name(street_name).get("tsid")
        if tsid is None:
            await toast("Something went wrong! Try another street?", socket)
            LOGGER.warning(f"Street <label={street_name} has no TSID")
            return

        placed_item = await InventoryManager.take_item_from_user(email, map["slot"], map["subSlot"], 1)
        if placed_item is not None:
            pass
            # TODO EntityItem.place(email, placed_item.itemType, tsid)

    def put_on_ground(self, x, y, street_name: str, item_id: str = None, count: int = 1):
        street = cou_globals.streets.get(street_name)
        if not street:
            return

        for i in range(0, count):
            temp_id = item_id
            if not temp_id:
                rand_str = str(randint(0, 9999))
                temp_id = "i" + create_id(x, y, self.itemType, street_name + rand_str)

            item = Item.clone(self)
            item.x = x
            item.y = street.get_y_from_ground(x, y, 1, 1)
            item.item_id = temp_id
            item.onGround = True
            item.metadata = self.metadata
            street.ground_items[temp_id] = item

    def get_map(self) -> dict:
        map = self.to_json()
        map["id"] = self.item_id
        map["tool_animation"] = self.toolAnimation
        return map

    async def customize_actions(self, email: str) -> list[Action]:
        return self.actions
