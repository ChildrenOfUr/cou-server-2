import asyncio
from datetime import timedelta
import inspect
import json
import time
from typing import Callable, Union


MIN_CLIENT_VERSION = 1471


prompt_callbacks = {}

class Timer:
    def __init__(self, duration: timedelta, callback: Callable):
        self._cancelled = False
        self._periodic = False
        self._duration = duration
        self._callback = callback
        self.is_active = True

        asyncio.get_event_loop().call_later(duration.total_seconds(), self._run)

    @classmethod
    def periodic(cls, duration: timedelta, callback: Callable) -> "Timer":
        timer = Timer(duration, callback)
        timer._periodic = True
        return timer

    def cancel(self) -> None:
        self._cancelled = True
        self.is_active = False

    def _run(self) -> None:
        if self._cancelled:
            return

        if inspect.iscoroutinefunction(self._callback):
            asyncio.create_task(self._callback())
        elif inspect.iscoroutine(self._callback):
            asyncio.create_task(self._callback)
        else:
            self._callback()

        if self._periodic:
            asyncio.get_event_loop().call_later(self._duration.total_seconds(), self._run)
        else:
            self.is_active = False


async def prompt_string(
    prompt: str,
    user_socket,
    reference: str,
    callback: callable,
    char_limit: int = 0,
) -> None:
    """Ask a user for a string
    Pass a unique value for `reference` so that the response is routed correctly.
    `callback` will be called with the following positional args:
        1. reference
        2. user's response
    """

    prompt_callbacks[reference] = callback

    await user_socket.send(
        json.dumps.encode(
            {
                "promptString": True,
                "promptText": prompt,
                "promptRef": reference,
                "charLimit": char_limit,
            }
        )
    )


async def toast(message: str, web_socket, skip_chat: bool = False, on_click=None):
    """ Tell a client to display a toast """
    await web_socket.send(
        json.dumps(
            {
                "toast": True,
                "message": message,
                "skipChat": skip_chat,
                "onClick": on_click,
            }
        )
    )


async def play_sound(sound: str, web_socket):
    """Tell the client to play a sound by name."""
    await web_socket.send(json.dumps({"playSound": True, "sound": sound}))


async def prompt_for_string(
    prompt: str, web_socket, reference: str, callback, char_limit: int = 0
):
    """
    Ask a user for a string.

    Pass a unique value for 'reference' so that the response is routed correctly.

    When the user submits a string, 'callback' will be called with the following arguments:
    1. reference: str
    2. user's response: str
    """
    global PROMPT_CALLBACKS

    if not PROMPT_CALLBACKS:
        PROMPT_CALLBACKS = dict()

    PROMPT_CALLBACKS[reference] = callback

    await web_socket.send(
        json.dumps(
            {
                "promptString": True,
                "promptText": prompt,
                "promptRef": reference,
                "charLimit": char_limit,
            }
        )
    )


async def wait_for(
    predicate: Union[bool, Callable[[], bool]],
    timeout: float = None,
    period: float = 0.25,
) -> None:
    start_time = time.time()
    end_time = None
    if timeout:
        end_time = start_time + timeout
    while True:
        if isinstance(predicate, Callable):
            if predicate():
                return
        if end_time and time.time() > end_time:
            raise ValueError("Condition never became true within timeout.")
        await asyncio.sleep(period)


def clamp(value, min_value, max_value):
    """ Return [value] adjusted to exist between [min_value] and [max_value] """

    return max(min(value, max_value), min_value)
