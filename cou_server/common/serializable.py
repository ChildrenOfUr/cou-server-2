from datetime import datetime, timezone
import dataclasses
import inspect
import json
import logging
import typing

import deepfriedmarshmallow
import marshmallow
from marshmallow import class_registry


LOGGER = logging.getLogger(__name__)
_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%fZ"


def serializable(cls):
    """Reduces the overhead of defining a serializable class. Requires that the class is an @dataclass"""

    transform_names = getattr(cls, "_transform_names", True)
    schema_class = _create_schema(cls, transform_names=transform_names)
    setattr(cls, f"_{cls.__name__}Schema", schema_class)
    setattr(cls, "_schema_instance", schema_class())

    @classmethod
    def from_json(cls, data: dict|str, many: bool = False) -> cls:
        try:
            if isinstance(data, str):
                data = json.loads(data)
            return cls._schema_instance.load(data, unknown=marshmallow.EXCLUDE, many=many)
        except marshmallow.ValidationError:
            LOGGER.error(
                f"Faled to load data as a valid {cls.__name__}.\n\n"
                f"Schema Fields: {cls._schema_instance.declared_fields}\n\n"
                f"Data: {data}\n\n"
            )
            raise

    def to_json(self) -> dict:
        return self._schema_instance.dump(self)

    @classmethod
    def clone(cls, other: cls) -> cls:
        return cls.from_json(other.to_json())

    setattr(cls, "from_json", from_json)
    setattr(cls, "to_json", to_json)
    setattr(cls, "clone", clone)

    return cls


def _create_schema(cls: type, transform_names: bool = True) -> type:
    class_attributes = {}
    camel_names = {}
    init_fields = set()
    post_init_fields = set()

    def _get_field_name(original_name: str) -> str:
        if transform_names:
            if original_name in camel_names:
                    field_name = camel_names[original_name]
            else:
                pieces = original_name.split("_")
                field_name = pieces[0] + ''.join(piece.title() for piece in pieces[1:])
                camel_names[original_name] = field_name
        else:
            field_name = original_name
        return field_name

    def _get_schema_field(field_type: type, field_name: str, dump_only: bool = False) -> marshmallow.fields.Field:
        if typing.get_origin(field_type) is None:
            if field_type in (int, bool, float, str):
                marshmallow_field = getattr(marshmallow.fields, field_type.__name__.title())(
                    data_key=field_name, dump_only=dump_only, allow_none=True,
                )
            elif field_type == datetime:
                marshmallow_field = marshmallow.fields.AwareDateTime(
                    format=_DATETIME_FORMAT, default_timezone=timezone.utc,
                    data_key=field_name, dump_only=dump_only, allow_none=True,
                )
            else:
                marshmallow_field = marshmallow.fields.Nested(
                    f"_{field_type.__name__}Schema", data_key=field_name, dump_only=dump_only, allow_none=True, unknown=marshmallow.EXCLUDE,
                )
        if typing.get_origin(field_type) == list:
            inner_type = typing.get_args(field_type)[0]
            if inner_type in (int, bool, float, str):
                marshmallow_field = marshmallow.fields.List(
                    getattr(marshmallow.fields, inner_type.__name__.title()), data_key=field_name, dump_only=dump_only, allow_none=True,
                )
            elif inner_type == datetime:
                marshmallow_field = marshmallow.fields.List(
                    marshmallow.fields.AwareDateTime(format=_DATETIME_FORMAT, default_timezone=timezone.utc),
                    data_key=field_name, dump_only=dump_only, allow_none=True,
                )
            else:
                marshmallow_field = marshmallow.fields.List(marshmallow.fields.Nested(
                    f"_{inner_type.__name__}Schema", data_key=field_name, dump_only=dump_only, allow_none=True, unknown=marshmallow.EXCLUDE,
                ))
        if typing.get_origin(field_type) == dict:
            marshmallow_field = marshmallow.fields.Dict(data_key=field_name, dump_only=dump_only, allow_none=True)

        return marshmallow_field

    # setup all the serializable fields by inspecting the dataclass fields
    for field in dataclasses.fields(cls):
        field_name = _get_field_name(field.name)
        if not field.init:
            post_init_fields.add(field_name)
            continue
        init_fields.add(field_name)
        marshmallow_field = _get_schema_field(field.type, field_name)
        class_attributes[field.name] = marshmallow_field

    # look at any property fields as well
    for field in [f for f in inspect.getmembers(cls) if type(f[1]) == property]:
        field_name = _get_field_name(field[0])
        post_init_fields.add(field_name)
        dump_only = field[1].fset is None
        marshmallow_field = _get_schema_field(
            inspect.get_annotations(field[1].fget)["return"], field_name, dump_only=dump_only,
        )
        class_attributes[field[0]] = marshmallow_field

    class_attributes["_camel_names"] = camel_names
    class_attributes["_dc_init_fields"] = init_fields
    class_attributes["_dc_post_init_fields"] = post_init_fields

    if transform_names:
        # we made the assumption above that the fields coming from the
        # server are camelcased, and that's very often true, but not
        # always true, so let's try to ensure it is true here
        @marshmallow.pre_load()
        def sort_out_field_names(self, data, **kwargs) -> dict:
            camel_dict = {}
            for key, value in data.items():
                if key in self._camel_names:
                    camel_name = self._camel_names[key]
                else:
                    pieces = key.split("_")
                    camel_name = pieces[0] + ''.join(piece.title() for piece in pieces[1:])
                    self._camel_names[key] = camel_name
                camel_dict[camel_name] = value
            return camel_dict
        class_attributes["sort_out_field_names"] = sort_out_field_names

    @marshmallow.post_load()
    def make_cls(self, data, **kwargs) -> cls:
        init_data = {}
        post_init_data = {}
        for key, value in data.items():
            if key in cls._schema_instance._dc_post_init_fields:
                post_init_data[key] = value
            else:
                init_data[key] = value
        instance = cls(**init_data)
        for key, value in post_init_data.items():
            try:
                setattr(instance, key, value)
            except AttributeError:
                pass # must be a read only property
        return instance
    class_attributes[f"make_{cls.__name__}"] = make_cls

    schema_name = f"_{cls.__name__}Schema"
    schema_class = type(schema_name, (deepfriedmarshmallow.JitSchema,), class_attributes)

    # make sure the class is registered
    class_registry.register(schema_name, schema_class)

    return schema_class
