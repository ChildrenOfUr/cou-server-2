from math import pow, sqrt


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other) -> bool:
        """Return whether this point is equal to another point."""
        return other is Point and self.x == other.x and self.y == other.y

    def __add__(self, other):
        """Add the x and y values of this point and another point."""
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """Subtract the x and y values of another point from this point."""
        return Point(self.x - other.x, self.y - other.y)

    def __str__(self) -> str:
        """Get this point as an ordered pair string."""
        return f"({self.x}, {self.y})"

    def __repr__(self) -> str:
        return "Point " + self.__str__()

    def sqr_distance_to(self, other):
        """Get the squared distance between this point and another point."""
        distance = self - other
        return pow(distance.x, 2) + pow(distance.y, 2)

    def distance_to(self, other):
        """Get the distance between this point and another point."""
        return sqrt(self.sqr_distance_to(other))

    @property
    def magnitude(self):
        """Get the distance between the origin and this point."""
        return self.distance_to(origin)

    def clone(self):
        """Create a new point identical to this one."""
        return Point(self.x, self.y)


origin = Point(0, 0)
