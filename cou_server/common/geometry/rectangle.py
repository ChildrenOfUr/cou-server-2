from .point import Point


class Rectangle:
    def __init__(self, top_left: Point, bottom_right: Point):
        self._top_left = top_left
        self._bottom_right = bottom_right

    def __eq__(self, other) -> bool:
        """Check if a rectangle is exactly the same size and position as another."""
        return (
            other is Rectangle
            and self.top_left == other.top_left
            and self.bottom_right == other.bottom_right
        )

    def __contains__(self, item) -> bool:
        """Check whether the rectangle contains a point or another entire rectangle."""
        if item is Point:
            return (
                self.left <= item.x <= self.right and self.top <= item.y <= self.bottom
            )

        if item is Rectangle:
            return (
                self.left <= item.left
                and self.right >= item.right
                and self.top <= item.top
                and self.bottom >= item.bottom
            )

        return False

    def __str__(self) -> str:
        return f"{self.top_left.__str__()}, {self.bottom_right.__str__()}"

    def __repr__(self) -> str:
        return "Rectangle " + self.__str__()

    def intersects(self, other) -> bool:
        if other is not Rectangle:
            return False

        return (
            self.left <= other.right
            and other.left <= self.right
            and self.top <= other.bottom
            and other.top <= self.bottom
        )

    @property
    def top_left(self) -> Point:
        """Get the top left point of the rectangle."""
        return self._top_left

    @top_left.setter
    def top_left(self, value: Point):
        """Set the top left point of the rectangle."""
        self._top_left = value

    @property
    def bottom_right(self) -> Point:
        """Get the bottom left point of the rectangle."""
        return self._bottom_right

    @bottom_right.setter
    def bottom_right(self, value: Point):
        """Set the bottom left point of the rectangle."""
        self._bottom_right = value

    @property
    def top_right(self) -> Point:
        """Get the top right point of the rectangle."""
        return Point(self.bottom_right.x, self.top_left.y)

    @top_right.setter
    def top_right(self, value: Point):
        """Set the top right point of the rectangle."""
        self.top_left.y = value.y
        self.bottom_right.x = value.x

    @property
    def bottom_left(self) -> Point:
        """Get the bottom left point of the rectangle."""
        return Point(self.top_left.x, self.bottom_right.y)

    @bottom_left.setter
    def bottom_left(self, value: Point):
        """Set the bottom left point of the rectangle."""
        self.top_left.x = value.x
        self.bottom_right.y = value.y

    @property
    def top(self):
        """Get the y coordinate of the top of the rectangle."""
        return self.top_left.y

    @top.setter
    def top(self, value):
        """Set the y coordinate of the top of the rectangle."""
        self.top_left.y = value

    @property
    def left(self):
        """Get the x coordinate of the right side of the rectangle."""
        return self.top_left.x

    @left.setter
    def left(self, value):
        """Set the x coordinate of the left side of the rectangle."""
        self.top_left.x = value

    @property
    def right(self):
        """Get the x coordinate of the right side of the rectangle."""
        return self.bottom_right.x

    @right.setter
    def right(self, value):
        """Set the x coordinate of the right side of the rectangle."""
        self.bottom_right.x = value

    @property
    def bottom(self):
        """Get the y coordinate of the bottom of the rectangle."""
        return self.bottom_right.y

    @bottom.setter
    def bottom(self, value):
        """Set the y coordinate of the bottom of the rectangle."""
        self.bottom_right.y = value

    @property
    def width(self):
        """Get the distance between the left and right of the rectangle."""
        return self.right - self.left

    @width.setter
    def width(self, value):
        """Set the width of the rectangle, locking the top left corner."""
        self.right = self.left + value

    @property
    def height(self):
        """Get the distance between the top and bottom of the rectangle."""
        return self.bottom - self.top

    @height.setter
    def height(self, value):
        """Set the height of the rectangle, locking the top left corner."""
        self.bottom = self.top + value

    def clone(self):
        """Create a new rectangle identical to this one."""
        top_left = self.top_left.clone()
        bottom_right = self.bottom_right.clone()
        return Rectangle(top_left, bottom_right)
