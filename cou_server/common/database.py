from contextlib import asynccontextmanager

from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.ext.asyncio import async_sessionmaker
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import DeclarativeBase

from cou_server.api_keys import DATABASE_URI
from cou_server.common import get_logger

logger = get_logger(__name__)
local_engine = create_async_engine("sqlite+aiosqlite:///cou_server.db")
remote_engine = create_async_engine(DATABASE_URI)
LocalSession = async_sessionmaker(bind=local_engine)
RemoteSession = async_sessionmaker(bind=remote_engine, expire_on_commit=False)


class Base(AsyncAttrs, DeclarativeBase):
    pass


async def db_create():
    """ Create or upgrade the database """

    # make sure all models are loaded first
    from cou_server import models

    async with local_engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    logger.debug("local database ready")


@asynccontextmanager
async def db_session_context(local_db=False):
    """ Returns a session to the db which will be auto committed when the
        context exits.
    """

    if local_db:
        db_session = LocalSession()
    else:
        db_session = RemoteSession()
    try:
        yield db_session
        await db_session.commit()
    except Exception:
        await db_session.rollback()
        raise
