import json
from threading import Timer

from cou_server.common import get_logger

logger = get_logger(__name__)


class FileCache:
    interval = 30
    heights_cache = None
    heads_cache = None

    @staticmethod
    def load_caches():
        # Make trees speech bubbles appear where they should
        FileCache.heights_cache = FileCache.load_cache_from_disk("heightsCache.json")
        FileCache.heads_cache = FileCache.load_cache_from_disk("headsCache.json")

        # save the cache to the disk
        def periodic_save():
            try:
                for cache in ["heads", "heights"]:
                    cache_file = f"{cache}Cache.json"
                    cache = locals()[f"{cache}_cache"]
                    FileCache.save_cache_to_disk(cache_file, cache)
            except Exception:
                logger.exception("Problem writing {cache_file}.")

            # do it again later
            Timer(FileCache.interval, periodic_save)

        logger.debug("[FileCache] Caches loaded")
        Timer(FileCache.interval, periodic_save)

    @staticmethod
    def load_cache_from_disk(filename):
        try:
            with open(filename, "r") as cache_file:
                return json.loads(cache_file.read())
        except Exception:
            return {}

    @staticmethod
    def save_cache_to_disk(filename, cache):
        try:
            with open(filename, "w") as cache_file:
                cache_file.write(json.dumps(cache))
        except Exception:
            logger.exception(f"Could not save {cache} cache {filename}.")
