from quart import Blueprint, jsonify
from quart_cors import cors

const_blueprint = Blueprint("const_blueprint", __name__)
cors(const_blueprint)

QUOIN_LIMIT = 100
QUOIN_MULTIPLIER_LIMIT = 73  # prime
CHANGE_USERNAME_COST = 1000  # currants


@const_blueprint.route("/constants/json")
async def get_all_constants():
    return jsonify(
        {
            "quoinLimit": QUOIN_LIMIT,
            "quoinMultiplierLimit": QUOIN_MULTIPLIER_LIMIT,
            "changeUsernameCost": CHANGE_USERNAME_COST,
        }
    )
