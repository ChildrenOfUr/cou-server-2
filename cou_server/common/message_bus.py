import asyncio
import inspect
import logging
from typing import Callable


LOGGER = logging.getLogger(__name__)


_NO_SUBSCRIBER_WARNS = []


class _BusSubscriber:
    def __init__(self, channel, callback, headers=None):
        self.channel = channel
        self.callback = callback
        self.headers = headers


class MessageBus:
    subscribers = []

    @classmethod
    def publish(cls, channel, data = None, *args, **kwargs):
        no_subscribers = True
        for subscriber in cls.subscribers:
            if subscriber.channel != channel:
                continue

            no_subscribers = False

            sig = inspect.signature(subscriber.callback)
            for param in sig.parameters.values():
                if (
                    param.name == "headers"
                    and param.kind == inspect.Parameter.KEYWORD_ONLY
                ):
                    kwargs["headers"] = subscriber.headers
            if inspect.iscoroutinefunction(subscriber.callback):
                asyncio.create_task(subscriber.callback(data, *args, **kwargs))
            else:
                subscriber.callback(data, *args, **kwargs)

        if no_subscribers and channel not in _NO_SUBSCRIBER_WARNS:
            LOGGER.debug(f"No subscribers were registered for {channel} event containing data '{data}', args '{args}', and kwargs '{kwargs}'")
            _NO_SUBSCRIBER_WARNS.append(channel)

    @classmethod
    def unsubscribe(cls, subscriber):
        """ Unregister a given subscriber. They won't receive any more messages """

        try:
            cls.subscribers.remove(subscriber)
        except Exception:
            # guess they're not here anymore
            pass


    @staticmethod
    def subscribe(channels: str|list[str], method=None, headers=None) -> Callable|_BusSubscriber|list[_BusSubscriber]:
        """ Register this function as a callback on the given channel of the
            Message Bus
        """

        if isinstance(channels, str):
            channels = [channels]

        def _add_subscriptions(callback):
            subscriptions = [_BusSubscriber(channel, callback, headers=headers) for channel in channels]
            MessageBus.subscribers.extend(subscriptions)
            return subscriptions

        # if the method/function is passed in: MessageBus.subscribe("foo", func)
        if method:
            return _add_subscriptions(method)

        # used as a decorator:
        # @MessageBus.subscribe("foo")
        # def func():
        #     pass
        def decorator(func):
            _add_subscriptions(func)
            return func

        return decorator
