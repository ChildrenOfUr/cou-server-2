from quart import websocket


class Identifier:
    def __init__(self, username: str, current_street: str, tsid: str, socket: websocket):
        self.username = username
        self.current_street = current_street
        self.tsid = tsid
        self.socket = socket
        self.channel_list: list[str] = []
        self.current_x = 1.0
        self.current_y = 0.0

    def __str__(self):
        return f"<Identifier for {self.username} on {self.tsid} at ({self.current_x, self.current_y})>"
