import dataclasses
from datetime import timedelta
from importlib import resources
import json

from sqlalchemy.future import select

from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.util import Timer
from cou_server.common.serializable import serializable
from cou_server.common.database import db_session_context
from cou_server.models.metabolics import Metabolics
from cou_server.models.user import User


LOGGER = get_logger(__name__)


@serializable
@dataclasses.dataclass
class Buff:
    id: str
    name: str
    description: str
    length: int

    @property
    def indefinite(self) -> bool:
        # buffs with a length of -1 will stay until removed
        return self.length == -1

    async def get_for_player(self, email: str) -> "PlayerBuff":
        # cache (to prevent duplicate timers)
        cached = PlayerBuff.get_from_cache(self.id, email)
        if cached:
            return cached

        # database
        try:
            async with db_session_context() as db_session:
                metabolics = (await db_session.scalars(select(Metabolics).join(User).filter(User.email == self.email))).first()
                remaining = metabolics.buffs_json.get(self.id, self.length)
                return PlayerBuff(Buff.clone(self), email, remaining)
        except Exception:
            LOGGER.exception(f"Error getting buff {self.id} for <email={email}>")
            return None


class PlayerBuff(Buff):
    cache: dict[str, list["PlayerBuff"]] = {}

    @classmethod
    def get_from_cache(cls, id: str, email: str) -> "PlayerBuff":
        if email not in cls.cache:
            return None

        for buff in cls.cache[email]:
            if buff.id == id:
                return buff

        return None

    @classmethod
    def from_json(cls, data: dict|str, many: bool = False) -> "PlayerBuff":
        player_buff = PlayerBuff(
            super().from_json(data, many=many),
            data.get("player_email"),
            remaining=data.get("player_remaining")
        )

        player_buff._cache()
        return player_buff

    def __init__(self, base: Buff, email: str, remaining: int = None):
        data = base.to_json()
        data.pop("indefinite", None)
        super().__init__(**data)
        self.email = email
        self._update_timer: Timer = None

        if remaining is None:
            self.remaining = self.length
        else:
            self.remaining = remaining

        self._cache()

    def to_json(self) -> dict:
        return super().to_json() | {"player_email": self.email, "player_remaining": self.remaining}

    def _cache(self) -> None:
        if self.get_from_cache(self.id, self.email) is None:
            if self.email not in type(self).cache:
                type(self).cache[self.email] = []
            type(self).cache[self.email].append(self)

    async def start_updating(self) -> None:
        if not self.indefinite:
            # subtract 1 second from the remaining time every second
            async def update_buff():
                self.remaining -= 1

                if self.remaining <= 0:
                    # buff is over
                    await self.stop_updating()
                elif self.remaining % 10 == 0:
                    # write every 10 seconds
                    await self._write()
            self._update_timer = Timer.periodic(timedelta(seconds=1), update_buff)

        # save the current status to the database
        await self._write()

    async def stop_updating(self, write: bool = True) -> None:
        # pause the counter
        if self._update_timer:
            self._update_timer.cancel()

        # save the current status to the database
        if write:
            await self._write()

    async def remove(self) -> None:
        await self.stop_updating(write=False)
        self.remaining = 0
        await self._write(remove=True)
        type(self).cache.pop(self.email, None)

    async def extend(self, additional: int) -> None:
        self.remaining += additional
        await self._write()
        cou_globals.user_sockets[self.email].send(json.dumps({
            "buff_extend": self.id,
            "buff_extend_secs": additional,
        }))

    async def _write(self, remove: bool = False) -> bool:
        try:
            async with db_session_context() as db_session:
                metabolics = (await db_session.scalars(select(Metabolics).join(User).filter(User.email == self.email))).first()
                buffs_data = metabolics.buffs_json
                buffs_data[self.id] = self.remaining
                if (not self.indefinite or remove) and self.remaining <= 0:
                    buffs_data.pop(self.id, None)
                metabolics.buffs_json = buffs_data
            return True
        except Exception:
            LOGGER.exception(f"Error setting buff {self.id} for <email={self.email}>")
            return False


class BuffManager:
    buffs: dict[str, Buff] = {}

    @classmethod
    def load_buffs(cls) -> int:
        with resources.files("data.buffs").joinpath("buff_data.json").open() as buffs_file:
                for id, data in json.loads(buffs_file.read()).items():
                    cls.buffs[id] = Buff(id=id, **data)
        LOGGER.debug(f"[BuffManager] Loaded {len(cls.buffs)} buffs")
        return len(cls.buffs)

    @classmethod
    def find(cls, id: str):
        return cls.buffs.get(id)

    @classmethod
    async def add_to_user(cls, buff_id: str, email: str, socket) -> bool:
        """Give a user a buff"""
        if await cls.player_has_buff(buff_id, email):
            # user already has this buff
            return False

        new_buff = PlayerBuff(cls.find(buff_id), email)
        await socket.send(json.dumps({"buff": new_buff.to_json()}))
        await new_buff.start_updating()
        return True

    @classmethod
    async def remove_from_user(cls, buff_id: str, email: str, socket) -> bool:
        """Remove a buff from a user"""
        matching = [buff for buff in await cls.get_player_buffs(email=email) if buff["id"] == buff_id]
        await socket.send(json.dumps({"buff_remove": buff_id}))

        if matching:
            old_buff = PlayerBuff.from_json(matching[0])
            await old_buff.remove()
            return True
        return False

    @classmethod
    async def start_updating_user(cls, email: str) -> None:
        """Start updating all buffs for a user (login)"""
        if email is None:
            return

        try:
            for buff_map in await cls.get_player_buffs(email=email):
                buff = PlayerBuff.from_json(buff_map)
                await buff.start_updating()
        except Exception:
            LOGGER.exception(f"Could not resume buffs for <email={email}>")

    @classmethod
    async def stop_updating_user(cls, email: str) -> None:
        """Stop updating all buffs for a user (logout)"""
        if email is None:
            return

        try:
            for buff_map in await cls.get_player_buffs(email=email):
                buff = PlayerBuff.from_json(buff_map)
                await buff.stop_updating()
            PlayerBuff.cache.pop(email, None)
        except Exception:
            LOGGER.exception(f"Could not pause buffs for <email={email}>")

    @classmethod
    async def player_has_buff(cls, buff_id: str, email: str) -> bool:
        """Whether a player has a buff"""
        if email in PlayerBuff.cache:
            # check cache
            return PlayerBuff.get_from_cache(buff_id, email) is not None
        else:
            # check database
            buffs = await cls.get_player_buffs(email=email)
            for buff in buffs:
                if buff["id"] == buff_id:
                    return True
            return False

    @classmethod
    async def get_player_buffs(cls, email: str = None, username: str = None) -> list[dict]:
        if email is None and username is not None:
            email = await User.get_email_from_username(username)
        elif email is None and username is None:
            return None

        player_buffs_list = []
        # get data from database
        async with db_session_context() as db_session:
            metabolics = (await db_session.scalars(select(Metabolics).join(User).filter(User.email == email))).first()
            player_buffs_data = metabolics.buffs_json

        # fill in buff information
        try:
            for id, remaining in player_buffs_data.items():
                player_buff = PlayerBuff.get_from_cache(id, email) or PlayerBuff(cls.find(id), email, remaining)
                player_buffs_list.append(player_buff.to_json())
        except Exception:
            LOGGER.exception("Error getting buff information")

        return player_buffs_list
