import asyncio
from contextlib import asynccontextmanager
import dataclasses
import functools
import json

from quart import Blueprint, jsonify
from quart_cors import cors
from sqlalchemy.future import select

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import toast
from cou_server.endpoints import quest
from cou_server.entities.items.item import Item
from cou_server.models.inventory import Inventory, InventoryModel, Slot
from cou_server.models.user import User
from cou_server.quests import messages


inventory_endpoint = Blueprint("inventory_endpoint", __name__)
cors(inventory_endpoint)


LOGGER = get_logger(__name__)
_INVENTORY_LOCK_LOCK = asyncio.locks.Lock()
_INVENTORY_LOCKS: dict[str, tuple[str, asyncio.locks.Lock]] = {}


@asynccontextmanager
async def _lock_inventory(email: str, reason: str) -> asyncio.locks.Lock:
    try:
        async with _INVENTORY_LOCK_LOCK:
            if email not in _INVENTORY_LOCKS:
                _INVENTORY_LOCKS[email] = (reason, asyncio.locks.Lock())
        yield _INVENTORY_LOCKS[email][1]
    except Exception:
        LOGGER.error(f"Could not acquire a lock for inventory of <email={email}> for {reason} because {_INVENTORY_LOCKS[email][0]}")
        raise


class InventoryManager:
    @classmethod
    async def _serialize_inventory(cls, email: str) -> list[dict]|None:
        inventory = await cls._get_db_inventory(email)
        if inventory is None:
            LOGGER.error(f"No inventory found for <email={email}>")
            return None

        slot_maps: list[dict] = []
        for slot in inventory.slots:
            item = None
            if item_type := slot.item_type:
                item: Item = Item.clone(cou_globals.items[item_type])
                item.metadata = slot.metadata
                if item.isContainer and item.metadata.get("slots"):
                    bag_slots: list[Slot] = Slot.from_json(item.metadata["slots"], many=True)
                    bag_slot_maps: list[dict] = []
                    for bag_slot in bag_slots:
                        bag_item = None
                        if bag_item_type := bag_slot.item_type:
                            bag_item: Item = Item.clone(cou_globals.items[bag_item_type])
                            fixed_meta = {}
                            for key, value in bag_slot.metadata.items():
                                fixed_meta[key] = str(value)
                            bag_item.metadata = fixed_meta
                        bag_slot_map = {
                            "itemType": bag_slot.item_type,
                            "item": None if bag_item is None else bag_item.to_json(),
                            "count": bag_slot.count,
                        }
                        bag_slot_maps.append(bag_slot_map)
                    item.metadata["slots"] = json.dumps(bag_slot_maps)
            slot_map = {
                "itemType": slot.item_type,
                "item": None if item is None else item.to_json(),
                "count": slot.count,
            }
            slot_maps.append(slot_map)
        return slot_maps

    @classmethod
    async def _get_db_inventory(cls, email: str) -> Inventory:
        async with db_session_context() as db_session:
            db_inventory = (await db_session.scalars(select(InventoryModel).join(User).filter(User.email == email))).one_or_none()
            if db_inventory is None:
                return None

            slots: list[Slot] = Slot.from_json(db_inventory.inventory_json, many=True)
            # I've seen some count=0 slots where there's an item_type set, make sure it's 1 in those cases
            for slot in slots:
                if slot.item_type:
                    if slot.count == 0:
                        slot.count = 1
                    if cou_globals.items[slot.item_type].isContainer:
                        inner_slots: list[Slot] = Slot.from_json(slot.metadata.get("slots", '[]'), many=True)
                        for inner_slot in inner_slots:
                            if inner_slot.item_type and inner_slot.count == 0:
                                inner_slot.count = 1
                        slot.metadata["slots"] = json.dumps([s.to_json() for s in inner_slots])
            return Inventory(slots=slots)

    @classmethod
    async def _save_db_inventory(cls, inventory: Inventory, email: str) -> None:
        async with db_session_context() as db_session:
            user = (await db_session.scalars(select(User).filter(User.email == email))).one()
            db_inventory = (await db_session.scalars(select(InventoryModel).join(User).filter(User.email == email))).one_or_none()
            if db_inventory is None:
                db_inventory = InventoryModel(user=user)
                db_session.add(db_inventory)
                # player just got their first item, let's tell them about bags
                if email in quest.quest_log_cache:
                    await quest.quest_log_cache[email].offer_quest("Q8")
            db_inventory.inventory_json = json.dumps([s.to_json() for s in inventory.slots])

    @classmethod
    async def fire_inventory_at_user(cls, socket = None, email: str = None, update: bool = False):
            slot_maps = await InventoryManager._serialize_inventory(email)
            map = {
                "inventory": "true",
                "update": update,
                "slots": slot_maps,
            }
            await socket.send(json.dumps(map))

    @classmethod
    async def add_item_to_user(cls, email: str, item: Item|dict|str, count: int, from_object: str = "_self") -> int:
        """Returns the number of items successfully added to the user's inventory"""

        try:
            if count < 1:
                raise ValueError("Count must be greater than or equal to 1")
            if isinstance(item, dict):
                item = Item.from_json(item)
            elif isinstance(item, str):
                item = cou_globals.items[item]
            elif not isinstance(item, Item):
                raise ValueError("Item must be an Item, dict (serialized Item), or str (name of an item)")
        except Exception:
            LOGGER.exception(f"Unable to give <email={email}> {count} of {item}")
            return 0

        async with _lock_inventory(email, "addItemToUser"):
            socket = cou_globals.user_sockets[email]
            inventory_action = InventoryAction()
            added = await inventory_action._add_item(item, count, email)
        await cls.fire_inventory_at_user(socket, email, update=True)
        if added > 0:
            item_type = item.itemType
            MessageBus.publish("requirement_progress", messages.RequirementProgress(f"getItem_{item_type}", email, count=count))
            if item_type in ["pick", "fancy_pick"]:
                # Dullite, Beryl and Sparkly
                await quest.quest_log_cache[email].offer_quest("Q6")
            elif item_type == "cocktail_shaker":
                await quest.quest_log_cache[email].offer_quest("Q12")

        return added

    @classmethod
    async def take_item_from_user(cls, email: str, slot: int, sub_slot: int, count: int) -> Item:
        async with _lock_inventory(email, "takeItemFromUser"):
            socket = cou_globals.user_sockets[email]
            inventory_action = InventoryAction()
            item_taken = await inventory_action._take_item(slot, sub_slot, count, email)
            if item_taken is not None:
                await cls.fire_inventory_at_user(socket, email, update=True)
        return item_taken

    @classmethod
    async def take_any_items_from_user(cls, email: str, item_type: str, count: int, simulate: bool = False) -> int:
        async with _lock_inventory(email, "takeAnyItemsFromUser"):
            socket = cou_globals.user_sockets[email]
            inventory_action = InventoryAction()
            taken = await inventory_action._take_any_items(item_type, count, email, simulate=simulate)
            if taken == count and not simulate:
                await cls.fire_inventory_at_user(socket, email, update=True)

        return taken

    @classmethod
    async def move_item(cls, email: str, from_index: int = -1, from_bag_index: int = -1, to_index: int = -1, to_bag_index: int = -1) -> bool:
        async with _lock_inventory(email, "moveItem"):
            # get the user's inventory to work on
            inventory = await cls._get_db_inventory(email)
            inventory_before_change = Inventory.clone(inventory)

            # this has to be atomic so if it throws, return the inventory to the original
            try:
                # swap the from and to items
                new_contents = inventory.slots[from_index]
                if from_bag_index > -1:
                    bag_slot = inventory.slots[from_index] # bag in hotbar
                    bag_slots = Slot.from_json(bag_slot.metadata["slots"], many=True) # bag contents
                    new_contents = bag_slots[from_bag_index] # slot inside bag
                orig_contents = await cls.change_slot(inventory, email, to_index, to_bag_index, new_contents)
                if orig_contents is None:
                    raise RuntimeError(f"Could not move {new_contents.item_type} to {to_index}.{to_bag_index} within inventory")
                # move old item into other slot
                orig_contents = await cls.change_slot(inventory, email, from_index, from_bag_index, orig_contents, merge=False)
                if orig_contents is None:
                    raise RuntimeError(f"Could not move {new_contents.item_type} to {to_index}.{to_bag_index} within inventory")

                # update the database
                await cls._save_db_inventory(inventory, email)

                # update the client
                user_socket = cou_globals.user_sockets[email]
                await cls.fire_inventory_at_user(user_socket, email, update=True)
            except Exception:
                await cls._save_db_inventory(inventory_before_change, email)
                LOGGER.exception("Problem moving item")
                return False

            return True

    @classmethod
    async def change_slot(cls, inventory: Inventory, email: str, index: int, sub_index: int, new_contents: Slot, merge: bool = True) -> Slot:
        """Replace a slot in the inventory with the specified new_contents. If new_contents is not provided,
        the slot will be emptied. No checking is done for existing slot data, so if you want to make sure the
        slot is empty before replacing it, use Inventory.slots[index].is_empty first
        """

        # we're putting it into a bag
        if sub_index > -1:
            return await cls._change_bag_slot(inventory, email, index, sub_index, new_contents, merge=merge)

        # get the old slot data
        list = inventory.slots

        # merge them
        orig_contents = list[index]
        return_slot = Slot.clone(orig_contents)
        orig_item: Item = cou_globals.items.get(orig_contents.item_type)
        if merge and orig_contents.item_type == new_contents.item_type:
            room_remaining = orig_item.stacksTo - orig_contents.count
            add_num = min(room_remaining, new_contents.count)

            orig_contents.count += add_num
            new_contents.count -= add_num
            list[index] = orig_contents

            return_slot = new_contents
            if return_slot.count == 0:
                return_slot.is_empty
        else:
            list[index] = new_contents

        # save the new inventory slot data
        await cls._save_db_inventory(inventory, email)
        return return_slot

    @classmethod
    async def get_item_in_slot(cls, slot: int, sub_slot: int, email: str) -> Item:
        return await InventoryAction()._take_item(slot, sub_slot, 0, email, simulate=True)

    @classmethod
    async def add_firefly_to_jar(cls, email: str, socket, amount: int = 1) -> int:
        """Adds [amount] fireflies to a jar in [email]'s inventory. Returns the number
        that couldn't fit.
        """

        to_add = amount

        def _add_to_jar(jar: Slot) -> None:
            in_jar = int(jar.metadata.get("fireflies", 0))
            if to_add < 0 and in_jar >= abs(to_add):
                in_jar += to_add
                to_add = 0
            else:
                while in_jar < 7 and to_add > 0:
                    in_jar += 1
                    to_add -= 1
            jar.metadata["fireflies"] = str(in_jar)

        async with _lock_inventory(email, "addFireflyToJar"):
            inventory = await cls._get_db_inventory(email)
            tmp_slots: list[Slot] = Inventory.clone(inventory).slots
            for slot in tmp_slots:
                # skip empty slots
                if not slot.item_type:
                    continue

                # jars in top-level slots
                if slot.item_type == "firefly_jar":
                    _add_to_jar(slot)

                # jars in bag slots
                if cou_globals.items[slot.item_type].isContainer and cou_globals.items[slot.item_type].subSlots is not None:
                    bag_slots: list[Slot] = Slot.from_json(slot.metadata["slots"], many=True)
                    for bag_slot in [s for s in bag_slots if s.item_type == "firefly_jar"]:
                        _add_to_jar(bag_slot)
                    slot.metadata["slots"] = json.dumps([s.to_json() for s in bag_slots])

            inventory.slots = tmp_slots
            await cls._save_db_inventory(inventory, email)
            await cls.fire_inventory_at_user(socket, email, update=True)
            return to_add

    @classmethod
    async def decrease_durability(cls, email: str, valid_types: str|list[str], amount: int = 1) -> bool:
        """[valid_types] can be either a str or a list[str] which lists the valid item types
        from which to take durability. [amount] is the amount to add to the item's durability_used
        """

        if isinstance(valid_types, str):
            valid_types = [valid_types]

        async with _lock_inventory(email, "decreaseDurability"):
            socket = cou_globals.user_sockets[email]
            success = await InventoryAction()._decrease_durability(valid_types, amount, email)
            if success:
                await cls.fire_inventory_at_user(socket, email, update=True)

        return success

    @classmethod
    async def _change_bag_slot(cls, inventory: Inventory, email: str, bag_index: int, bag_slot_index: int, new_contents: Slot, merge: bool = True) -> Slot:
        """Replace a slot (bag_slot_index) of a bag (bag_index) in the inventory with the specified new_contents.
        If new_contents is not provided, the slot will be emptied. No checking is done for existing slot data, so
        if you want to make sure the slot is empty before replacing it, use Inventory.slots[index].is_empty first.
        """

        new_item: Item = cou_globals.items.get(new_contents.item_type)
        if new_contents.item_type is not None and new_item is not None and new_item.isContainer:
            return None

        bag_item: Item = cou_globals.items[inventory.slots[bag_index].item_type]
        if not bag_item.filter_allows(item_type=new_contents.item_type):
            return None

        # read down the slot tree
        inv_slots = inventory.slots # hotbar
        bag_slot = inv_slots[bag_index] # bag in hotbar
        if bag_slot.metadata.get("slots") is None:
            # if the bag has no slot data (newly created),
            # fill it with empty slots
            bag_slots = cls._generate_empty_slots(cou_globals.items[bag_slot.item_type].subSlots)
        else:
            # if the bag already has slot data, load it into the list
            bag_slots = Slot.from_json(bag_slot.metadata["slots"], many=True)

        # bag contents
        orig_contents: Slot = bag_slots[bag_slot_index] # slot inside bag
        return_slot: Slot = Slot.clone(orig_contents)
        orig_item: Item = cou_globals.items.get(orig_contents.item_type)
        if merge and orig_contents.item_type == new_contents.item_type:
            room_remaining = orig_item.stacksTo - orig_contents.count
            add_num = min(room_remaining, new_contents.count)

            orig_contents.count += add_num
            new_contents.count -= add_num
            bag_slots[bag_slot_index] = orig_contents

            return_slot = new_contents
            if return_slot.count == 0:
                return_slot.is_empty
        else:
            # change out the bag slot
            bag_slots[bag_slot_index] = new_contents # slot inside bag

        # save up the slot tree
        bag_slot.metadata["slots"] = json.dumps([s.to_json() for s in bag_slots]) # bag contents
        inventory.slots[bag_index] = bag_slot # bag in hotbar
        await cls._save_db_inventory(inventory, email)
        return return_slot

    @classmethod
    def _generate_empty_slots(cls, amt: int = 0) -> list[Slot]:
        slots = []
        for index in range(1, amt + 1):
            slots.append(Slot())

        return slots

class InventoryAction:
    def __init__(self) -> None:
        self.to_merge = 0
        self.merged = 0

    async def _add_item(self, item: Item, count: int, email: str) -> int:
        if item.isContainer and item.metadata.get("slots") is None:
            empty_slots = [Slot() for _ in range(item.subSlots)]
            item.metadata["slots"] = json.dumps([s.to_json() for s in empty_slots])

        # keep a record of how many items we have merged into slots already
        # and how many more need to find homes
        self.to_merge = count
        self.merged = 0

        # Go through entire inventory and try to find a slot that either:
        # a) has the same type of item in it and is nto a full stack, or
        # b) is a specialized container that can accept this item
        # c) is empty and can accept at least [count] of item
        # d) is a generic container and has an available slot
        inventory = await InventoryManager._get_db_inventory(email)

        # check for same itemType already existing in a bag
        for slot in inventory.slots:
            if self.to_merge == 0:
                break

            slot_item: Item = cou_globals.items.get(slot.item_type)
            if slot_item is None:
                continue
            if slot_item.isContainer and not item.isContainer and self._bag_contains(slot, item.itemType):
                inner_slots = self._get_modified_bag(slot, item, allow_empty=False)
                slot.metadata["slots"] = json.dumps([s.to_json() for s in inner_slots])

        # check for same itemType already existing on the slot bar
        for slot in inventory.slots:
            if self.to_merge == 0:
                break

            # if not, decide if we can merge into the slot
            if slot.item_type == item.itemType and slot.count < item.stacksTo and not slot.metadata:
                slot = self._get_modified_slot(slot, item)

        # check for specialized bag
        for slot in inventory.slots:
            slot_item: Item = cou_globals.items.get(slot.item_type)
            if slot_item is None:
                continue
            if self.to_merge == 0:
                break

            if slot_item.isContainer and not item.isContainer and slot_item.filter_allows(item_type=item.itemType) and slot_item.subSlotFilter:
                inner_slots = self._get_modified_bag(slot, item)
                slot.metadata["slots"] = json.dumps([s.to_json() for s in inner_slots])

        # check for empty slot in one of the 10
        for slot in inventory.slots:
            if self.to_merge == 0:
                break

            if not slot.item_type or slot.count == 0:
                slot = self._get_modified_slot(slot, item)

        # check for a generic bag in which we can merge
        for slot in inventory.slots:
            if self.to_merge == 0:
                break

            slot_item: Item = cou_globals.items.get(slot.item_type)
            if not slot_item:
                continue
            if slot_item.isContainer and not item.isContainer and not slot_item.subSlotFilter:
                inner_slots = self._get_modified_bag(slot, item)
                slot.metadata["slots"] = json.dumps([s.to_json() for s in inner_slots])

        inventory_json = json.dumps([s.to_json() for s in inventory.slots])

        if self.to_merge > 0:
            LOGGER.error(f"[Inventory] Cannot give {item.itemType} x {count} because <email={email}> ran out of slots before all items were added.")
            player_id = cou_globals.users.get(await User.get_username_from_email(email))
            if player_id:
                item.put_on_ground(player_id.current_x + 40, player_id.current_y, player_id.current_street, count=self.to_merge)
                LOGGER.debug(f"[Inventory] {self.o_merge} {item.itemType}(s) dropped.")

        async with db_session_context() as db_session:
            db_inventory = (await db_session.scalars(select(InventoryModel).join(User).filter(User.email == email))).one()
            db_inventory.inventory_json = inventory_json

        return self.merged

    async def _take_item(self, slot: int, sub_slot: int, count: int, email: str, simulate: bool = False) -> Item:
        try:
            inventory = await InventoryManager._get_db_inventory(email)
            tmp_slots = Inventory.clone(inventory).slots
            to_modify = tmp_slots[slot]

            # if we're taking from a bag
            if sub_slot > -1:
                bag_slots: list[Slot] = Slot.from_json(to_modify.metadata["slots"], many=True)
                bag_slot_to_modify = bag_slots[sub_slot]
                if bag_slot_to_modify.count < count:
                    return None
                else:
                    if bag_slot_to_modify.count == count:
                        bag_slot_to_modify = Slot()
                    else:
                        bag_slot_to_modify.count -= count

                dropped = bag_slots.pop(sub_slot)
                bag_slots.insert(sub_slot, bag_slot_to_modify)
                to_modify.metadata["slots"] = json.dumps([s.to_json() for s in bag_slots])
                tmp_slots.pop(slot)
            else:
                if to_modify.count < count:
                    return None
                if to_modify.count == count:
                    to_modify = Slot()
                else:
                    to_modify.count -= count

                dropped = tmp_slots.pop(slot)

            tmp_slots.insert(slot, to_modify)

            if not simulate:
                inventory.slots = tmp_slots
                await InventoryManager._save_db_inventory(inventory, email)

            if not dropped.item_type:
                return None

            dropped_item = Item.clone(cou_globals.items[dropped.item_type])
            dropped_item.metadata = dropped.metadata
            return dropped_item
        except Exception:
            return None

    async def _take_any_items(self, item_type: str, count: int, email: str, simulate: bool = False) -> int:
        item: Item = cou_globals.items.get(item_type)
        if item is None:
            LOGGER.warning(f"Could not get item from type {item_type}")
            return 0

        # keep a record of how many items we have taken from slots already,
        # and how many more we need to remove
        to_grab = count
        grabbed = 0

        # Go through entire inventory and try to find a slot that has this item,
        # and continue until all are taken
        inventory = await InventoryManager._get_db_inventory(email)
        tmp_slots: list[Slot] = Inventory.clone(inventory).slots
        for index, slot in enumerate(tmp_slots):
            # check if we are done taking, then stop looping
            if to_grab == 0:
                break

            slot_item: Item = cou_globals.items.get(slot.item_type)
            if slot_item is None:
                continue
            if slot_item.isContainer and slot_item.filter_allows(item_type=item.itemType):
                inner_slots: list[Slot] = Slot.from_json(slot.metadata.get("slots", []), many=True)
                for sub_index, inner_slot in enumerate(inner_slots):
                    # check if we are done taking, then stop looping
                    if to_grab == 0:
                        break
                    # does this slot have the type of item we are taking?
                    if inner_slot.item_type != item.itemType:
                        continue

                    # skip empty slots
                    if inner_slot.is_empty:
                        continue

                    have = inner_slot.count
                    if have >= to_grab:
                        diff = to_grab
                        inner_slot.count -= to_grab
                    else:
                        diff = have
                        inner_slot.count = 0

                    if inner_slot.count == 0:
                        inner_slots[sub_index] = Slot()

                    # update counters and move to the next slot
                    to_grab -= diff
                    grabbed += diff
                slot.metadata["slots"] = json.dumps([s.to_json() for s in inner_slots])

            # does this slot have the type of item we are taking?
            if slot.item_type != item.itemType:
                continue

            # skip empty slots
            if slot.is_empty:
                continue

            # skip containers that are not empty
            if item.isContainer:
                if "slots" in slot.metadata:
                    inner_slots: list[Slot] = Slot.from_json(slot.metadata["slots"], many=True)
                    is_empty = True
                    for inner_slot in inner_slots:
                        if inner_slot.item_type:
                            is_empty = False
                    if not is_empty:
                        continue

            have = slot.count
            if have >= to_grab:
                diff = to_grab
                slot.count -= to_grab
            else:
                diff = have
                slot.count = 0

            if slot.count == 0:
                tmp_slots[index] = Slot()

            # update counters and move to the next slot
            to_grab -= diff
            grabbed += diff

        if to_grab > 0:
            # abort - if we can't have it all, we can't have any
            if not simulate:
                LOGGER.warning(
                    f"[Inventory] Cannot take {item.itemType} x {count} because the user ran out of slots"
                    f" before all items were taken. {to_grab} items skipped."
                )
            return 0
        else:
            if not simulate:
                inventory.slots = tmp_slots
                await InventoryManager._save_db_inventory(inventory, email)

            return grabbed

    def _bag_contains(self, slot: Slot, item_type: str) -> bool:
        if "slots" not in slot.metadata:
            return False

        inner_slots: list[Slot] = Slot.from_json(slot.metadata["slots"], many=True)
        for inner_slot in inner_slots:
            if inner_slot.item_type == item_type:
                return True

        return False

    def _get_modified_slot(self, slot: Slot, item: Item) -> Slot:
        empty_slot = not slot.item_type or slot.count == 0

        # figure out how many we can merge
        avail_in_stack = item.stacksTo - slot.count

        if avail_in_stack >= self.to_merge:
            slot.count += self.to_merge
            self.merged += self.to_merge
            self.to_merge = 0
        else:
            slot.count += avail_in_stack
            self.merged += avail_in_stack
            self.to_merge -= avail_in_stack

        # if the slot was empty, give it some data
        if empty_slot:
            slot.item_type = item.itemType
            slot.metadata = item.metadata

        return slot

    def _get_modified_bag(self, slot: Slot, item: Item, allow_empty: bool = True) -> list[Slot]:
        slot_item = cou_globals.items.get(slot.item_type)

        if "slots" in slot.metadata:
            inner_slots = Slot.from_json(slot.metadata["slots"], many=True)
        else:
            inner_slots = [Slot() for _ in range(slot_item.subSlots)]

        for slot in inner_slots:
            # Check if we are done merging, then stop looping
            if self.to_merge == 0:
                break

            # if not, decide if we can merge into the slot
            can_merge = False
            empty_slot = False

            if slot.is_empty and allow_empty:
                can_merge = True
                empty_slot = True
            else:
                if slot.item_type == item.itemType and slot.count < item.stacksTo and not slot.metadata:
                    can_merge = True

            # if this slot is suitable...
            if can_merge:
                # figure out how many we can merge
                avail_in_stack = item.stacksTo - slot.count
                if avail_in_stack >= self.to_merge:
                    slot.count += self.to_merge
                    self.merged += self.to_merge
                    self.to_merge = 0
                else:
                    slot.count += avail_in_stack
                    self.merged += avail_in_stack
                    self.to_merge -= avail_in_stack

            # if the slot was empty, give it some data
            if empty_slot:
                slot.item_type = item.itemType
                slot.metadata = item.metadata
            else:
                # if not, skip it
                continue

        return inner_slots

    async def _decrease_durability(self, valid_types: list[str], amount: int, email: str) -> bool:
        possibles: list[DurabilitySlot] = []

        inventory = await InventoryManager._get_db_inventory(email)
        tmp_slots: list[Slot] = Inventory.clone(inventory).slots
        for item_type in valid_types:
            sample: Item = cou_globals.items[item_type]
            # look in the regular slots
            for index, slot in enumerate(tmp_slots):
                if slot.item_type == item_type:
                    used = int(slot.metadata.get("durabilityUsed", 0))
                    if used + amount > sample.durability:
                        continue
                    remaining = 100 * ((sample.durability - used) / sample.durability)
                    possibles.append(DurabilitySlot(remaining, index))

        for item_type in valid_types:
            sample = cou_globals.items[item_type]
            # add the bag contents
            for index, slot in enumerate(tmp_slots):
                if slot.item_type and cou_globals.items[slot.item_type].isContainer and cou_globals.items[slot.item_type].subSlots:
                    if len(slot.metadata.get("slots", [])) > 0:
                        bag_slots: list[Slot] = Slot.from_json(slot.metadata["slots"], many=True)
                        for sub_index, bag_slot in enumerate(bag_slots):
                            if bag_slot.item_type == item_type:
                                used = int(bag_slot.metadata.get("durabilityUsed", 0))
                                if used + amount > sample.durability:
                                    continue
                                remaining = 100 * ((sample.durability - used) / sample.durability)
                                possibles.append(DurabilitySlot(remaining, index, sub_slot=sub_index))

        # remove broken tools
        possibles = [ds for ds in possibles if ds.percent_remaining > 0]

        if possibles:
            # sort the list and pick the one with the most used already
            possibles.sort(reverse=True)
            most_used = possibles.pop(0)
            newly_broken = None

            # write it to the tmp_slots array
            if most_used.sub_slot == -1:
                slot_to_modify = tmp_slots[most_used.slot]
                used = int(slot_to_modify.metadata.get("durabilityUsed", 0))
                used += amount
                slot_to_modify.metadata["durabilityUsed"] = str(used)
                tmp_slots[most_used.slot] = slot_to_modify

                if used == cou_globals.items[slot_to_modify.item_type].durability:
                    newly_broken = cou_globals[slot_to_modify.item_type].name
            else:
                # have to modify a bag slot
                bag = tmp_slots[most_used.slot]
                bag_slots: list[Slot] = Slot.from_json(bag.metadata["slots"], many=True)
                bag_slot = bag_slots[most_used.sub_slot]
                used = int(bag_slot.metadata.get("durabilityUsed", 0))
                used += amount
                bag_slot.metadata["durabilityUsed"] = str(used)
                bag.metadata["slots"] = json.dumps([s.to_json() for s in bag_slots])
                tmp_slots[most_used.slot] = bag

                if used == cou_globals.items[bag_slot.item_type].durability:
                    newly_broken = cou_globals.items[bag_slot.item_type].name

            # finally save the array as the new inventory
            inventory.slots = tmp_slots
            await InventoryManager._save_db_inventory(inventory, email)

            if newly_broken:
                await toast(f"Yikes! Your {newly_broken} just broke.", cou_globals.user_sockets[email])

            return True

        return False


@functools.total_ordering
@dataclasses.dataclass
class DurabilitySlot:
    """private class for sorting slots by durability used"""
    percent_remaining: float
    slot: int
    sub_slot: int = -1

    def __lt__(self, other: "DurabilitySlot") -> bool:
        if other is None:
            return False
        return self.percent_remaining < other.percent_remaining

    def __eq__(self, other: "DurabilitySlot") -> bool:
        if other is None:
            return False
        return self.percent_remaining == other.percent_remaining



#   /**
# 	 * Updates the inventory's JSON representation
# 	 * with its current slot contents.
# 	 */
#   void updateJson() {
#     if (slots is List) {
#       inventory_json = jsonx.encode(slots);
#     }
#   }

#   // Public Methods /////////////////////////////////////////////////////////////////////////////

#   // Return the inventory as a List<Map>, where each slot is a Map in the List
#   // Can then be READ by other functions (but not written to)
#   List<Map> getItems() {
#     return JSON.decode(inventory_json);
#   }

#   bool _durabilityOk(Slot slot) {
#     if (!slot.metadata.containsKey('durabilityUsed')) {
#       return true;
#     } else {
#       int used = int.parse(slot.metadata['durabilityUsed'],
#           onError: (String source) => 0);
#       int max = items[slot.itemType].durability;
#       if (items[slot.itemType].durability == null) {
#         return true;
#       } else {
#         return (used < max);
#       }
#     }
#   }

#   // Returns the number of a certain item a user has
#   int countItem(String itemType, {bool includeBroken: true}) {
#     int count = 0;

#     //count all the normal slots
#     slots.forEach((Slot s) {
#       if (s.itemType == itemType && (includeBroken || _durabilityOk(s))) {
#         count += s.count;
#       }
#     });

#     //add the bag contents
#     slots
#         .where((Slot s) =>
#             !s.itemType.isEmpty &&
#             items[s.itemType].isContainer &&
#             items[s.itemType].subSlots != null)
#         .forEach((Slot s) {
#       if (s.metadata["slots"] != null && (s.metadata["slots"]).length > 0) {
#         List<Slot> bagSlots =
#             jsonx.decode(s.metadata['slots'], type: listOfSlots);
#         if (bagSlots != null) {
#           bagSlots.forEach((Slot bagSlot) {
#             if (bagSlot.itemType == itemType &&
#                 (includeBroken || _durabilityOk(bagSlot))) {
#               count += bagSlot.count;
#             }
#           });
#         }
#       }
#     });

#     return count;
#   }

#   static Future _wait(Duration duration) {
#     Completer c = new Completer();
#     new Timer(duration, () => c.complete());
#     return c.future;
#   }

#   static Future<bool> _aquireLock(String email, String reason) async {
#     int numTriesLeft = 100; //we'll throw an error after 5 seconds of trying
#     if (inventoryLocked[email] != null) {
#       while (inventoryLocked[email].length > 0 && numTriesLeft > 0) {
#         await _wait(new Duration(milliseconds: 50));
#         numTriesLeft--;
#       }

#       if (((inventoryLocked[email] ?? []) as List).length > 0) {
#         Log.warning(
#             "Could not acquire a lock for inventory of <email=$email> for $reason because ${inventoryLocked[email]}");
#         return false;
#       }
#     }

#     inventoryLocked[email] = (inventoryLocked[email] ?? []);
#     inventoryLocked[email].add(reason);
#     return true;
#   }

#   static void _releaseLock(String email, String reason) {
#     inventoryLocked[email].remove(reason);
#   }

#   // Static Public Methods //////////////////////////////////////////////////////////////////////

#   static Future<bool> hasItem(String email, String itemType, int count) async {
#     return (await takeAnyItemsFromUser(email, itemType, count,
#             simulate: true)) >=
#         count;
#   }

#   static Future<bool> hasUnbrokenItem(String email, String itemType, int count,
#       {bool notifyIfBroken: false}) async {
#     InventoryV2 inv = await getInventory(email);
#     bool result =
#         (await inv.countItem(itemType, includeBroken: false)) >= count;

#     if (notifyIfBroken && !result && (await hasItem(email, itemType, count))) {
#       // Player has a broken item of the same type
#       toast("Your ${items[itemType].name} is broken",
#           StreetUpdateHandler.userSockets[email]);
#     }

#     return result;
#   }

#   /**
# 	 * [validTypes] can be either a String or a List<String> which lists the valid
# 	 * item type from which to take durability.
# 	 * [amount] is the amount to add to the item's durabilityUsed
# 	 */
#   static Future<bool> decreaseDurability(String email, dynamic validTypes,
#       {int amount: 1}) async {
#     assert(validTypes is String || validTypes is List<String>);

#     List<String> types;
#     if (validTypes is String) {
#       types = [validTypes];
#     } else {
#       types = validTypes;
#     }

#     if (!(await _aquireLock(email, 'decreaseDurability'))) {
#       return false;
#     }

#     WebSocket userSocket = StreetUpdateHandler.userSockets[email];
#     InventoryV2 inv = await getInventory(email);

#     bool success = await inv._decreaseDurability(types, amount, email);

#     if (success) {
#       await fireInventoryAtUser(userSocket, email, update: true);
#     }

#     _releaseLock(email, 'decreaseDurability');
#     return success;
#   }

#   Slot getSlot(int invIndex, [int bagIndex]) {
#     if (bagIndex == null) {
#       try {
#         return slots[invIndex];
#       } catch (e, st) {
#         Log.error('Error accessing inventory slot $invIndex', e, st);
#         return new Slot();
#       }
#     } else {
#       try {
#         String mdsString = slots[invIndex].metadata["slots"];
#         Map<String, dynamic> mdsSlot = jsonx.decode(mdsString)[bagIndex];
#         return new Slot.withMap(mdsSlot);
#       } catch (e, st) {
#         Log.error(
#             'Error accessing bag slot $bagIndex of inventory slot $invIndex',
#             e,
#             st);
#         return new Slot();
#       }
#     }
#   }
# }


@inventory_endpoint.route("/getInventory/username/<username>")
async def get_inventory_by_username(username: str):
    async with db_session_context() as db_session:
        results = await db_session.scalars(select(InventoryModel).join(User).filter(User.username == username))
        inventory = results.one_or_none()
        if not inventory:
            return jsonify([])
        return jsonify(await InventoryManager._serialize_inventory((await inventory.awaitable_attrs.user).email))


@inventory_endpoint.route("/getInventory/<email>")
async def get_inventory_by_email(email: str):
    inventory = await InventoryManager._serialize_inventory(email)
    if not inventory:
        return jsonify([])
    return jsonify(inventory)
