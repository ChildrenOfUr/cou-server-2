from quart import Blueprint, request, Response
from quart_cors import cors
from random import choice

letters_blueprint = Blueprint("letters_blueprint", __name__, url_prefix="/letters")
cors(letters_blueprint)

_LETTER_ASSIGNMENTS = dict()

_LETTERS = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
]

_LETTERS_H = _LETTERS + ["_heart"]


def random_letter(heart: bool = False) -> str:
    """Return a random letter a-z.

    Set 'heart' to true to include a chance (1/27) for _heart.
    """
    return choice(_LETTERS_H if heart else _LETTERS)


def new_player_letter(username: str) -> str:
    """
    Changes the player's assigned letter to a random letter.

    Used on street change.
    """
    if not username:
        username = request.args.get("username", default=str(), type=str)

    _LETTER_ASSIGNMENTS[username] = random_letter(heart=True)
    return _LETTER_ASSIGNMENTS[username]


def get_player_letter(username: str) -> str:
    """Return the player's assigned letter."""
    if username in _LETTER_ASSIGNMENTS:
        return _LETTER_ASSIGNMENTS[username]

    return new_player_letter(username)


@letters_blueprint.route("/getPlayerLetter")
async def _get_player_letter():
    username = request.args.get("username", default=str(), type=str)
    return Response(get_player_letter(username), mimetype="text/plain")


@letters_blueprint.route("/newPlayerLetter")
async def _new_player_letter():
    username = request.args.get("username", default=str(), type=str)
    return Response(new_player_letter(username), mimetype="text/plain")
