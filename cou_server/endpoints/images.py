import base64
import io
import json
from pathlib import Path
import re
from urllib.parse import quote_plus

import aiohttp
from quart import Blueprint, Response, jsonify, request
from quart_cors import cors
from PIL import Image
from sqlalchemy.future import select

from cou_server.common import CACHE_DIR
from cou_server.common.database import db_session_context
from cou_server.common.file_cache import FileCache
from cou_server.models.user import User


images_blueprint = Blueprint("images_blueprint", __name__)
cors(images_blueprint)


AVATAR_IMAGE_CACHE_DIR = Path(CACHE_DIR, "avatar_images")

if not AVATAR_IMAGE_CACHE_DIR.is_dir():
    AVATAR_IMAGE_CACHE_DIR.mkdir()


@images_blueprint.route("/setCustomAvatar")
async def set_custom_avatar():
    username = request.args.get("username")
    avatar = request.args.get("avatar")

    if avatar == username:
        avatar = None

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.username == username))).first()
        user.custom_avatar = avatar

    FileCache.heads_cache.pop(username, None)
    FileCache.heads_cache.pop(f"{username}.fullheight", None)


@images_blueprint.route("/getSpritesheets")
async def get_spritesheets():
    username = request.args.get("username", None)
    no_custom_avatars = request.args.get("noCustomAvatars", False)

    if not username:
        return jsonify({})

    if not no_custom_avatars:
        async with db_session_context() as db_session:
            user = (await db_session.scalars(select(User).filter(User.username == username))).first()
            if user and user.custom_avatar:
                username = user.custom_avatar

    cache_file = Path(AVATAR_IMAGE_CACHE_DIR, f"{username.lower()}.json")
    try:
        with open(cache_file, "r") as cache:
            return jsonify(json.loads(cache.read()))
    except Exception:
        with open(cache_file, "w") as cache:
            spritesheets = await _get_spritesheets_from_web(username)
            cache.write(json.dumps(spritesheets))
            return jsonify(spritesheets)


async def _get_spritesheets_from_web(username):
    spritesheets = {}
    url = f"http://www.glitchthegame.com/friends/search/?q={quote_plus(username)}"
    async with aiohttp.ClientSession(headers={"User-Agent": "Mozilla/5.0"}) as session:
        async with session.get(url) as response:
            response = await response.text()

        regex = f'/profiles/(.+)/" class="friend-name">{username}'

        match = re.search(regex, response, re.IGNORECASE)
        if match:
            tsid = match.group(1)
            async with session.get(f"http://www.glitchthegame.com/profiles/{tsid}") as response:
                sheets = [
                    "base",
                    "angry",
                    "climb",
                    "happy",
                    "idle1",
                    "idle2",
                    "idle3",
                    "idleSleepy",
                    "jump",
                    "surprise",
                ]
                for sheet in sheets:
                    regex = f'"(.+{sheet}.png)"'
                    spritesheets[sheet] = re.search(regex, await response.text()).group(1)

            return spritesheets
        else:
            return await _get_spritesheets_from_web("Elle Lament")


@images_blueprint.route("/getActualImageHeight")
async def get_actual_image_height():
    image_url = request.args.get("url")
    num_rows = int(request.args.get("numRows"))
    num_columns = int(request.args.get("numColumns"))

    if FileCache.heights_cache.get(image_url) is not None:
        return str(FileCache.heights_cache[image_url])

    try:
        async with aiohttp.ClientSession(headers={"User-Agent": "Mozilla/5.0"}) as session:
            async with session.get(image_url) as response:
                img_file = io.BytesIO(await response.read())
    except Exception:
        return "0"

    image = Image.open(img_file).convert("RGBA")
    single_frame = image.crop(
        (0, 0, image.width // num_columns, image.height // num_rows)
    )
    trimmed_frame = single_frame.crop(single_frame.getbbox())
    FileCache.heights_cache[image_url] = trimmed_frame.height
    return str(trimmed_frame.height)


@images_blueprint.route("/trimImage")
async def trim_image():
    username = request.args.get("username")
    no_custom_avatars = request.args.get("noCustomAvatars", False)
    full_height = request.args.get("fullHeight", False)
    cache_key = f"{username}.fullheight" if full_height else username

    if FileCache.heads_cache.get(cache_key):
        return FileCache.heads_cache[cache_key]

    spritesheet = await (await get_spritesheets()).get_json()
    image_url = spritesheet["base"]
    if not image_url:
        return ""

    async with aiohttp.ClientSession(headers={"User-Agent": "Mozilla/5.0"}) as session:
        async with session.get(image_url) as response:
            img_file = io.BytesIO(await response.read())

    # int frameWidth = image.width ~/ 15;
	# 	int frameHeightScl = ((fullHeight != null && fullHeight)
	# 		? image.height // full height
	# 		: image.height ~/ 1.5); // head only
	# 	image = copyCrop(image, image.width - frameWidth, 0, frameWidth, frameHeightScl);
	# 	List<int> trimRect = findTrim(image, mode: TRIM_TRANSPARENT);
	# 	Image trimmed = copyCrop(image, trimRect[0], trimRect[1], trimRect[2], trimRect[3]);

    image = Image.open(img_file).convert("RGBA")
    frame_width = int(image.width / 15)
    frame_height_scl = image.height if full_height else int(image.height / 1.5)
    image = image.crop((image.width - frame_width, 0, image.width, frame_height_scl))
    image = image.crop(image.getbbox())  # trim the transparent part

    png_bytes = io.BytesIO()
    image.save(png_bytes, format="PNG")
    encoded = base64.encodebytes(png_bytes.getvalue())
    encoded = encoded.decode().replace("\n", "").encode()
    # encoded = base64.encodebytes(image.tobytes())
    FileCache.heads_cache[cache_key] = encoded
    return encoded
