from quart import Blueprint, jsonify, request
from quart_cors import cors
from sqlalchemy.future import select

from cou_server import api_keys
from cou_server.common.database import db_session_context
from cou_server.models.user import User

elevation_endpoint = Blueprint("elevation_endpoint", __name__, url_prefix="/elevation")
cors(elevation_endpoint)

DEFAULT = "_"
_cache = {}


@elevation_endpoint.route("/get/<string:username>")
async def elevation_get(username):
    if username in _cache:
        return _cache[username]

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.username == username))).first()
        elevation_str = DEFAULT
        if user:
            elevation_str = user.elevation
        _cache[username] = elevation_str
        return elevation_str


@elevation_endpoint.route("/list/<string:status>")
async def list_status(status):
    response = []
    async with db_session_context() as db_session:
        users = (await db_session.scalars(select(User).filter(User.elevation == status))).all()
        for user in users:
            last_login = user.last_login if user.last_login else "never"
            response.append({"username": user.username, "last_login": last_login})

    return jsonify(response)


@elevation_endpoint.route("/set")
async def set_elevation():
    token = request.args.get("token")
    channel = request.args.get("channel")
    text = request.args.get("text")

    if token != api_keys.SLACK_PROMOTE_TOKEN:
        return "Invalid token"

    if channel != "G0277NLQS":
        return "Run this from the administration group"

    try:
        parts = text.split(" ")
        elevation = parts[0].strip()
        username = parts[1:].join(" ")
    except Exception:
        return "Invalid paramters"

    if elevation == "none":
        elevation = DEFAULT

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.username == username))).first()
        user.elevation = elevation
        _cache[username] = elevation
        if elevation != DEFAULT:
            suffix = f"a {elevation}"
        else:
            suffix = "demoted"
        return f"{username} is now {suffix}."
