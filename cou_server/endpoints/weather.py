import asyncio
import json
import urllib.request

from datetime import datetime, timedelta, timezone
from numbers import Number

from quart import Blueprint, websocket

from cou_server.api_keys import OWM_API_KEY
from cou_server.common import TZ_UTC, get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints.mapdata import get_street_by_tsid, hubs

weather_blueprint = Blueprint("weather_blueprint", __name__)

logger = get_logger(__name__)

# OpenWeather API documentation: https://openweathermap.org/current
#
# Define _OWM_API in api_keys.py, using a free license key. Note this project
# uses the One Call API, which requires no additional licensing.
_OWM_API = "http://api.openweathermap.org/data/2.5/onecall"
_OWM_PARAMS = f"?appid={OWM_API_KEY}"

_WEATHER_CACHE: dict[int, "WeatherData"] = {}
_WEATHER_SOCKETS: dict[str, websocket] = {}
_deferred_minutes = 0


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for socket in _WEATHER_SOCKETS.values():
        await socket.close(1000, "Server Shutdown")


class WeatherData:
    _OWM_IMG = "https://openweathermap.org/img/w/"

    @staticmethod
    def k_to_f(kelvin: float) -> float:
        """Convert a temperature in Kelvin to degrees Fahrenheit."""
        return (kelvin * (9 / 5)) - 459.67

    def __init__(self, owm: dict):
        self.latitude = owm.get("lat")
        self.longitude = owm.get("lon")

        # Because we use `owm_download` for current and forecasted weather, we
        # need to specify exactly what results are specific to current weather.
        if owm.get("current"):
            self.calc_date = datetime.fromtimestamp(owm["current"]["dt"], TZ_UTC)
            self.sunrise = datetime.fromtimestamp(owm["current"]["sunrise"], TZ_UTC)
            self.sunset = datetime.fromtimestamp(owm["current"]["sunset"], TZ_UTC)
            self.temp = WeatherData.k_to_f(owm["current"]["temp"])
            self.pressure = owm["current"]["pressure"]
            self.sea_level_pressure = owm["current"]["pressure"] # deprecated in One Call
            self.humidity = owm["current"]["humidity"]
            self.clouds = owm["current"]["clouds"]
            self.wind_speed = owm["current"]["wind_speed"]
            self.wind_deg = owm["current"]["wind_deg"]
            self.weather_id = owm["current"]["weather"][0]["id"]
            self.weather_main = owm["current"]["weather"][0]["main"]
            self.weather_description = owm["current"]["weather"][0]["description"]
            self.weather_icon = self._OWM_IMG + owm["current"]["weather"][0]["icon"] + ".png"

            if "rain" in owm["current"]:
                self.rain_vol = owm["current"]["rain"]["1h"]
            else:
                self.rain_vol = 0

            if "snow" in owm["current"]:
                self.snow_vol = owm["current"]["snow"]["1h"]
            else:
                self.snow_vol = 0

        else:
            self.calc_date = datetime.fromtimestamp(owm["dt"], TZ_UTC)
            self.sunrise = datetime.fromtimestamp(owm["sunrise"], TZ_UTC)
            self.sunset = datetime.fromtimestamp(owm["sunset"], TZ_UTC)
            # These values defined in `temp` only in the case of `daily`, `hourly`,
            # and `'minutely`. We only care about the `daily` forecast.
            self.temp_min = WeatherData.k_to_f(owm["temp"]["min"])
            self.temp_max = WeatherData.k_to_f(owm["temp"]["max"])
            # Define `temp` using a formula when parsing the `daily` forecast.
            self.temp = (self.temp_min + self.temp_max) / 2
            self.temp_day = WeatherData.k_to_f(owm["temp"]["day"])
            self.temp_night = WeatherData.k_to_f(owm["temp"]["night"])
            self.temp_eve = WeatherData.k_to_f(owm["temp"]["eve"])
            self.temp_morn = WeatherData.k_to_f(owm["temp"]["morn"])
            self.pressure = owm["pressure"]
            self.sea_level_pressure = owm["pressure"] # deprecated in One Call
            self.humidity = owm["humidity"]
            self.clouds = owm["clouds"]
            self.wind_speed = owm["wind_speed"]
            self.wind_deg = owm["wind_deg"]
            self.weather_id = owm["weather"][0]["id"]
            self.weather_main = owm["weather"][0]["main"]
            self.weather_description = owm["weather"][0]["description"]
            self.weather_icon = self._OWM_IMG + owm["weather"][0]["icon"] + ".png"

            if "rain" in owm and isinstance(owm["rain"], Number):
                self.rain_vol = owm["rain"]
            else:
                self.rain_vol = 0

            if "snow" in owm and isinstance(owm["snow"], Number):
                self.snow_vol = owm["snow"]
            else:
                self.snow_vol = 0

    def to_dict(self):
        return {
            "latitude": getattr(self, "latitude", None),
            "longitude": getattr(self, "longitude", None),
            "weatherId": getattr(self, "weather_id", None),
            "weatherMain": getattr(self, "weather_main", None),
            "weatherDesc": getattr(self, "weather_description", None),
            "weatherIcon": getattr(self, "weather_icon", None),
            "temp": getattr(self, "temp", None),
            "tempMin": getattr(self, "temp_min", None),
            "tempMax": getattr(self, "temp_max", None),
            "tempDay": getattr(self, "temp_day", None),
            "tempNight": getattr(self, "temp_night", None),
            "tempEve": getattr(self, "temp_eve", None),
            "tempMorn": getattr(self, "temp_morn", None),
            "humidity": getattr(self, "humidity", None),
            "pressure": getattr(self, "pressure", None),
            "seaLevelPressure": getattr(self, "sea_level_pressure", None),
            "windSpeed": getattr(self, "wind_speed", None),
            "windDeg": getattr(self, "wind_deg", None),
            "clouds": getattr(self, "clouds", None),
            "rainVol": getattr(self, "rain_vol", None),
            "snowVol": getattr(self, "snow_vol", None),
            "calcDateTxt": str(getattr(self, "calc_date", None)),
            "countryCode": getattr(self, "country_code", None),
            "sunriseTxt": str(getattr(self, "sunrise", None)),
            "sunsetTxt": str(getattr(self, "sunset", None)),
            "cityId": getattr(self, "city_id", None),
            # "cityName": getattr(self, "city_name", None),
        }


class WeatherLocation:
    _MAX_AGE = timedelta(hours=1)

    def __init__(self, current: WeatherData, forecast: list, dt: datetime = None):
        self.current = current
        self.forecast = forecast
        self.datetime = dt if dt else datetime.now(timezone.utc)

    @property
    def expired(self) -> bool:
        return datetime.now(timezone.utc) > self.datetime + self._MAX_AGE

    def to_dict(self):
        return {
            "current": self.current.to_dict(),
            "forecast": [f.to_dict() for f in self.forecast],
        }


def get_conditions(tsid: str, using_hub_id: bool = False) -> WeatherData|None:
    """Return the weather data for a TSID, or None if the TSID does not have weather."""
    city_id, city_lat, city_lon = get_city_id(tsid, using_hub_id)

    if not city_id:
        return None

    if city_id in _WEATHER_CACHE and not _WEATHER_CACHE[city_id].expired:
        return _WEATHER_CACHE[city_id]

    return download(city_id, city_lat, city_lon)


def get_conditions_dict(tsid: str, using_hub_id: bool = False) -> dict:
    """Return the weather data for a TSID as a dict."""
    weather = get_conditions(tsid, using_hub_id)
    if weather:
        return weather.to_dict()
    else:
        return {"error": "no_weather", "hub_id" if using_hub_id else "tsid": tsid}


def get_city_id(tsid, using_hub_id: bool = False) -> int|None:
    """Return the city id for a TSID or None if it doesn't have one."""

    if not using_hub_id:
        street = get_street_by_tsid(tsid)

        # TODO
        # Technically, City ID should not be used anymore. Remove later, if not
        # used elsewhere.
        street_city_id = street.get("owm_city_id", None)
        street_city_lat = street.get("owm_city_lat", None)
        street_city_lon = street.get("owm_city_lon", None)
        if street_city_id:
            return street_city_id, street_city_lat, street_city_lon

    # check hub
    hub_id = tsid if using_hub_id else street["hub_id"]
    hub = hubs()[str(hub_id)]
    return hub.get("owm_city_id", None), hub.get("owm_city_lat", None), hub.get("owm_city_lon", None)


@weather_blueprint.websocket("/weather")
async def handle():
    """Add a new client."""

    while True:
        try:
            data = json.loads(await websocket.receive())
            asyncio.create_task(_process_message(websocket._get_current_object(), data))
        except asyncio.CancelledError:
            _cleanup_list(websocket._get_current_object())
            raise


async def _process_message(web_socket, msg_json: dict):
    """Handle incoming messages from clients."""
    username = msg_json["username"]
    tsid = msg_json["tsid"]
    hub = msg_json.get("hub", None)

    # Add reference to user if not already stored
    if username not in _WEATHER_SOCKETS:
        _WEATHER_SOCKETS[username] = web_socket

    # Send the current weather to the just connected user
    if tsid:
        # Get weather data for location
        await web_socket.send(json.dumps(get_conditions_dict(tsid)))
    elif hub:
        web_socket.send(
            json.dumps(
                {
                    "conditions": get_conditions_dict(hub, using_hub_id=True),
                    "hub_id": hub,
                    "hub_label": hubs()[hub]["name"],
                }
            )
        )
    else:
        # Client will retry when it is done loading
        await web_socket.send("Street not loaded")
        await web_socket.close()


def _cleanup_list(web_socket):
    """Remove disconnected clients."""
    leaving_user: str = None

    for username, socket in _WEATHER_SOCKETS.items():
        if web_socket == socket:
            _WEATHER_SOCKETS[username] = None
            leaving_user = username

    del _WEATHER_SOCKETS[leaving_user]


def _owm_download(dl_city_lat: str, dl_city_lon: str):
    url = _OWM_API +  _OWM_PARAMS + '&lat=' + str(dl_city_lat) + '&lon=' + str(dl_city_lon)
    resource = urllib.request.Request(url)
    data = urllib.request.urlopen(resource).read().decode()
    owm = json.loads(data)
    return owm


def download(city_id: int = None, city_lat: int = None, city_lon: int = None) -> WeatherData|None:
    """
    Update cached weather data from OpenWeatherMap.

    Pass cityId to download for one city (and return the data),
    or not to refresh the entire cache (and return true).
    """
    global _deferred_minutes, _last_check

    # Waiting for recovery?
    if _deferred_minutes > 0:
        if not _last_check:
            _last_check = datetime.now(timezone.utc)

        # Decrement each minute
        if _last_check.minute < datetime.now(timezone.utc).minute:
            _deferred_minutes -= 1

        return None
    else:
        _last_check = datetime.now(timezone.utc)

    if not city_id:
        for cached_city_id in _WEATHER_CACHE.keys():
            download(cached_city_id)
    else:
        try:
            # Get current conditions
            current = WeatherData(_owm_download(city_lat, city_lon))

            # Get forecast conditions
            forecast = list()
            days = _owm_download(city_lat, city_lon)["daily"]
            for day in days[1:5]:
                forecast.append(WeatherData(day))

            # Assemble location data
            weather = WeatherLocation(current, forecast)

            # Add to cache
            _WEATHER_CACHE[city_id] = weather

            return weather
        except Exception as e:
            logger.exception(f"Error downloading weather for city id '{city_id}'.")
            _deferred_minutes += 1
            return None


def is_raining_in(tsid: str) -> bool:
    """Whether the weather is rainy on a given street."""
    weather = get_conditions(tsid)

    if not weather:
        return False

    return (
        "rain" in weather.current.weather_main.lower()
        or "rain" in weather.current.weather_description.lower()
    )


async def update_all_clients():
    """Send data to all clients."""
    for username, socket in _WEATHER_SOCKETS.items():
        if username not in cou_globals.users:
            continue

        tsid = cou_globals.users[username].tsid

        await socket.send(
            json.dumps(get_conditions_dict(tsid))
        )


async def simulate_weather():
    logger.debug("Simulating weather forever")
    while True:
        await update_all_clients()
        await asyncio.sleep(5)
