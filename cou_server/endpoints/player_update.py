import asyncio
from datetime import datetime
import json
import math

from quart import Blueprint, websocket
from sqlalchemy.future import select

from cou_server import MIN_CLIENT_VER
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.identifier import Identifier
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints import letters, quest
from cou_server.models.user import User
from cou_server.quests.messages import RequirementProgress, PlayerPosition


logger = get_logger(__name__)
player_blueprint = Blueprint("player_blueprint", __name__)

users: dict[str, Identifier] = {}
cou_globals.users = users
message_post_counter = {}


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for user in users.values():
        await user.socket.close(1000, "Server Shutdown")


@player_blueprint.websocket("/playerUpdate")
async def chat_ws():
    while True:
        data = json.loads(await websocket.receive())
        asyncio.create_task(processMessage(websocket._get_current_object(), data))


async def processMessage(socket, message):
    try:
        data = message
        if data.get("clientVersion") is not None:
            if data["clientVersion"] < MIN_CLIENT_VER:
                await socket.send(json.dumps({"error": "version to low"}))
            return

        username = data.get("username")
        email = data.pop("email", None)
        if username in users:
            # we've had an update for this user before
            previous_street = users[username].current_street
            if previous_street != data.get("street"):
                # the user must have switched streets

                if data.get("street") == "Louise Pasture":
                    # offer the race to the forest
                    if email in quest.quest_log_cache:
                        await quest.quest_log_cache[email].offer_quest("Q4")

                if "." in data.get("tsid", ""):
                    # we're on an instanced street
                    if email in quest.quest_log_cache:
                        await quest.quest_log_cache[email].offer_quest("Q11")

                MessageBus.publish(
                    "player_location_change",
                    RequirementProgress(f"location_{data['street']}", email),
                )

                data["changeStreet"] = data["street"]
                data["previousStreet"] = previous_street
                users[username].current_street = data["street"]

            data["letter"] = letters.get_player_letter(username)

            try:
                prev_x = users[username].current_x
                prev_y = users[username].current_y
                current_x = float(data["xy"].split(",")[0])
                current_y = float(data["xy"].split(",")[1])
                x_diff = abs(current_x - prev_x)
                y_diff = abs(current_y - prev_y)

                new_steps = abs(math.ceil((x_diff + y_diff) / 22))
                await StatManager.add(email, Stat.steps_taken, increment=new_steps, buffer=True)

                if y_diff > 17:
                    # TODO: detect this better
                    await StatManager.add(email, Stat.jumps, buffer=True)

                users[username].current_x = current_x
                users[username].current_y = current_y
                # limit the number of position messages that get broadcast
                # 1 message every 5gh at 30 messages per second would be 6 per second
                if (
                    message_post_counter.get(email) is None
                    or message_post_counter[email] > 5
                ):
                    MessageBus.publish(
                        "player_position",
                        PlayerPosition(data["street"], email, current_x, current_y),
                    )
                    message_post_counter[email] = 0
                else:
                    message_post_counter[email] += 1
            except Exception:
                logger.exception("Error processing player update")
        else:
            # this user must have just connected
            users[username] = Identifier(username, data["street"], data["tsid"], socket)
            data["letter"] = letters.get_player_letter(username)

            # update last login date
            async with db_session_context() as db_session:
                (await db_session.scalars(select(User).filter(User.username == username))).first().last_login = datetime.now()
            try:
                current_x = float(data["xy"].split(",")[0])
                current_y = float(data["xy"].split(",")[1])
                users[username].current_x = current_x
                users[username].current_y = current_y
            except Exception:
                logger.exception("Error processing player update")

        await send_all(data)

    except Exception:
        logger.exception("Error processing player message")


async def send_all(data):
    for username, identifier in users.items():
        if (
            data["street"] == identifier.current_street
            or data.get("changeStreet") is not None
        ):
            try:
                await identifier.socket.send(json.dumps(data))
            except Exception:
                pass
