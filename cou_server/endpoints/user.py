from quart import Blueprint, Response, jsonify, request
from quart_cors import cors

from werkzeug.exceptions import NotFound, BadRequest

from cou_server.common.database import db_session_context
from cou_server.endpoints import chat
from cou_server.models.user import User, UserSchema

users_blueprint = Blueprint("user_blueprint", __name__)
cors(users_blueprint)

# USERS


@users_blueprint.route("/searchUsers")
def serach_users():
    query = request.args.get("query", default="").lower()
    with db_session_context() as db_session:
        users = (
            db_session.query(User.username)
            .filter(User.username.ilike(f"%{query}%"))
            .all()
        )
        return jsonify([user.username for user in users])


@users_blueprint.route("/listUsers")
def list_users():
    users = []
    channel = request.args.get("channel").lower()
    for username, user_id in chat.users.items():
        if channel in user_id.channel_list:
            users.append(username)
    return jsonify(users)


# IP ADDRESSES

connection_history = []


@users_blueprint.route("/addresses")
def get_connection_history():
    return jsonify(connection_history)


# void addToConnectionHistory(InternetAddress address) {
# 	if (!connectionHistory.contains(address.address)) {
# 		connectionHistory.add(address.address);
# 	}
# }
