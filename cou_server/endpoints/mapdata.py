from importlib import resources
import json
from pathlib import Path
import urllib.request

from quart import Blueprint, jsonify, request, Response
from quart_cors import cors
from sqlalchemy.future import select

from cou_server.common import CACHE_DIR, get_logger
from cou_server.common.database import db_session_context

logger = get_logger(__name__)

CAT422_CACHE_DIR = Path(CACHE_DIR, "CAT422_streets")

if not CAT422_CACHE_DIR.is_dir():
    CAT422_CACHE_DIR.mkdir()

# Remote paths
CAT422_LOCATIONS = "https://raw.githubusercontent.com/ChildrenOfUr/CAT422-glitch-location-viewer/master/locations/"

map_blueprint = Blueprint("map_blueprint", __name__)
cors(map_blueprint)


async def read_map_data() -> None:
    global STREETS, HUBS, RENDER, STREET_IDS_TO_NAMES

    try:
        with resources.files("data.map").joinpath("streets.json").open() as streets_file:
            STREETS = json.load(streets_file)
            STREET_IDS_TO_NAMES = {}
            # Create ID->name lookup table
            for label, data in STREETS.items():
                if label and "tsid" in data and data["tsid"]:
                    STREET_IDS_TO_NAMES[tsid_l(data["tsid"])] = label

            logger.debug(
                f"[MapData] Created street lookup table with {len(STREET_IDS_TO_NAMES)} entries"
            )

        with resources.files("data.map").joinpath("hubs.json").open() as hubs_file:
            HUBS = json.load(hubs_file)

        with resources.files("data.map").joinpath("render.json").open() as render_file:
            RENDER = json.load(render_file)

        from cou_server.streets.instancing import InstanceStreet

        async with db_session_context() as db_session:
            for instance in (await db_session.scalars(select(InstanceStreet))):
                STREETS[instance.label] = {
                    "tsid": instance.tsid,
                    "hub_id": instance.hub_id,
                    "in_game": True,
                    "map_hidden": True,
                }
                STREET_IDS_TO_NAMES[tsid_l(instance.tsid)] = instance.label

        logger.debug("Loaded map data")
    except Exception as e:
        logger.error("Could not load map data: " + str(e))


def streets() -> dict:
    global STREETS
    return STREETS


def hubs() -> dict:
    global HUBS
    return HUBS


def get_streets_in_hub(hub_id: str) -> list[dict[str, dict]]:
    """List all streets in a hub"""
    streets = []
    for street in streets():
        if street.get("hub_id") == hub_id:
            streets.append(street)
    return streets


@map_blueprint.route("/getMapData")
async def get_map_data():
    return jsonify(
        {
            "streets": STREETS,
            "hubs": HUBS,
            "render": RENDER,
            "streets_by_id": STREET_IDS_TO_NAMES,
        }
    )


@map_blueprint.route("/listStreets")
async def list_streets():
    if request.args.get("all", default="0", type=str).lower() in ["", "true", "1"]:
        found_streets = STREETS.keys()
    else:
        found_streets = {
            label for label in STREETS.keys() if not street_is_hidden(label)
        }

    return Response("\n".join(found_streets), mimetype="text/plain")


@map_blueprint.route("/getStreet")
async def get_street_data():
    """Get street assets file from CAT422 repo."""
    tsid = request.args.get("tsid", default=str(), type=str)

    if not tsid:
        return Response(status=400)
    tsid = tsid_g(tsid)

    # Check cache
    try:
        return await get_and_cache_file(tsid)
    except Exception:
        return Response(status=404)


async def get_and_cache_file(tsid: str) -> dict:
    cache_file = Path(CAT422_CACHE_DIR, f"{tsid}.json")
    base_tsid = tsid.split(".")[0]

    # is this an instance street? check db
    if instance_json := await _fetch_from_db(tsid):
        return instance_json

    if not cache_file.is_file():
        # Check GitHub (base street tsid only)
        _fetch_from_github(base_tsid, cache_file)
    with open(cache_file, mode="r") as street_file:
        return json.loads(street_file.read())


async def _fetch_from_db(tsid: str) -> dict|None:
    from cou_server.streets.instancing import InstanceStreet

    async with db_session_context() as db_session:
        instance = (await db_session.scalars(select(InstanceStreet).filter(InstanceStreet.tsid == tsid))).one_or_none()
        if not instance:
            return None
        return json.loads(instance.json)


def _fetch_from_github(tsid: str, cache_file: Path):
    logger.debug(
            f"Street file '{tsid}.json' is not cached, so downloading from GitHub"
    )
    try:
        file = urllib.request.Request(f"{CAT422_LOCATIONS}/{tsid}.json")
        repo_file = urllib.request.urlopen(file).read().decode()

        # Add to cache
        with open(cache_file, mode="x") as new_file:
            new_file.write(repo_file)
    except urllib.request.HTTPError:
        logger.error(f"Could not find file for {tsid} in CAT422 repo")
        raise


def tsid_l(tsid: str) -> str:
    """Get a TSID in 'L...' (Tiny Speck) form."""
    if not len(tsid):
        raise ValueError("TSID is invalid")

    if tsid[0] == "G":
        # Currently in CAT422 form
        return "L" + tsid[1:]
    else:
        # Assume TS form
        return tsid


def tsid_g(tsid: str) -> str:
    """Get a TSID in 'G...' (CAT422) form."""
    if not len(tsid):
        raise ValueError("TSID is invalid")

    if tsid[0] == "L":
        # Currently in TS form
        return "G" + tsid[1:]
    else:
        # Assume CAT422 form
        return tsid


def get_street_by_tsid(tsid: str) -> dict:
    """Find a street map by tsid (either G or L form)."""
    if not tsid:
        raise ValueError("Cannot find street without a TSID")

    for label in STREETS:
        if tsid_l(STREETS[label]["tsid"]) == tsid_l(tsid):
            found = STREETS[label]
            found["label"] = label
            return found

    raise ValueError(f"No street found with tsid '{tsid}'")


def get_street_by_name(name: str) -> dict:
    return STREETS[name]


def street_is_hidden(label: str) -> bool:
    if not label:
        raise ValueError("Street name must be supplied for hidden checking")

    if label not in STREETS:
        raise ValueError(f"No street found with label '{label}'")

    # Check at street level
    street = STREETS[label]
    if street.get("map_hidden", False):
        return True

    # Check at hub level
    hub = HUBS[str(street["hub_id"])]
    return hub.get("map_hidden", False)


def is_savanna_street(label: str) -> bool:
    """Whether a street is in the savanna."""
    if not label:
        raise ValueError("Street name must be supplied for savanna detection")

    if label not in STREETS:
        raise ValueError(f"No street found with label '{label}'")

    try:
        street = STREETS[label]
        assert street

        hub = HUBS[str(street["hub_id"])]
        assert hub

        return hub.get("savanna", False)
    except KeyError:
        # Missing data
        return False


def savanna_escape_to(current_label: str) -> str:
    """Get the "nearest" non-savanna street."""
    if not current_label:
        raise ValueError("Street name must be supplied for savanna escape")

    if current_label not in STREETS:
        raise ValueError(f"No street found with label '{current_label}'")

    HUB_TO_TSID = {
        "86": "LIF18V95I972R96",  # Baqala to Tamila
        "90": "LIFF6BQE33H26JC",  # Choru to Vantalu
        "95": "LDO8NGHIFTQ21CQ",  # Xalanga to Folivoria
        "91": "LHF4QVGL7NI269C",  # Zhambu to Tahli
    }

    try:
        if not is_savanna_street(current_label):
            return STREETS[current_label]["tsid"]

        current_tsid = STREETS[current_label]["tsid"]
        assert current_tsid

        hub_id = str(get_street_by_tsid(current_tsid)["hub_id"])
        assert hub_id
        assert HUB_TO_TSID[hub_id]
        return HUB_TO_TSID[hub_id]
    except AssertionError or KeyError:
        # Missing data, go to Cebarkul
        return "LIF12PMQ5121D68"
