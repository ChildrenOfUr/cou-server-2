import asyncio
import json
from importlib import resources

from quart import Blueprint, jsonify, websocket
from quart_cors import cors
from sqlalchemy.future import select

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.message_bus import MessageBus
from cou_server.models.user import User


quest_blueprint = Blueprint("quest_blueprint", __name__)
cors(quest_blueprint)

logger = get_logger(__name__)
user_sockets: dict[str, websocket] = {}
quests = {}
quest_log_cache = {}


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for socket in user_sockets.values():
        await socket.close(1000, "Server Shutdown")


async def update_quest_log(quest_log):
    async with db_session_context() as db_session:
        quest_log = await db_session.merge(quest_log)


async def get_quest_log(email: str):
    from cou_server.quests.quest_log import QuestLog

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.email == email))).one_or_none()
        if not user:
            logger.error(f"No user found for <email={email}> so can't get quest log")
            raise ValueError(f"No user found for {email}")
        if (await user.awaitable_attrs.quest_log) is None:
            user.quest_log = QuestLog()
            await db_session.commit()
        db_session.expunge(user.quest_log)
        return user.quest_log


@quest_blueprint.route("/quest/requirementTypes")
async def requirementTypes():
    types = []
    events = []

    for quest_id, quest in quests.items():
        for requirement in quest.requirements:
            if requirement.type not in types:
                types.append(requirement.type)
            if requirement.event_type not in events:
                events.append(requirement.event_type)

    return {"types": types, "events": events}


@quest_blueprint.route("/quest/pieces")
async def get_pieces():
    pieces = {
        "getItem_<Item>": "Aquire Item",
        "makeRecipe_<Item>": "Make Recipe",
        "treePet<Tree>": "Pet Tree",
        "location_<Location>": "Goto Location",
        "sendMail_<Player>_containingItems_<List_Item>_currants_<int>": "Send Mail",
    }

    return jsonify(pieces)


async def cleanup_list(socket):
    leaving_user = None

    for email, ws in user_sockets.items():
        if ws == socket:
            ws = None
            leaving_user = email

    if leaving_user and leaving_user in quest_log_cache:
        await quest_log_cache[leaving_user].stop_tracking()
        quest_log_cache.pop(leaving_user, None)
        user_sockets.pop(leaving_user, None)


@quest_blueprint.websocket("/quest")
async def quests_ws():
    from cou_server.quests import messages
    from cou_server.quests.quest_log import QuestLog

    while True:
        try:
            data = json.loads(await websocket.receive())

            if data.get("connect", None):
                email = data["email"]

                # setup our associative data structures
                user_sockets[email] = websocket._get_current_object()

                quest_log_cache[email] = await get_quest_log(email)

                # start tracking this user's quest log
                await quest_log_cache[email].start_tracking(email)

            if data.get("acceptQuest", None):
                try:
                    MessageBus.publish(
                        "accept_quest", messages.AcceptQuest(data["id"], data["email"])
                    )
                except Exception:
                    logger.exception(
                        f"Accepting quest <id={data.get('id')}> for <email={data['email']}>"
                    )

            if data.get("rejectQuest", None):
                try:
                    MessageBus.publish(
                        "reject_quest", messages.RejectQuest(data["id"], data["email"])
                    )
                except Exception:
                    logger.exception(
                        f"Rejecting quest <id={data.get('id')}> for <email={data['email']}>"
                    )
        except asyncio.CancelledError:
            await cleanup_list(websocket._get_current_object())
            raise


def load_quests():
    from cou_server.quests.quest import Quest

    try:
        for quest in [
            q for q in resources.files("data.quests").iterdir() if str(q).endswith(".json")
        ]:
            with resources.files("data.quests").joinpath(quest).open() as quest_file:
                quest_data = json.loads(quest_file.read())
                quests[quest_data["id"]] = Quest.from_json(quest_data)

        logger.debug(f"Loaded {len(quests)} quests")
    except Exception:
        logger.exception(f"Could not load quest data for {quest}")
