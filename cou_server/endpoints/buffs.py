from quart import Blueprint, jsonify, request
from quart_cors import cors

from cou_server.buffs.buff_manager import BuffManager


buffs_endpoint = Blueprint("buffs_endpoint", __name__, url_prefix="/buffs")
cors(buffs_endpoint)


@buffs_endpoint.route("/get/<string:email>")
async def buffs_get(email):
    """API access to [get_player_buffs] by email"""
    return jsonify(await BuffManager.get_player_buffs(email=email))


@buffs_endpoint.route("/getByUsername/<string:username>")
async def buffs_get_by_username(username):
    """API access to [get_player_buffs] by username"""
    return jsonify(await BuffManager.get_player_buffs(username=username))
