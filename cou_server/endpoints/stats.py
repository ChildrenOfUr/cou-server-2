from quart import Blueprint, jsonify
from quart_cors import cors

from cou_server.achievements.stats import StatManager


stats_endpoint = Blueprint("stats_endpoint", __name__)
cors(stats_endpoint)


@stats_endpoint.route('/getGameStats')
def get_game_stats():
    return jsonify(StatManager.get_all_sums())
