from quart import Blueprint, jsonify, request, Response
from quart_cors import cors
import inflect
from sqlalchemy.future import select

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.endpoints.mapdata import tsid_l
from cou_server.models.entity import Entity

logger = get_logger(__name__)
inflector = inflect.engine()

entity_blueprint = Blueprint("entity_blueprint", __name__)
cors(entity_blueprint)


@entity_blueprint.route("/previewStreetEntities")
async def preview_street_entities():
    tsid = request.args.get("tsid", type=str)
    if not tsid:
        return Response("No tsid provided", status=400)

    tsid = tsid_l(tsid)
    num_entities = {}
    quoin_types = [
        "Img",
        "Mood",
        "Currant",
        "Mystery",
        "Favor",
        "Energy",
        "Time",
        "Quarazy",
    ]
    async with db_session_context() as db_session:
        entities = (await db_session.scalars(select(Entity).filter(Entity.tsid == tsid).filter(Entity.type.notin_(quoin_types)))).all()

        for entity in entities:
            # change a string like GardeningGoodsVendor to Gardening Goods Vendor
            new_type = "".join(
                map(lambda x: x if x.islower() else f" {x}", entity.type)
            ).strip()

            # if new_type.lower() in StreetSpiritGroddle.VARIANTS:
            # new_type = f'Shrine to {new_type}'

            if "Street Spirit" in new_type:
                new_type = "Street Spirit"

            if new_type in num_entities:
                num_entities[new_type] += 1
            else:
                num_entities[new_type] = 1

        pluralized_entities = {}
        for type, count in num_entities.items():
            if count > 1:
                pluralized_entities[inflector.plural(type)] = count
            else:
                pluralized_entities[type] = count

    return jsonify(pluralized_entities)
