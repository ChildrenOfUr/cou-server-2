import asyncio
from datetime import datetime, timezone
import math
import json
import random

from quart import Blueprint, jsonify, request, websocket
from quart_cors import cors
from sqlalchemy import or_, text
from sqlalchemy.future import select
from werkzeug.exceptions import NotFound, BadRequest

from cou_server.buffs.buff_manager import BuffManager
from cou_server.common import constants, get_logger
from cou_server.common.database import db_session_context, remote_engine
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import clamp
from cou_server.endpoints import chat, mapdata, player_update, quest
from cou_server.entities.plants.respawning_items.hellgrapes import HellGrapes
from cou_server.models.metabolics import Metabolics, MetabolicsSchema, img_levels
from cou_server.models.user import User


metabolics_endpoint = Blueprint("metabolics_endpoint", __name__)
cors(metabolics_endpoint)


logger = get_logger(__name__)
user_sockets: dict[str, websocket] = {}
GLOBAL_QUOIN_MULTIPLIER = 1


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for socket in user_sockets.values():
        await socket.close(1000, "Server Shutdown")


@metabolics_endpoint.websocket("/metabolics")
async def metabolics_ws():
    while True:
        try:
            data = json.loads(await websocket.receive())
            username = data.get("username")
            if username not in user_sockets:
                user_sockets[username] = websocket._get_current_object()
        except asyncio.CancelledError:
            # cleanup closed socket
            leaving_user = None
            for username, user_socket in user_sockets.items():
                if websocket._get_current_object() == user_socket:
                    user_socket = None
                    leaving_user = username
            user_sockets.pop(leaving_user, None)
            raise


last_simulation = datetime.now(timezone.utc)
simulate_energy = False
simulate_mood = False


async def simulate_timer():
    logger.debug("Simulating metabolics forever")
    while True:
        await simulate()
        await asyncio.sleep(5)


async def energy_timer():
    global simulate_energy
    while True:
        simulate_energy = True
        await asyncio.sleep(90)


async def mood_timer():
    global simulate_mood
    while True:
        simulate_mood = True
        await asyncio.sleep(60)


async def simulate():
    global last_simulation
    for username, socket in user_sockets.copy().items():
        try:
            async with db_session_context() as db_session:
                user = (await db_session.scalars(select(User).filter(User.username == username))).first()
                m = await user.awaitable_attrs.metabolics
                email = user.email

                # effects from smashed/hungover buffs
                SMASHED_RATE = 3  # - energy, + mood
                SMASHED_TIME = 30  # seconds

                HUNGOVER_RATE = 17  # - mood
                HUNGOVER_TIME = 5  # seconds

                is_smashed = await BuffManager.player_has_buff("smashed", email)
                is_hungover = await BuffManager.player_has_buff("hungover", email)
                if (
                    abs(datetime.now(timezone.utc) - last_simulation).seconds >= SMASHED_TIME
                    and is_smashed
                ):
                    # smashed converts energy into mood
                    # energy will never go below HOOCH_RATE with this buff
                    m.energy = clamp(
                        m.energy - SMASHED_RATE, SMASHED_RATE, m.max_energy
                    )
                    m.mood += SMASHED_RATE
                elif (
                    abs(datetime.now(timezone.utc) - last_simulation).seconds >= HUNGOVER_TIME
                    and is_hungover
                ):
                    # hungover is a mood killer
                    m.mood -= HUNGOVER_RATE

                if simulate_mood:
                    _calc_and_set_mood(m)

                if m.mood < m.max_mood // 10 and email in quest.quest_log_cache:
                    await quest.quest_log_cache[email].offer_quest("Q10")

                if simulate_energy:
                    _calc_and_set_energy(m)

                user_identifier = player_update.users.get(username)
                if user_identifier:
                    await update_death(user_identifier)

                # store current street and position
                if user_identifier:
                    m.current_street = user_identifier.tsid
                    m.current_street_x = user_identifier.current_x
                    m.current_street_y = user_identifier.current_y
                    # send the metabolics back to the user
                    await socket.send(MetabolicsSchema().dumps(m))
        except Exception:
            logger.exception("Metabolics simulation failed")

        last_simulation = datetime.now(timezone.utc)


def _calc_and_set_mood(m):
    global simulate_mood
    max_mood = m.max_mood
    mood_ratio = m.mood / max_mood

    # determine how much mood they should lose based on current percentage of max
    # https://web.archive.org/web/20130106191352/http://www.glitch-strategy.com/wiki/Mood
    if mood_ratio < 0.5:
        m.mood -= math.ceil(max_mood * 0.005)
    elif 0.81 < mood_ratio <= 0.5:
        m.mood -= math.ceil(max_mood * 0.01)
    else:
        m.mood -= math.ceil(max_mood * 0.015)

    # keep between 0 and max (both inclusive)
    m.mood = clamp(m.mood, 0, m.max_mood)
    simulate_mood = False


def _calc_and_set_energy(m):
    global simulate_energy
    # players lose .8% of their max energy every 90 seconds
    # https://web.archive.org/web/20120805062536/http://www.glitch-strategy.com/wiki/Energy
    m.energy -= math.ceil(m.max_energy * 0.008)

    # keep between 0 and max (both inclusive)
    m.energy = clamp(m.energy, 0, m.max_energy)
    simulate_energy = False


@metabolics_endpoint.route("/getMetabolics")
async def get_metabolics(username=None, email=None, user_id=None, case_sensitive=False):
    if request:
        username = request.args.get("username")
        email = request.args.get("email")
        user_id = request.args.get("user_id")
        case_sensitive = request.args.get("case_sensitive", False)

    async with db_session_context() as db_session:
        query = select(Metabolics).join(Metabolics.user)
        if email:
            query = query.filter(User.email == email)
        elif username:
            if not case_sensitive:
                query = query.filter(User.username.ilike(username))
            else:
                query = query.filter(User.username.like(username))
        elif user_id:
            query = query.filter(User.id == user_id)

        metabolic = (await db_session.scalars(query)).first()
        if not metabolic:
            metabolic = Metabolics()
            db_session.add(metabolic)
            user = (
                (await db_session.scalars(
                    select(User)
                    .filter(
                        or_(
                            User.username == username,
                            User.email == email,
                            User.id == user_id,
                        )
                    )
                ))
                .first()
            )
            if not user:
                raise BadRequest(
                    f"Could not find user with username={username}, email={email}, user_id={user_id}."
                )
            metabolic.user = user
        return jsonify(MetabolicsSchema().dump(metabolic))


@metabolics_endpoint.route("/getLevel")
async def get_level():
    img = request.args.get("img", type=int)

    if img >= img_levels[60]:
        result = 60
    else:
        for level in img_levels.keys():
            level_img = img_levels[level]
            if img < level_img:
                result = level - 1
                break

    return str(result)


@metabolics_endpoint.route("/getImgForLevel")
async def get_img_for_level():
    lvl = request.args.get("level", type=int)

    if lvl > 0 and lvl <= 60:
        return str(img_levels[lvl])
    else:
        return str(-1)


async def update_death(user_identifier, init=False):
    HELL_ONE = "GA5PPFP86NF2FOS"
    CEBARKUL = "GIF12PMQ5121D68"
    NARAKA = 40
    if not user_identifier:
        return

    async with db_session_context() as db_session:
        m = (
            (await db_session.scalars(select(User)
            .filter(User.username == user_identifier.username)))
            .first()
        )
        m = await m.awaitable_attrs.metabolics
        if m.energy == 0 and (m.undead_street is None or init):
            m.dead = True

            # go to hell one
            await user_identifier.socket.send(
                json.dumps({"gotoStreet": "true", "tsid": HELL_ONE})
            )
        elif m.energy >= HellGrapes.ENERGY_REQ:
            # enough energy to be alive

            street = mapdata.get_street_by_tsid(m.current_street)
            if street is None or street.get("hub_id", NARAKA) == NARAKA:
                # in Naraka, return to world
                await user_identifier.socket.send(
                    json.dumps(
                        {
                            "gotoStreet": True,
                            "tsid": m.undead_street if m.undead_street else CEBARKUL,
                        }
                    )
                )

                m.dead = False

def track_new_days() -> None:
    # Refill everyone's energy on the start of a new day
    async def refill_energy():
        async with remote_engine.connect() as conn:
            update = text("UPDATE metabolics SET energy = max_energy, quoins_collected = 0")
            result = await conn.execute(update)
            logger.debug(f"Updated {result.rowcount} users' metabolics due to new day event")
    MessageBus.subscribe("clock_on_new_day", refill_energy)

    logger.debug("[Init] Tracking new days")


async def add_quoin(quoin, username):
    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.username == username))).first()
        m = await user.awaitable_attrs.metabolics

        if m.quoins_collected >= constants.QUOIN_LIMIT:
            # Daily quoin limit
            await deny_quoin(quoin, username)
            return

        amt = 0
        if quoin.type == "mystery":
            # choose a number 0.01 i to 0.09 i
            amt = random.randint(1, 9) / 100
            # add it to the player's quoin multiplier
            m.quoin_multiplier += amt

            # limit QM
            if m.quoin_multiplier > constants.QUOIN_MULTIPLIER_LIMIT:
                m.quoin_multiplier = constants.QUOIN_MULTIPLIER_LIMIT
                # add double quoins buff instead
                await BuffManager.add_to_user("double_quoins", user.email, user_sockets[user.email])
        else:
            # choose a number 1-5
            amt = random.randint(1, 5)
            # multiply it by the player's quoin multiplier
            amt = round(amt * m.quoin_multiplier)
            amt = round(amt * GLOBAL_QUOIN_MULTIPLIER)
            # double if buffed
            if await BuffManager.player_has_buff("double_quoins", user.email):
                amt *= 2

        if quoin.type == "quarazy":
            amt *= 7

        if quoin.type == "currant":
            m.currants += amt

        if quoin.type in ["img", "quarazy"]:
            m.img += amt
            m.lifetime_img += amt

        if quoin.type == "mood":
            if m.mood + amt > m.max_mood:
                amt = m.max_mood - m.mood
            m.mood += amt

        if quoin.type == "energy":
            if m.energy + amt > m.max_energy:
                amt = m.max_energy - m.energy
            m.energy += amt

        if quoin.type == "favor":
            for giant in [
                "alph",
                "cosma",
                "friendly",
                "grendaline",
                "humbaba",
                "lem",
                "mab",
                "pot",
                "spriggan",
                "tii",
                "zille",
            ]:
                giant_favor = getattr(m, f"{giant}favor")
                giant_favor_max = getattr(m, f"{giant}favor_max")
                giant_favor += amt
                giant_favor = clamp(giant_favor, 0, giant_favor_max - 1)

        if quoin.type == "time":
            if await BuffManager.player_has_buff("nostalgia", user.email):
                buff = await BuffManager.buffs["nostalgia"].get_for_player(user.email)
                await buff.extend(datetime.timedelta(seconds=amt))

        if amt > 0:
            m.quoins_collected += 1

        try:
            map = {
                "collectQuoin": "true",
                "id": quoin.id,
                "amt": amt,
                "quoinType": quoin.type,
            }
            await quoin.set_collected(username)
            await user_sockets[username].send(json.dumps(map))  # send quoin
            await user_sockets[username].send(MetabolicsSchema().dumps(m))  # send metabolics
        except Exception:
            logger.exception(
                f"Could not set metabolics {m} for player {username} adding quoin"
            )


async def deny_quoin(quoin, username):
    map = {"collectQuoin": "true", "success": "false", "id": quoin.id}
    try:
        await user_sockets[username].send(json.dumps(map))
        return True
    except Exception:
        logger.exception(f"Could not pass map {map} to player {username} denying quoin")
        return False
