from datetime import datetime, timedelta, timezone

from quart import Blueprint, jsonify
from quart_cors import cors
import psutil

from cou_server.endpoints import chat, street_update


status_blueprint = Blueprint("status_blueprint", __name__)
cors(status_blueprint)


class ServerStatus:
    server_start: datetime

    @classmethod
    def online_players(cls) -> list[str]:
        """List online players"""

        return list(set(player for player in chat.users))

    @classmethod
    def streets_loaded(cls) -> list[dict[str, str]]:
        """List streets in memory"""

        return [{"label": street.label, "tsid": street.tsid} for street in street_update.streets.values()]

    @classmethod
    def bytes_used(cls) -> int:
        process = psutil.Process()
        return process.memory_info().rss

    @classmethod
    def cpu_used(cls) -> float:
        return psutil.cpu_percent()

    @classmethod
    def uptime(cls) -> timedelta:
        return datetime.now(timezone.utc) - cls.server_start


@status_blueprint.route("/serverStatus")
def server_status():
    uptime = ServerStatus.uptime()
    seconds = int(uptime.total_seconds())
    hours = int(seconds // 3600)
    minutes = str(int(seconds // 60) - (hours * 60)).zfill(2)
    seconds = str(seconds % 60).zfill(2)

    return jsonify({
        "numPlayers": len(ServerStatus.online_players()),
        "playerList": ServerStatus.online_players(),
        "numStreetsLoaded": len(ServerStatus.streets_loaded()),
        "streetsLoaded": ServerStatus.streets_loaded(),
        "bytesUsed": f"{ServerStatus.bytes_used():,}",
        "cpuUsed": ServerStatus.cpu_used(),
        "uptime": f"{hours}:{minutes}:{seconds}",
    })
