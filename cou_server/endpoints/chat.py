import asyncio
import json
import io

from PIL import Image
import aiohttp
import discord
from quart import Blueprint, websocket

from api_keys import DISCORD_GLOBAL_CHAT_WEBHOOK, DISCORD_TOKEN, GLITCH_FOREVER_WEBHOOK
from cou_server import MIN_CLIENT_VER
from cou_server.common import get_logger
from cou_server.common.identifier import Identifier
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import toast
from cou_server.endpoints import images


logger = get_logger(__name__)
chat_blueprint = Blueprint("chat_blueprint", __name__)

users: dict[str, Identifier] = {}


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for user in users.values():
        await user.socket.close(1000, "Server Shutdown")


class ChatEvent:
    def __init__(
        self, username=None, message=None, channel=None, street_name=None, **kwargs
    ):
        self.username = username
        self.message = message
        self.channel = channel
        self.street_name = street_name


@chat_blueprint.websocket("/chat")
async def chat_ws():
    while True:
        try:
            data = json.loads(await websocket.receive())

            if data.get("exempt", None) is None:
                channel = data.get("channel", "")
                status_message = data.get("statusMessage", None)
                username = data.get("username", None)
                message = data.get("message", None)
                muted = False  # TODO
                if (
                    channel == "Global Chat"
                    and not status_message
                    and username
                    and message
                    and not muted
                ):
                    asyncio.create_task(slack_send(username, message))
                asyncio.create_task(processMessage(websocket._get_current_object(), data))
        except asyncio.CancelledError:
            await cleanup_lists(websocket._get_current_object())
            raise


async def processMessage(socket, message):
    data = message
    client_version = data.get("clientVersion", None)
    exempt = data.get("exempt", False)
    channel = data.get("channel", "")
    username = data.get("username", "")
    muted = False  # TODO
    status_message = data.get("statusMessage", "")
    street = data.get("street", "")
    tsid = data.get("tsid", "")
    new_street_label = data.get("newStreetLabel", "")
    old_street_label = data.get("oldStreetLabel", "")
    old_street_tsid = data.get("oldStreetTsid", "")
    new_street_tsid = data.get("newStreetTsid", "")
    new_street = data.get("newStreet", "")

    if client_version is not None and client_version < MIN_CLIENT_VER:
        await socket.send(
            json.dumps({"error": "Your client is outdated. Please reload the page."})
        )
        return

    if not exempt and channel == "Global Chat" and muted:
        await socket.send(
            json.dumps(
                {
                    "muted": "true",
                    "toastText": (
                        "You may not use Global Chat because you are a nuisance to Ur."
                        " Please click here to email us if you believe this is an error."
                    ),
                    "toastClick": "__EMAIL_COU__",
                }
            )
        )
        return

    if status_message == "join":
        data["statusMessage"] = "true"
        data["message"] = " joined."
        users[username] = Identifier(username, street, tsid, socket)
        users[username].channel_list.extend([street, "Global Chat"])
    elif status_message == "changeStreet":
        already_sent = []
        for user_name, user_id in users.items():
            if user_name == username:
                user_id.current_street = new_street_label
                if old_street_label in user_id.channel_list:
                    user_id.channel_list.remove(old_street_label)
                user_id.channel_list.append(new_street_label)
            if (
                user_id.username not in already_sent
                and user_id.username != username
                and user_id.current_street == old_street_tsid
            ):
                left_for_message = {
                    "statusMessage": "leftStreet",
                    "username": username,
                    "streetName": new_street_label,
                    "tsid": new_street_tsid,
                    "message": " has left for ",
                    "channel": "Local Chat",
                }
                if users[user_id.username]:
                    await users[user_id.username].socket.send(
                        json.dumps(left_for_message)
                    )
                already_sent.append(user_id.username)
            if user_id.current_street == new_street and user_id.username != username:
                toast(f"{username} is here!", user_id.socket)
        return
    elif status_message == "list":
        user_list = []
        for user_name, user_id in users.items():
            if channel == "Local Chat" and user_id.current_street == street:
                user_list.append(user_id.username)
            elif channel != "Local Chat":
                user_list.append(user_id.username)
        data["users"] = user_list
        data["message"] = "Users in this channel: "
        await users[username].socket.send(json.dumps(data))
        return

    MessageBus.publish("chat_event", ChatEvent(**data))
    await send_all(json.dumps(data))


async def slack_send(username, message):
    try:
        url_username = username.replace(" ", "_")
        icon_url = f"https://childrenofur.com/data/heads/{url_username}.head.png"
        async with aiohttp.ClientSession() as session:
            async with session.get(icon_url) as response:
                if response.status == 200:
                    await _send_message(message, username, icon_url)
                    return

                # if the head picture doesn't already exist, try to make one
                spritesheets = await images._get_spritesheets_from_web(username)
                if "base" in spritesheets:
                    async with session.get(spritesheets["base"]) as response:
                        img_file = io.BytesIO(await response.read())
                        image = Image.open(img_file).convert("RGBA")
                        frame_width = int(image.width / 15)
                        frame_height = int(image.height * .6)
                        x_start = frame_width - frame_height if frame_width > frame_height else 0
                        image = image.crop((x_start, 0, frame_width, frame_height))
                        png_bytes = io.BytesIO()
                        image.save(png_bytes, format="PNG")
                        bytes = png_bytes.getvalue()

                        form_data = aiohttp.FormData()
                        form_data.add_field("file", bytes, content_type="image/png", filename=f"{url_username}.head.png")
                        async with session.post("https://childrenofur.com/data/heads/uploadhead.php", data=form_data) as response:
                            icon_url = f"https://childrenofur.com/data/heads/{url_username}.head.png"
                            await _send_message(message, username, icon_url)
                else:
                    # If the username isn't found, just use the cupcake
                    icon_url = "http://s21.postimg.org/czibb690j/head.png"
                    await _send_message(message, username, icon_url)
    except Exception:
        logger.exception("Error sending slack message")


async def _send_message(text: str, username: str, icon_url: str) -> None:
    # Send to our Discord server
    async with aiohttp.ClientSession() as session:
        body = {
            "content": text,
            "username": username,
            "avatar_url": icon_url,
            "flags": 4,  # SUPPRESS_EMBEDS
        }
        async with session.post(DISCORD_GLOBAL_CHAT_WEBHOOK, json=body) as response:
            if response.status >= 400:
                logger.error(f"Error sending chat message to Discord: {response.status}: {await response.text()}")

        body = {
            "text": text,
            "username": username,
            "icon_url": icon_url,
        }
        # Send to the Glitch Forever Slack workspace
        async with session.post(GLITCH_FOREVER_WEBHOOK, json=body) as response:
            if response.status >= 400:
                logger.error(f"Error sending chat message to Slack: {response.status}: {await response.text()}")


class DiscordListener(discord.Client):
    def __init__(self, *args, **kwargs):
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(*args, intents=intents, **kwargs)

    async def on_ready(self):
        logger.info(f"Logged on as {self.user}")

    async def on_message(self, message: discord.message.Message):
        if message.channel.name != "global-chat":
            return
        await send_all(json.dumps({
            "username": message.author.name,
            "message": message.content,
            "channel": "Global Chat",
        }))


async def listen_to_discord():
    await DiscordListener().start(DISCORD_TOKEN)


async def cleanup_lists(socket, reason="No reason given"):
    leaving_user = None
    for user_name, user_id in users.items():
        if user_id.socket == socket:
            user_id.socket = None
            leaving_user = user_name
    users.pop(leaving_user, None)

    # send a message to all other clients that this user has disconnected
    data = {"message": " left.", "channel": "Local Chat", "username": leaving_user}
    await send_all(json.dumps(data))


async def send_all(message):
    for user_name, user_id in users.items():
        await user_id.socket.send(message)
