from urllib.parse import quote_plus

from quart import Blueprint
from quart_cors import cors
from sqlalchemy.future import select
from werkzeug.exceptions import BadRequest, NotFound

from cou_server.common.database import db_session_context
from cou_server.models.user import User


usernamecolors_blueprint = Blueprint(
    "usernamecolors_blueprint", __name__, url_prefix="/usernamecolors"
)
cors(usernamecolors_blueprint)


# Username colors are always represented as 7-character strings.
# They must include the #, followed by 6 digits in base 16.

# A value of # without any following digits indicates an unset value,
# and the algorithm for generating colors based on username character
# codes should be used instead.


# Returns the color in form #FFFFFF for a user with the provided username.
@usernamecolors_blueprint.route("/get/<string:username>")
async def get_color(username):
    # Handle URL encodings (eg. " " is "%20" in the string)
    username = quote_plus(username)

    # Default value
    hex_value = "#"

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.username == username))).first()
        if user:
            if user.username_color.strip() != "#":
                hex_value = user.username_color
            else:
                hex_value = await get_by_chars(username)
        else:
            hex_value = await get_by_chars(username)

    return hex_value.strip().upper()


@usernamecolors_blueprint.route("/set/<string:email>/<color_hex>")
async def set_color(email, color_hex):
    if not color_hex.strip().startswith("0x") or len(color_hex.trim()) != 8:
        raise BadRequest("Color input must be of the form 0xFFFFFF")

    nekkid_hex = color_hex.replace("0x", "", 1).upper()
    try:
        int(nekkid_hex, base=16)
    except Exception:
        raise BadRequest(f"Cannot use invalid hex {nekkid_hex} as username color.")

    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.email == email))).first()
        if not user:
            raise NotFound(f"No user found for email {email}.")
        user.username_color = nekkid_hex


@usernamecolors_blueprint.route("/getbychars/<string:username>")
async def get_by_chars(username):
    colors = [
        "blue",
        "deepskyblue",
        "fuchsia",
        "gray",
        "green",
        "olivedrab",
        "maroon",
        "navy",
        "olive",
        "orange",
        "purple",
        "red",
        "teal",
    ]

    return colors[abs(hash(username)) % (len(colors) - 1)]
