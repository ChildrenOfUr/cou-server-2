from importlib import resources
import json
import re

from quart import Blueprint, jsonify, request
from quart_cors import cors

from cou_server.common import get_logger
from cou_server.common.global_gobbler import cou_globals
from cou_server.entities.items.item import Item


items_endpoint = Blueprint("items_endpoint", __name__)
cors(items_endpoint)

logger = get_logger(__name__)
cou_globals.items = {}


@items_endpoint.route("/getItems")
def items_get():
    category = request.args.get("category", default=None)
    name = request.args.get("name", default=None)
    type = request.args.get("type", default=None)
    is_regex = request.args.get("isRegex", default="false") == "true"

    item_list: list[Item] = []

    if category:
        if is_regex:
            item_list.extend([i for i in cou_globals.items.values() if re.match(category, i.category, re.IGNORECASE)])
        else:
            item_list.extend([i for i in cou_globals.items.values() if i.category.lower() == category.lower()])

    if name:
        if is_regex:
            item_list.extend([i for i in cou_globals.items.values() if re.match(name, i.name, re.IGNORECASE)])
        else:
            item_list.extend([i for i in cou_globals.items.values() if i.name.lower() == name.lower()])

    if type:
        if is_regex:
            item_list.extend([i for i in cou_globals.items.values() if re.match(type, i.itemType, re.IGNORECASE)])
        else:
            item_list.extend([i for i in cou_globals.items.values() if i.itemType.lower() == type.lower()])

    if name is None and category is None and type is None:
        return jsonify([i.to_json() for i in cou_globals.items.values()])

    return jsonify([i.to_json() for i in item_list])


@items_endpoint.route("/getItemByName")
def get_item_by_name():
    try:
        name = request.args["name"]
        return jsonify(cou_globals.items[name].to_json())
    except Exception:
        return jsonify({"status": "FAIL", "reason": f"Could not find item: {name}"})


def load_items():
    for item_file in [
        i for i in resources.files("data.items").iterdir() if str(i).endswith(".json")
    ]:
        with resources.files("data.items").joinpath(item_file).open() as item_file:
            for name, item_map in json.loads(item_file.read()).items():
                item_map["itemType"] = name
                cou_globals.items[name] = Item.from_json(item_map)
    logger.debug(f"[Item] loaded {len(cou_globals.items)} items")
    return len(cou_globals.items)


def load_consume_values():
    """Load consume values from JSON file"""

    with resources.files("data.items.actions").joinpath("consume.json").open() as consume_file:
        total = 0
        for item_type, award in json.loads(consume_file.read()).items():
            try:
                cou_globals.items[item_type].consumeValues = award
                total += 1
            except Exception:
                logger.exception(f"Error setting consume values for {item_type} to {award}")

    logger.debug(f"[Item] Loaded {total} consume values")
    return total
