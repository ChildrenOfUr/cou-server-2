import asyncio
import json
import math

from quart import Blueprint, jsonify, request, websocket
from quart_cors import cors
from sqlalchemy.future import select

from cou_server import MIN_CLIENT_VER
from cou_server.buffs.buff_manager import BuffManager
from cou_server.common import get_logger, timed
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.database import db_session_context
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import prompt_callbacks, wait_for
from cou_server.entities.action import Action
from cou_server.endpoints.inventory import InventoryManager
from cou_server.entities.items.actions.note import NoteManager
from cou_server.entities.npcs.npc import NPC
from cou_server.endpoints.metabolics import update_death, add_quoin, deny_quoin
from cou_server.endpoints.player_update import users
from cou_server.metabolics.metabolics_change import MetabolicsChange
from cou_server.models.user import User
from cou_server.streets.street import Street
from cou_server.streets.instancing import InstanceStreet


logger = get_logger(__name__)
street_blueprint = Blueprint("street_blueprint", __name__)
cors(street_blueprint)

streets: dict[str, Street] = {}
cou_globals.streets = streets
user_sockets: dict[str, websocket] = {}
cou_globals.user_sockets = user_sockets

_pending_npcs: dict[str, dict[str, NPC]] = {}
_pending_npcs_remove: list[str] = []


@MessageBus.subscribe("server_shutdown")
async def server_shutdown(*args):
    for socket in user_sockets.values():
        await socket.close(1000, "Server Shutdown")


async def simulate_streets():
    """Run the simulation loop forever"""

    logger.debug("Simulating all loaded streets")

    @timed
    async def sim_loop():
        to_remove = []
        for street_name, street in streets.copy().items():
            await street.simulate()

    while True:
        await sim_loop()
        await asyncio.sleep(1)


async def update_npcs():
    """Simulate all of the NPCs (movement, etc)"""

    logger.debug("Simulating all loaded NPCs")

    async def sim_loop():
        global _pending_npcs_remove

        for street_name, street in streets.copy().items():
            if not street.occupants:
                continue
            move_map = {}
            move_map["npcMove"] = "true"
            move_map["npcs"] = []
            # even if they're no longer on the street, send them to the client
            # so they can remove if needed
            move_map["removeNpcs"] = _pending_npcs_remove[:]

            # add queued NPCs
            if _pending_npcs.get(street_name):
                street.npcs.extend(_pending_npcs[street_name])
                _pending_npcs[street_name].clear()

            # remove queued NPCs
            for id in [id for id in _pending_npcs_remove if id in street.npcs]:
                street.npcs.pop(id, None)
            _pending_npcs_remove = []

            for npc in street.npcs.values():
                npc.update()

            for username, socket in street.occupants.copy().items():
                if socket is None:
                    continue

                try:
                    move_map["npcs"] = []

                    for npc in street.npcs.values():
                        if npc.has_moved:
                            move_map["npcs"].append(npc.get_map(username))
                    await socket.send(json.dumps(move_map))
                except Exception:
                    logger.exception(f"Error sending move_map {move_map} to {username}")

    while True:
        await sim_loop()
        await asyncio.sleep(1 / NPC.UPDATE_FPS)


@street_blueprint.websocket("/streetUpdate")
async def chat_ws():
    while True:
        data = json.loads(await websocket.receive())
        asyncio.create_task(processMessage(websocket._get_current_object(), data))


async def load_street(street_name: str, tsid: str) -> None:
    street_name = street_name.strip()
    try:
        async with db_session_context() as db_session:
            street = (await db_session.scalars(select(Street).filter(Street.tsid == tsid))).first()
            if street is None:
                # maybe an instance
                street = (await db_session.scalars(select(InstanceStreet).filter(InstanceStreet.tsid == tsid))).first()
            streets[street_name] = street
            await street.load_task()
    except Exception:
        logger.exception(f"Could not load street {tsid}")


async def processMessage(socket, message):
    # we should receive 3 kinds of messages:
    # layer enters street, player exits street, player interacts with object
    # everything else will be outgoing
    try:
        data = message
        if "clientVersion" in data and data["clientVersion"] < MIN_CLIENT_VER:
            socket.send(json.dumps({"error": "version too low"}))
            return

        # chat bubbles: button clicked, call callback
        if data.get("bubbleButton") is not None:
            try:
                button_id = int(data["bubbleButton"])
                NPC.pending_bubble_callbacks[button_id]()
            except Exception:
                logger.exception(
                    f"Could not call callback for chat bubble button <id={data['bubbleButton']}>"
                )
            return

        # string prompt window: send reference and response to callback
        if data.get("promptRef") is not None:
            try:
                ref = data["promptRef"]
                response = data.get("promptResponse", "")
                prompt_callbacks[ref](ref, response)
            except Exception:
                logger.exception(
                    f"Could not call callback for string prompt <ref={data['promptRef']}"
                )
            return

        street_name = data.get("streetName", "").strip()
        username = data.get("username", "").strip()
        email = data.get("email", "").strip()
        if not email:
            async with db_session_context() as db_session:
                results = await db_session.scalars(select(User).filter(User.username == username))
                email = results.first().email

        # a player has joined or left the street
        if data.get("message") == "joined":
            # if the street doesn't yet exist, create it (maybe it got stored back to the datastore)
            if not street_name in streets:
                await load_street(street_name, data["tsid"])
            # set this player as being on this street
            if username in users:
                users[username].tsid = data["tsid"]

            user_sockets[email] = socket
            try:
                streets[street_name].occupants[username] = socket
            except Exception:
                logger.warning(
                    f"Adding {username} to {street_name}. Waiting to retry..."
                )
                try:
                    def wait_for_load(street_name) -> bool:
                        if street_name not in streets:
                            return False
                        return getattr(streets[street_name], "loaded", False)
                    await wait_for(lambda: wait_for_load(street_name), timeout=10)
                    streets[street_name].occupants[username] = socket
                except Exception:
                    logger.exception(
                        f"Giving up on adding {username} to {street_name}."
                    )

            async with db_session_context() as db_session:
                user = (await db_session.scalars(select(User).filter(User.username == username))).first()
                m = await user.awaitable_attrs.metabolics
                m.location_history.append(data["tsid"])

            if data["firstConnect"]:
                await wait_for(lambda: username in users)
                await InventoryManager.fire_inventory_at_user(socket, email)
                await update_death(users.get(username), init=True)
                await BuffManager.start_updating_user(email)

            # These will automatically disregard false calls, so call both every time
            # WintryPlaceHandler.enter(street_name, email, socket)
            # SavannaHandler.enter(street_name, email, socket)
            return
        elif data.get("message") == "left":
            getattr(streets.get(street_name), "occupants", {}).pop(username, None)
            if not getattr(streets.get(street_name), "occupants", {}):
                streets.pop(street_name, None)

            # stop updating their buffs
            await BuffManager.stop_updating_user(await User.get_email_from_username(data["username"]))

            # These will automatically disregard false calls, so call both every time
            # WintryPlaceHandler.enter(street_name, email, socket)
            # SavannaHandler.enter(street_name, email, socket)
            return

        if data.get("remove") is not None:
            if data["type"] == "quoin":
                touched = streets[street_name].quoins[data["remove"]]
                player = users[username]

                if player is None:
                    logger.warning(f"Could not find player {username} to collect quoin")
                elif touched is not None and not touched.collected:
                    xDiff = abs(touched.x - player.current_x)
                    yDiff = abs(touched.y - player.current_y)
                    diff = math.sqrt(pow(xDiff, 2) + pow(yDiff, 2))

                    if diff < 500:
                        await add_quoin(touched, username)
                    else:
                        await deny_quoin(touched, username)
                        logger.debug(f"Denied quoin to {username}: too far away {diff}")
                elif touched is None:
                    logger.warning(
                        f"Could not collect quoin {data['remove']} for player {username}: quoin not found"
                    )

            return

        if data.get("callMethod") is not None:
            if data.get("id") == "global_action_monster":
                await _call_global_method(data, socket, email)
                return
            else:
                entity_type = data["type"].replace("entity", "").strip()
                entity_type = entity_type.replace("groundItemGlow", "").strip()
                entity_map = streets[street_name].entity_maps.get(entity_type)
                method_name = normalize_method_name(data["callMethod"])
                arguments = {"socket": socket, "email": email}

                if entity_map is not None and entity_map.get(data.get("id")) is not None:
                    try:
                        entity = entity_map[data["id"]]
                        if entity_type == "groundItem":
                            for action in entity.actions:
                                if (
                                    normalize_method_name(action.actionName)
                                    == method_name
                                    and action.groundAction
                                ):
                                    data["arguments"]["map"] = {
                                        "id": data["id"],
                                        "streetName": data["streetName"],
                                    }

                        if data["arguments"] is not None:
                            arguments.update(data["arguments"])
                        await getattr(entity, method_name)(**arguments)
                    except Exception:
                        logger.exception(
                            f"Could not invoke entity method {method_name}"
                        )
                else:
                    # check if it's an item and not an entity
                    try:
                        entity = cou_globals.items[entity_type]
                        arguments["username"] = data["username"]
                        arguments["street_name"] = data["streetName"]
                        arguments["map"] = data["arguments"]
                        await getattr(entity, method_name)(**arguments)
                    except Exception:
                        logger.exception(f"Could not invoke item method {method_name}")
                return
    except Exception:
        logger.exception("Error processing street update")


async def _call_global_method(map, socket, email):
    arguments = {"userSocket": socket, "email": email}
    if map.get("callMethod") == "pickup" and "streetName" in map:
        arguments["streetName"] = map["streetName"]
    if map.get("arguments"):
        arguments.update(map["arguments"])
    await globals()[map["callMethod"]](**arguments)


async def teleport(userSocket = None, email = None, tsid = None, energyFree = False) -> bool:
    async with db_session_context() as db_session:
        user = (await db_session.scalars(select(User).filter(User.email == email))).first()
        m = await user.awaitable_attrs.metabolics
        if not energyFree:
            if user.id == -1 or m.energy < 50:
                return False
            if not await MetabolicsChange().try_set_metabolics(email, energy=-50):
                return False

    await leaveStreet(userSocket=userSocket, email=email)

    await userSocket.send(json.dumps({"gotoStreet": "true", "tsid": tsid}))

    return True


async def leaveStreet(userSocket = None, email = None, username = None, street = None):
    async with db_session_context() as db_session:
        results = await db_session.scalars(select(User).filter(User.email == email))
        m = await results.first().awaitable_attrs.metabolics
        m.last_street = m.current_street


async def moveItem(userSocket = None, email = None, fromIndex = -1, fromBagIndex = -1, toBagIndex = -1, toIndex = -1) -> bool:
    if fromIndex == -1 or toIndex == -1:
        # something's wrong
        return False

    return await InventoryManager.move_item(
        email, from_index=fromIndex, from_bag_index=fromBagIndex, to_index=toIndex, to_bag_index=toBagIndex,
    )


async def writeNote(userSocket = None, email = None, noteData = None) -> None:
    new_note = await NoteManager.add_from_client(noteData)
    await userSocket.send(json.dumps({"note_response": new_note}))
    await InventoryManager.decrease_durability(email, NoteManager.TOOL_ITEM)


def normalize_method_name(name: str) -> str:
    new_name = ""
    parts = name.split(" ")
    for index, part in enumerate(parts):
        if index > 0:
            parts[index] = parts[index][0:1].upper() + parts[index][1:]
        new_name += parts[index]

    new_name = new_name[0:1].lower() + new_name[1:]
    return new_name


def queue_npc_remove(entity_id):
    pass


@street_blueprint.route("/getActions")
async def get_actions():
    email = request.args.get("email")
    id = request.args.get("id")
    label = request.args.get("label")

    if email is None or id is None or label is None:
        logger.debug(
            f"<email={email}> tried to get actions for <id={id}> on <label={label}>"
        )
        return jsonify([])

    if not streets.get(label):
        logger.debug(f"<label={label}> is not a currently loaded street")
        return jsonify([])

    entity = streets[label].npcs.get(id)
    if not entity:
        entity = streets[label].plants.get(id)
    if not entity:
        entity = streets[label].doors.get(id)
    if not entity:
        entity = streets[label].ground_items.get(id)
    if not entity:
        # are they a player?
        if users.get(id):
            follow_action = Action(action="follow")
            follow_action.description = (
                "We already know it's buggy, but we thought you'd have fun with it."
            )
            profile_action = Action(action="profile")
            return jsonify([follow_action, profile_action])
    if not entity:
        logger.debug(f"<id={id}> is not a valid entity on <label={label}>")
        return jsonify([])

    return [action.to_json() for action in await entity.customize_actions(email)]
