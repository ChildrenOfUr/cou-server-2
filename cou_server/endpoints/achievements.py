from quart import Blueprint, jsonify, request
from quart_cors import cors
from sqlalchemy.sql import text

from cou_server.achievements.achievements import Achievement, _ACHIEVEMENTS
from cou_server.common import get_logger
from cou_server.common.database import remote_engine


achievements_endpoint = Blueprint("achievements_endpoint", __name__)
cors(achievements_endpoint)

logger = get_logger(__name__)
cached_achv_list = None


@achievements_endpoint.route("/listAchievements")
async def achievements_get():
    email = request.args.get("email", default=None)
    category = request.args.get("category", default=None)
    username = request.args.get("username", default=None)
    exclude_non_matches = request.args.get("excludeNonMatches", default="true") == "true"
    generic = False

    if email is None and category is None and username is None and exclude_non_matches is None:
        generic = True
        # generic request
        if cached_achv_list is not None:
            return jsonify(cached_achv_list)

    ids = list(_ACHIEVEMENTS.keys())
    maps = {}

    if (email is not None or username is not None) or not exclude_non_matches:
        # email or username provide, find their awarded achievements
        # OR
        # including non matches, need something to match against
        try:
            async with remote_engine.connect() as conn:
                query = "SELECT achievements FROM users WHERE "
                data = {}
                if email:
                    query += "email = :email"
                    data["email"] = email
                elif username:
                    query += "username = :username"
                    data["username"] = username
                else:
                    return jsonify({})
                awarded_ids = (await conn.execute(text(query), parameters=data)).mappings().first()["achievements"]
        except Exception:
            logger.exception(f"Error getting achievements for <email={email or username}>")
            return jsonify({})

    if email is None and username is None:
        # list ALL achievements
        for id in ids:
            achv = Achievement.find(id)
            if category is not None:
                if achv.category != category:
                    continue
            maps[achv.id] = achv.to_json()
    else:
        # list AWARDED achievements (or all with marked matches)
        if not exclude_non_matches:
            # include all, but mark matches
            for id in ids:
                achv = Achievement.find(id)
                if category is not None:
                    if achv.category != category:
                        continue
                achv_map = achv.to_json()
                achv_map["awarded"] = str(id in awarded_ids).lower()
                maps[achv.id] = achv_map
        else:
            # include only awarded
            for id in ids:
                achv = Achievement.find(id)
                if category is not None:
                    if achv.category != category:
                        continue
                maps[achv.id] = achv.to_json()

    if generic and cached_achv_list is None:
        cached_achv_list = maps

    return jsonify(maps)
