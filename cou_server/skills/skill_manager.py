from importlib import resources
import json

from quart import Blueprint, jsonify, request
from quart_cors import cors
from sqlalchemy.future import select

from cou_server import api_keys
from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.common.util import toast
from cou_server.models.user import User
from cou_server.skills.player_skill import PlayerSkill
from cou_server.skills.skill import Skill


logger = get_logger(__name__)


logger = get_logger(__name__)
skills_blueprint = Blueprint("skills_blueprint", __name__, url_prefix="/skills")
cors(skills_blueprint)

# All loaded skills...do not edit after initial load!
skills = {}


def load_skills() -> int:
    for skill_file in [
        i for i in resources.files("data.skills").iterdir() if str(i).endswith(".json")
    ]:
        with resources.files("data.skills").joinpath(skill_file).open() as skill_file:
            for name, skill_map in json.loads(skill_file.read()).items():
                skills[name] = Skill(**skill_map)
    logger.debug(f"[Skills] loaded {len(skills)} skills")
    return len(skills)


class SkillManager:
    @staticmethod
    async def learn(skill_id: str, email: str, new_points: int = 1) -> bool:
        from cou_server.endpoints.street_update import user_sockets

        # get existing skill or add if new
        skill = await PlayerSkill.find(skill_id, email)
        if skill.points == 0:
            await toast(
                f"You've started learning {skill.name}!",
                user_sockets[email],
                on_click="imgmenu",
            )

        # save to database
        success = await skill.add_points(new_points)

        if success["level_up"]:
            await toast(
                f"Your {skill.name} skill is now at level {skill.level}",
                user_sockets[email],
                on_click="imgmenu",
            )

        return success["writing"]

    @staticmethod
    async def get_level(skill_id: str, email: str) -> int:
        skill = await PlayerSkill.find(skill_id, email)
        if skill and skill.level:
            return skill.level
        return 0

    @staticmethod
    async def get_player_skills(email=None, username=None) -> list[PlayerSkill]:
        if email is None and username is None:
            return None

        try:
            async with db_session_context() as db_session:
                if email is None and username != None:
                    query = select(User).filter(User.username == username)
                else:
                    query = select(User).filter(User.email == email)
                player_skills_json = (await ((await db_session.scalars(query)).first()).awaitable_attrs.metabolics).skills_json

                # fill in skill information
                player_skills_list = []
                for id, points in player_skills_json.items():
                    player_skills_list.append(
                        PlayerSkill(Skill.find(id), email, points).to_map()
                    )

                return player_skills_list
        except Exception:
            logger.exception(f"Error getting skill list for email <email={email}>")


@skills_blueprint.route("/get/<string:email>")
async def skills_get(email):
    return jsonify(await SkillManager.get_player_skills(email=email))


@skills_blueprint.route("/getByUsername/<username>")
async def get_player_skills_username_route(username):
    return jsonify(await SkillManager.get_player_skills(username=username))


cached_data = None


@skills_blueprint.route("/list")
async def all_skills():
    global cached_data
    token = request.args.get("token")
    if token != api_keys.REDSTONE_TOKEN:
        return jsonify([{"error": "true"}, {"token": "invalid"}])

    if cached_data != None:
        return cached_data
    else:
        result = []
        for skill in skills.values():
            result.append(skill.to_map())
        cached_data = result
        return result
