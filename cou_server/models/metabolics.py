import json

from marshmallow import Schema, fields
from sqlalchemy import Column, Float, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref

from cou_server.common.database import Base
from cou_server.endpoints import mapdata


class Metabolics(Base):
    __tablename__ = "metabolics"

    id = Column(Integer, primary_key=True)
    mood = Column(Integer, nullable=False, default=50)
    max_mood = Column(Integer, nullable=False, default=100)
    energy = Column(Integer, nullable=False, default=50)
    max_energy = Column(Integer, nullable=False, default=100)
    currants = Column(Integer, nullable=False, default=0)
    img = Column(Integer, nullable=False, default=0)
    lifetime_img = Column(Integer, nullable=False, default=0)
    current_street = Column(String, nullable=False, default="LA58KK7B9O522PC")
    last_street = Column(String)
    undead_street = Column(String)
    current_street_x = Column(Float, nullable=False, default=1.0)
    current_street_y = Column(Float, nullable=False, default=0.0)
    quoin_multiplier = Column(Integer, default=1)
    quoins_collected = Column(Integer, default=0)
    _location_history = Column("location_history", String, default="[]")
    _skills_json = Column("skills_json", String, default="{}")
    _buffs_json = Column("buffs_json", String, default="{}")
    alphfavor = Column(Integer, default=0)
    alphfavor_max = Column(Integer, default=1000)
    cosmafavor = Column(Integer, default=0)
    cosmafavor_max = Column(Integer, default=1000)
    friendlyfavor = Column(Integer, default=0)
    friendlyfavor_max = Column(Integer, default=1000)
    grendalinefavor = Column(Integer, default=0)
    grendalinefavor_max = Column(Integer, default=1000)
    humbabafavor = Column(Integer, default=0)
    humbabafavor_max = Column(Integer, default=1000)
    lemfavor = Column(Integer, default=0)
    lemfavor_max = Column(Integer, default=1000)
    mabfavor = Column(Integer, default=0)
    mabfavor_max = Column(Integer, default=1000)
    potfavor = Column(Integer, default=0)
    potfavor_max = Column(Integer, default=1000)
    tiifavor = Column(Integer, default=0)
    tiifavor_max = Column(Integer, default=1000)
    zillefavor = Column(Integer, default=0)
    zillefavor_max = Column(Integer, default=1000)

    # relationships to other tables
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", backref=backref("metabolics", uselist=False))

    @property
    def energy_percent(self):
        return 100 * (self.energy / self.max_energy)

    @property
    def mood_percent(self):
        return 100 * (self.mood / self.max_mood)

    @property
    def dead(self):
        return getattr(self, "_dead", False)

    @dead.setter
    def dead(self, new_val):
        if new_val:
            # die
            self.undead_street = mapdata.tsid_l(self.current_street)
            self.energy = 0
            self.mood = 0

            # don't revive to the Wintry Place, enerty will deplete too soon
            if self.undead_street == mapdata.tsid_l(
                mapdata.streets()["Wintry Place"]["tsid"]
            ):
                self.undead_street = mapdata.tsid_l(
                    mapdata.streets()["Northwest Passage"]["tsid"]
                )
        else:
            # revive
            self.undead_street = None
            self.energy = self.max_energy // 10
            self.mood = self.max_mood // 10

    def add_img(self, amount):
        self.img += amount
        self.lifetime_img += amount

    @property
    def location_history(self):
        return json.loads(self._location_history)

    @location_history.setter
    def location_history(self, new_history):
        self._location_history = json.dumps(new_history)

    @property
    def skills_json(self):
        return json.loads(self._skills_json)

    @skills_json.setter
    def skills_json(self, new_skills):
        self._skills_json = json.dumps(new_skills)

    @property
    def buffs_json(self):
        return json.loads(self._buffs_json)

    @buffs_json.setter
    def buffs_json(self, new_buffs):
        self._buffs_json = json.dumps(new_buffs)


class MetabolicsSchema(Schema):
    alphFavor = fields.Integer(attribute="alphfavor")
    alphFavorMax = fields.Integer(attribute="alphfavor_max")
    cosmaFavor = fields.Integer(attribute="cosmafavor")
    cosmaFavorMax = fields.Integer(attribute="cosmafavor_max")
    currants = fields.Integer()
    currentStreet = fields.String(attribute="current_street")
    currentStreetX = fields.Float(attribute="current_street_x")
    currentStreetY = fields.Integer(attribute="current_street_y")
    energy = fields.Integer()
    friendlyFavor = fields.Integer(attribute="friendlyfavor")
    friendlyFavorMax = fields.Integer(attribute="friendlyfavor_max")
    grendalineFavor = fields.Integer(attribute="grendalinefavor")
    grendalineFavorMax = fields.Integer(attribute="grendaliefavor_max")
    humbabaFavor = fields.Integer(attribute="humbabafavor")
    humbabaFavorMax = fields.Integer(attribute="humbabafavor_max")
    id = fields.Integer()
    img = fields.Integer()
    lastStreet = fields.String(attribute="last_street")
    lemFavor = fields.Integer(attribute="lemfavor")
    lemFavorMax = fields.Integer(attribute="lemfavor_max")
    lifetimeImg = fields.Integer(attribute="lifetime_img")
    locationHistory = fields.String(attribute="_location_history")
    mabFavor = fields.Integer(attribute="mabfavor")
    mabFavorMax = fields.Integer(attribute="mabfavor_max")
    maxEnergy = fields.Integer(attribute="max_energy")
    maxMood = fields.Integer(attribute="max_mood")
    mood = fields.Integer()
    potFavor = fields.Integer(attribute="potfavor")
    potFavorMax = fields.Integer(attribute="potfavor_max")
    quoinMultiplier = fields.Integer(attribute="quoin_multiplier")
    quoinsCollected = fields.Integer(attribute="quoins_collected")
    tiiFavor = fields.Integer(attribute="tiifavor")
    tiiFavorMax = fields.Integer(attribute="tiifavor_max")
    undeadStreet = fields.String(attribute="undead_street")
    user_id = fields.Integer()
    zilleFavor = fields.Integer(attribute="zillefavor")
    zilleFavorMax = fields.Integer(attribute="zillefavor_max")


# LEVELS

img_levels = {
    1: 100,
    2: 137,
    3: 188,
    4: 258,
    5: 353,
    6: 484,
    7: 663,
    8: 908,
    9: 1244,
    10: 1704,
    11: 2334,
    12: 3198,
    13: 4381,
    14: 6002,
    15: 8223,
    16: 11266,
    17: 15434,
    18: 21145,
    19: 28969,
    20: 39688,
    21: 54373,
    22: 74491,
    23: 102053,
    24: 139813,
    25: 191544,
    26: 262415,
    27: 359509,
    28: 492527,
    29: 674762,
    30: 924424,
    31: 1266461,
    32: 1735052,
    33: 2377021,
    34: 3256519,
    35: 4461431,
    36: 6112160,
    37: 8373659,
    38: 11471913,
    39: 15716521,
    40: 21531634,
    41: 29498339,
    42: 40412724,
    43: 55365432,
    44: 75850642,
    45: 103915380,
    46: 142364071,
    47: 195038777,
    48: 267203124,
    49: 366068280,
    50: 501513544,
    51: 687073555,
    52: 941290770,
    53: 1289568355,
    54: 1766708646,
    55: 2420390845,
    56: 3315935458,
    57: 4542831577,
    58: 6223679260,
    59: 8526440586,
    60: 11681223603,
}

energy_levels = {
    0: 100,
    1: 100,
    2: 110,
    3: 120,
    4: 130,
    5: 140,
    6: 150,
    7: 170,
    8: 190,
    9: 210,
    10: 230,
    11: 250,
    12: 270,
    13: 300,
    14: 330,
    15: 360,
    16: 390,
    17: 420,
    18: 450,
    19: 480,
    20: 520,
    21: 560,
    22: 600,
    23: 640,
    24: 700,
    25: 750,
    26: 800,
    27: 850,
    28: 900,
    29: 950,
    30: 1100,
    31: 1150,
    32: 1200,
    33: 1260,
    34: 1310,
    35: 1370,
    36: 1430,
    37: 1490,
    38: 1550,
    39: 1610,
    40: 1670,
    41: 1740,
    42: 1830,
    43: 1900,
    44: 1970,
    45: 2050,
    46: 2130,
    47: 2210,
    48: 2290,
    49: 2370,
    50: 2460,
    51: 2550,
    52: 2640,
    53: 2730,
    54: 2830,
    55: 2930,
    56: 3030,
    57: 3140,
    58: 3240,
    59: 3340,
    60: 3450,
}
