import dataclasses

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref

from cou_server.common.database import Base
from cou_server.common.serializable import serializable


class InventoryModel(Base):
    __tablename__ = "inventories"

    inventory_id = Column(Integer, primary_key=True)
    inventory_json = Column(String, nullable=False)

    # relationships to other tables
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", backref=backref("inventory", uselist=False))


@serializable
@dataclasses.dataclass
class Slot:
    # a new instance of a Slot is empty by default
    item_type: str = ""
    count: int = 0
    metadata: dict[str, str] = dataclasses.field(default_factory=dict)

    @property
    def is_empty(self) -> bool:
        return self.item_type == "" or self.count == 0


@serializable
@dataclasses.dataclass
class Inventory:
    # Sets how many slots each player has
    inv_size: int = 10
    slots: list[Slot] = dataclasses.field(default_factory=list)
