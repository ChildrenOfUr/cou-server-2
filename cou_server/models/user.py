import json

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.future import select

from cou_server.common.database import Base, db_session_context


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False)
    email = Column(String, nullable=False)
    bio = Column(String)
    custom_avatar = Column(String)
    username_color = Column(String, default="#      ")
    elevation = Column(String, default="_")
    last_login = Column(DateTime)
    _friends = Column("friends", String, default="[]")

    def __repr__(self):
        return f'User(id="{self.id}", username="{self.username}", email="{self.email}")'

    async def get_friends(self, db_session):
        return (await db_session.scalars(select(User).filter(User.id.in_(json.loads(self._friends))))).all()

    def set_friends(self, db_session, new_friends):
        self._friends = json.dumps([f.id for f in new_friends])

    @classmethod
    async def get_id_from_email(cls, email: str) -> int:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.email == email))).one().id

    @classmethod
    async def get_id_from_username(cls, username: str) -> int:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.username == username))).one().id

    @classmethod
    async def get_username_from_email(cls, email: str) -> str:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.email == email))).one().username

    @classmethod
    async def get_username_from_id(cls, id: int) -> str:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.id == id))).one().username

    @classmethod
    async def get_email_from_id(cls, id: int) -> str:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.id == id))).one().email

    @classmethod
    async def get_email_from_username(cls, username: str) -> str:
        async with db_session_context() as db_session:
            return (await db_session.scalars(select(User).filter(User.username == username))).one().email


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
