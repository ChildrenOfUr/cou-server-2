import random

from sqlalchemy.future import select

from cou_server.achievements.achievements import Achievement
from cou_server.achievements.stats import Stat, StatManager
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.database import db_session_context
from cou_server.common.message_bus import MessageBus
from cou_server.common.util import clamp
from cou_server.models.metabolics import Metabolics
from cou_server.models.user import User
from cou_server.quests import messages


class MetabolicsChange:
    def __init__(self):
        self.gains = {"energy": 0, "mood": 0, "img": 0, "currants": 0}

    @classmethod
    async def get_metabolics(cls, email) -> Metabolics:
        async with db_session_context() as db_session:
            user = (await db_session.scalars(select(User).filter(User.email == email))).first()
            m = await user.awaitable_attrs.metabolics
            db_session.expunge(m)
            return m

    @classmethod
    async def set_metabolics(cls, metabolics: Metabolics) -> None:
        async with db_session_context() as db_session:
            await db_session.merge(metabolics)

    async def try_set_metabolics(
        self, email, rewards=None, energy=0, mood=0, img_min=0, img_range=0, currants=0
    ):
        """ Try to set the metabolics belongning to [email]. If [rewards] is set,
            it will take precedence over the other metabolics passed in.
        """

        self.reset_gains()

        if rewards:
            energy = rewards.energy
            mood = rewards.mood
            img_min = rewards.img
            currants = rewards.currants
            await self.try_set_favor(email, None, None, favors=rewards.favor)

        async with db_session_context() as db_session:
            user = (await db_session.scalars(select(User).filter(User.email == email))).first()
            m = await user.awaitable_attrs.metabolics

            # if we're taking away energy, make sure we have enough
            if energy < 0 and m.energy < abs(energy):
                return False

            # if we're taking away currents, make sure we have enough
            if currants < 0 and m.currants < abs(currants):
                return False

            energy = clamp(m.energy + energy, 0, m.max_energy) - m.energy
            mood = clamp(m.mood + mood, 0, m.max_mood) - m.mood
            m.energy += energy
            m.mood += mood
            m.currants += currants
            base_img = img_min
            if img_range > 0:
                base_img = random.randint(0, img_range) + img_min
            result_img = int(base_img * m.mood / m.max_mood)
            m.img += result_img
            m.lifetime_img += result_img

        # send results to client
        self.gains["energy"] = energy
        self.gains["mood"] = mood
        self.gains["img"] = result_img
        self.gains["currants"] = currants

        return True

    async def try_set_favor(self, email, giant_name, fav_amt, favors=None):
        metabolics = await self.get_metabolics(email=email)

        if favors:
            for favor in favors:
                metabolics = await self._set_favor(
                    email, metabolics, favor.giant_name, favor.fav_amt
                )
        else:
            metabolics = await self._set_favor(email, metabolics, giant_name, fav_amt)

        await self.set_metabolics(metabolics)
        return metabolics

    def reset_gains(self):
        self.gains = {"energy": 0, "mood": 0, "img": 0, "currants": 0}

    async def _set_favor(self, email, metabolics, giant_name, fav_amt):
        from cou_server.endpoints.inventory import InventoryManager

        if giant_name is None:
            return metabolics

        giant_name = giant_name.lower()
        giant_favor = getattr(metabolics, f"{giant_name}favor")
        max_amt = getattr(metabolics, f"{giant_name}favor_max")

        if giant_favor + fav_amt >= max_amt:
            setattr(metabolics, f"{giant_name}favor", 0)
            max_amt += 100
            setattr(metabolics, f"{giant_name}favor_max", max_amt)
            await InventoryManager.add_item_to_user(email, cou_globals.items["emblem_of_" + giant_name], 1)
            await Achievement.find(f"first_emblem_of_{giant_name}").award_to(email)

            # end emblem quest
            MessageBus.publish("requirement_progress", messages.RequirementProgress("emblemGet", email))
            await StatManager.add(email, Stat.emblems_collected)
        else:
            setattr(metabolics, f"{giant_name}favor", giant_favor + fav_amt)

        await StatManager.add(email, Stat.favor_earned, increment=fav_amt)
        return metabolics

    async def get_mood(self, username=None, email=None, user_id=None):
        if not username and not email and not user_id:
            raise ValueError("You must pass either username, email, or user_id")

        return (await self.get_metabolics(username=username, email=email, user_id=user_id)).mood

    async def get_energy(self, username=None, email=None, user_id=None):
        if not username and not email and not user_id:
            raise ValueError("You must pass either username, email, or user_id")

        return (await self.get_metabolics(username=username, email=email, user_id=user_id)).energy
