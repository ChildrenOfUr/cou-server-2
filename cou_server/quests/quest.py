import asyncio
import dataclasses
import json
import re
from threading import Timer

from cou_server.common.message_bus import MessageBus
from cou_server.common.serializable import serializable
from cou_server.endpoints.quest import user_sockets
from cou_server.metabolics.metabolics_change import MetabolicsChange
from cou_server.quests import messages
from cou_server.quests.messages import RequirementProgress
from cou_server.quests.trackable import Trackable


@serializable
@dataclasses.dataclass
class ConvoChoice:
    text: str = None
    goto_screen: int = None
    is_quest_accept: bool = False
    is_quest_reject: bool = False


@serializable
@dataclasses.dataclass
class ConvoScreen:
    paragraphs: list[str] = None
    choices: list[ConvoChoice] = None


@serializable
@dataclasses.dataclass
class Conversation:
    id: str = None
    screens: list[ConvoScreen] = None


@serializable
@dataclasses.dataclass
class QuestFavor:
    giant_name: str = None
    fav_amt: int = 0


@serializable
@dataclasses.dataclass
class QuestRewards:
    energy: int = 0
    mood: int = 0
    img: int = 0
    currants: int = 0
    favor: list[QuestFavor] = dataclasses.field(default_factory=list)


@serializable
@dataclasses.dataclass
class Requirement(Trackable):
    numRequired: int = None
    timeLimit: int = None
    id: str = None
    text: str = None
    type: str = None
    eventType: str = None
    iconUrl: str = ""
    typeDone: list[str] = dataclasses.field(default_factory=list)

    _fulfilled: bool = dataclasses.field(init=False, default=False)
    _num_fulfilled: int = dataclasses.field(init=False, default=0)

    def __post_init__(self):
        super().__init__()

    @property
    def fulfilled(self) -> bool:
        return self._fulfilled

    @fulfilled.setter
    def fulfilled(self, new_val: bool) -> None:
        self._fulfilled = new_val
        if self._fulfilled and self.being_tracked:
            MessageBus.publish(
                "complete_requirement", messages.CompleteRequirement(self, self.email)
            )

    @property
    def numFulfilled(self) -> int:
        return self._num_fulfilled

    @numFulfilled.setter
    def numFulfilled(self, new_val: int) -> None:
        self._num_fulfilled = new_val
        if self._num_fulfilled == self.numRequired:
            self.fulfilled = True

    def handle_requirement_progress(self, progress: RequirementProgress) -> None:
        if progress.email != self.email:
            return

        good_event = False
        count = 1
        if self._matching_event(progress.event_type):
            if (
                self.type == "counter_unique"
                and progress.event_type not in self.typeDone
            ):
                good_event = True
                self.typeDone.append(progress.event_type)
            elif self.type == "counter" or self.type == "timed":
                good_event = True
                count = progress.count

        if not good_event or self.fulfilled:
            return

        self.numFulfilled += count
        MessageBus.publish(
            "requirement_updated", messages.RequirementUpdated(self, self.email)
        )

    def start_tracking(self, email: str) -> None:
        if self.fulfilled:
            return

        super().start_tracking(email)

        self.mb_subscriptions.append(
            MessageBus.subscribe("requirement_progress", method=self.handle_requirement_progress)
        )

        if self.type == "timed":

            def fail_time_limit():
                MessageBus.publish(
                    "fail_requirement", messages.FailRequirement(self, email)
                )

            self.limit_timer = Timer(self.timeLimit, fail_time_limit)

    def _matching_event(self, event: str) -> bool:
        if re.search(self.eventType, event):
            return True
        return False


@serializable
@dataclasses.dataclass
class Quest(Trackable):
    id: str = dataclasses.field(default=None, compare=True, hash=True)
    title: str = None
    description: str = None
    complete: bool = False
    prerequisites: list[str] = dataclasses.field(default_factory=list)
    requirements: list[Requirement] = dataclasses.field(default_factory=list)
    conversation_start: Conversation = None
    conversation_end: Conversation = None
    conversation_fail: Conversation = None
    rewards: QuestRewards = None

    def __post_init__(self):
        Trackable.__init__(self)

        self.mb_subscriptions.append(
            MessageBus.subscribe("complete_requirement", method=self.handle_complete_requirement)
        )
        self.mb_subscriptions.append(
            MessageBus.subscribe("fail_requirement", method=self.handle_fail_requirement)
        )
        self.mb_subscriptions.append(
            MessageBus.subscribe("requirement_updated", method=self.handle_requirement_updated)
        )

    def __hash__(self) -> int:
        return hash(self.id)

    async def handle_complete_requirement(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        try:
            next(r for r in self.requirements if not r.fulfilled)
        except StopIteration:
            self.complete = True
            MessageBus.publish(
                "complete_quest", messages.CompleteQuest(self, self.email)
            )
            await self._give_rewards(data.email)

    def handle_fail_requirement(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        MessageBus.publish("fail_quest", messages.FailQuest(self, self.email))
        self.stop_tracking()

    async def handle_requirement_updated(self, data):
        if data.requirement not in self.requirements or data.email != self.email:
            return

        user_socket = user_sockets.get(self.email)
        if not user_socket:
            return
        update_message = {"questUpdate": True, "quest": self.to_json()}
        await user_socket.send(json.dumps(update_message))

    def start_tracking(self, email, just_started=False):
        if self.complete:
            return

        super().start_tracking(email)

        for requirement in self.requirements:
            requirement.start_tracking(email)

        heading = "questBegin" if just_started else "questInProgress"
        quest_in_progress = {heading: True, "quest": self.to_json()}
        user_socket = user_sockets.get(self.email)
        if user_socket:
            asyncio.create_task(user_socket.send(json.dumps(quest_in_progress)))

    async def _give_rewards(self, email: str):
        return await MetabolicsChange().try_set_metabolics(email, rewards=self.rewards)

    def stop_tracking(self):
        super().stop_tracking()
        for requirement in self.requirements:
            requirement.stop_tracking()
