import asyncio
import json

from marshmallow import fields
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import backref, relationship, reconstructor

from cou_server.common.database import Base
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints import quest as quest_service
from cou_server.endpoints.inventory import InventoryManager
from cou_server.models.user import UserSchema
from cou_server.quests import messages
from cou_server.quests.quest import Quest
from cou_server.quests.trackable import Trackable


class QuestLog(Base, Trackable):
    __tablename__ = "user_quests"

    id = Column(Integer, primary_key=True)
    completed_list = Column(String, default="[]")
    in_progress_list = Column(String, default="[]")

    # relationships to other tables
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", backref=backref("quest_log", uselist=False))

    def __init__(self, **kwargs):
        self.completed_list = "[]"
        self.in_progress_list = "[]"

        for prop in kwargs:
            setattr(self, prop, kwargs[prop])

        self.do_load()

    @reconstructor
    def do_load(self):
        self.offering_quest = False
        Trackable.__init__(self)

    def __repr__(self):
        return f"QuestLog: {QuestLogSchema().dump(self)}"

    @property
    def completed_quests(self) -> list[Quest]:
        return Quest.from_json(self.completed_list, many=True)

    @property
    def in_progress_quests(self) -> list[Quest]:
        return Quest.from_json(self.in_progress_list, many=True)

    async def handle_complete_quest(self, data):
        if data.email != self.email:
            return

        data.quest.complete = True
        data.quest.stop_tracking()
        quest_complete = {
            "questComplete": True,
            "quest": data.quest.to_json(),
        }
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            await user_socket.send(json.dumps(quest_complete))

        self.in_progress_list = json.dumps([q.to_json() for q in self.in_progress_quests if q != data.quest])
        self.completed_list = json.dumps([q.to_json() for q in self.completed_quests + [data.quest]])

        await quest_service.update_quest_log(self)

        # if the completed the sammich quest, go get a snocone
        # wait for a minute before offering
        if data.quest.id == "Q1":
            await asyncio.sleep(60)
            await self.offer_quest("Q7")

    async def handle_fail_quest(self, data: messages.FailQuest):
        if data.email != self.email:
            return

        data.quest.stop_tracking()

        quest_fail = {"questFail": True, "quest": data.quest.to_json()}
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            await user_socket.send(json.dumps(quest_fail))

        self.in_progress_list = json.dumps([q.to_json() for q in self.in_progress_quests if q != data.quest])

        await quest_service.update_quest_log(self)

    async def handle_requirement_updated(self, data: messages.RequirementUpdated) -> None:
        if data.email != self.email:
            return

        await quest_service.update_quest_log(self)

    def _stop_accept_reject_subscriptions(self) -> None:
        """ Since the accept and reject are only temporary and once the user
            decides, we should stop listening for more (until next time) so that
            we aren't double, triple, etc. notified.
        """

        try:
            for channel in ["accept_quest", "reject_quest"]:
                subscription = next(
                    iter([s for s in self.mb_subscriptions if s.channel == channel]),
                    None,
                )
                MessageBus().unsubscribe(subscription)
                self.mb_subscriptions.remove(subscription)
        except Exception:
            # best effort
            pass

    async def handle_accept_quest(self, data: messages.AcceptQuest, headers=None) -> None:
        if data.email != self.email:
            return

        if headers and headers.get("fromItem", None) == False:
            await InventoryManager.take_item_from_user(data.email, headers["slot"], headers["subSlot"], 1)

        self._stop_accept_reject_subscriptions()
        self.offering_quest = False
        await self.add_in_progress_quest(data.quest_id)

    def handle_reject_quest(self, data: messages.RejectQuest) -> None:
        if data.email != self.email:
            return

        self._stop_accept_reject_subscriptions()
        self.offering_quest = False

    async def start_tracking(self, email: str) -> None:
        super().start_tracking(email)

        # listen for quest completion events
        # if they don't belong to us, let someone else get them
        # if they do belong to us, send a message to the client to tell them of their success
        self.mb_subscriptions.append(
            MessageBus.subscribe("complete_quest", method=self.handle_complete_quest)
        )
        self.mb_subscriptions.append(
            MessageBus.subscribe("fail_quest", method=self.handle_fail_quest)
        )
        self.mb_subscriptions.append(
            MessageBus.subscribe("requirement_updated", method=self.handle_requirement_updated)
        )

        # start tracking on all our in progress quests
        for quest in self.in_progress_quests:
            quest.start_tracking(email)

    async def stop_tracking(self):
        super().stop_tracking()
        for quest in self.in_progress_quests:
            quest.stop_tracking()
        await quest_service.update_quest_log(self)

    async def add_in_progress_quest(self, quest_id):
        quest_to_add: Quest = Quest.clone(quest_service.quests[quest_id])
        if self._doing_or_done(quest_to_add):
            return False

        quest_to_add.start_tracking(self.email, just_started=True)
        self.in_progress_list = json.dumps([q.to_json() for q in self.in_progress_quests + [quest_to_add]])
        await quest_service.update_quest_log(self)

        return True

    def _doing_or_done(self, quest):
        if quest is None:
            return True
        if quest in self.completed_quests or quest in self.in_progress_quests:
            return True
        return False

    async def offer_quest(self, quest_id, from_item=False, slot=-1, sub_slot=-1):
        if self.offering_quest or self._doing_or_done(quest_service.quests[quest_id]):
            return

        quest_to_offer: Quest = Quest.clone(quest_service.quests[quest_id])

        # check if prerequisite quests are complete
        for prereq in quest_to_offer.prerequisites:
            previous_quest = Quest.clone(quest_service.quests[prereq])
            if previous_quest not in self.completed_quests:
                return

        headers = {"fromItem": from_item, "slot": slot, "subSlot": sub_slot}
        self.mb_subscriptions.append(
            MessageBus.subscribe("accept_quest", method=self.handle_accept_quest, headers=headers)
        )
        self.mb_subscriptions.append(
            MessageBus.subscribe("reject_quest", method=self.handle_reject_quest)
        )

        quest_offer = {"questOffer": True, "quest": quest_to_offer.to_json()}
        user_socket = quest_service.user_sockets.get(self.email)
        if user_socket:
            await user_socket.send(json.dumps(quest_offer))
        self.offering_quest = True


class QuestLogSchema(SQLAlchemyAutoSchema):
    in_progress_quests = fields.List(fields.Nested(getattr(Quest, "_QuestSchema")))
    completed_quests = fields.List(fields.Nested(getattr(Quest, "_QuestSchema")))
    user = fields.Nested(UserSchema)

    class Meta:
        model = QuestLog
