from functools import total_ordering


class Rectangle:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def __str__(self):
        return f"At {self.x}, {self.y}: {self.width}x{self.height}"

    def __eq__(self, other):
        return (
            self.x == other.x
            and self.y == other.y
            and self.width == other.width
            and self.height == other.height
        )


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"A point {self.x},{self.y}"


class Wall:
    def __init__(self, wall: dict, layer: dict, ground_y: int):
        self.width = wall["w"]
        self.height = wall["h"]
        self.x = wall["x"] + layer["w"] // 2 - self.width // 2
        self.y = wall["y"] + layer["h"] + ground_y
        self.id = wall["id"]

        self.bounds = Rectangle(self.x, self.y, self.width, self.height)

    def __str__(self):
        return f"wall {self.id}: {self.bounds}"


@total_ordering
class CollisionPlatform:
    def __init__(self, platform_line: dict, layer: dict, ground_y: int):
        self.id = platform_line["id"]
        self.ceiling = platform_line.get("platform_pc_perm") == 1
        self.item_perm = True

        for endpoint in platform_line["endpoints"]:
            if endpoint["name"] == "start":
                self.start = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.start = Point(
                        endpoint["x"] + layer["w"] // 2,
                        endpoint["y"] + layer["h"] + ground_y,
                    )
            if endpoint["name"] == "end":
                self.end = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.end = Point(
                        endpoint["x"] + layer["w"] // 2,
                        endpoint["y"] + layer["h"] + ground_y,
                    )

        width = self.end.x - self.start.x
        height = self.end.y - self.start.y
        self.bounds = Rectangle(self.start.x, self.start.y, width, height)

    def __repr__(self):
        return f"({self.start.x},{self.start.y})->({self.end.x},{self.end.y}) ceiling={self.ceiling}"

    def __hash__(self):
        return hash(self.start.x + self.start.y + self.end.x + self.end.y)

    def __eq__(self, other: "CollisionPlatform") -> bool:
        return self.start.y == other.start.y

    def __lt__(self, other: "CollisionPlatform") -> bool:
        return self.start.y < other.start.y
