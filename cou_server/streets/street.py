import asyncio

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import reconstructor

from cou_server.common import get_logger
from cou_server.common.database import Base
from cou_server.streets.street_like import StreetLike


logger = get_logger(__name__)


class Street(Base, StreetLike):
    __tablename__ = "streets"

    id = Column(String, primary_key=True)
    items = Column(String, default="[]")
    uid = Column(Integer, nullable=False, default=-1)
    tsid = Column(String)

    def __str__(self):
        return f"This is a Street with an id of {self.id} which has {len(self.db_ground_items)} items on it"
