import json

from sqlalchemy.future import select

from cou_server.common import get_logger
from cou_server.common.database import db_session_context
from cou_server.endpoints import mapdata
from cou_server import entities
from cou_server.entities.doors.door import Door
from cou_server.entities.items.item import Item
from cou_server.entities.npcs.npc import NPC
from cou_server.entities.npcs.vendors.vendor import Vendor
from cou_server.entities.plants.plant import Plant
from cou_server.entities.quoin import Quoin
from cou_server.models.entity import Entity
from cou_server.streets.street_objects import CollisionPlatform, Rectangle, Wall


LOGGER = get_logger(__name__)


class StreetLike:
    _json_cache = {}
    _persist_lock = {}

    @property
    def db_ground_items(self) -> list[Item]:
        items = []
        for item_map in json.loads(self.items):
            items.append(Item.from_json(item_map))
        return items

    @db_ground_items.setter
    def db_ground_items(self, new_items: list[Item]) -> None:
        self.items = json.dumps(new_items)

    async def load_task(self):
        self.loaded = False
        self.occupants = {}
        self.platforms: list[CollisionPlatform] = []
        self.walls = []
        self.ground_y = 0
        self.quoins = {}
        self.plants = {}
        self.npcs = {}
        self.doors = {}
        self.ground_items = {}
        self.entity_maps = {
            "quoin": self.quoins,
            "plant": self.plants,
            "npc": self.npcs,
            "door": self.doors,
            "groundItem": self.ground_items,
        }

        await self.load_json()
        self.load_items()
        await self.load_entities(self.tsid)
        self.loaded = True
        LOGGER.debug(
            f"Loaded street <street_name={self.label}> ({self.tsid}) into memory"
        )

    def load_items(self):
        for item in self.db_ground_items:
            item.put_on_ground(item.x, item.y, self.label, item_id=item.item_id)

    async def load_json(self, refresh_cache=False):
        tsid_g = mapdata.tsid_g(self.tsid)
        street_data = type(self)._json_cache.get(tsid_g, {})
        if refresh_cache or tsid_g not in type(self)._json_cache:
            street_data = await mapdata.get_and_cache_file(self.tsid)
            type(self)._json_cache[tsid_g] = street_data

        self.label = street_data["label"]
        self.ground_y = -abs(int((street_data["dynamic"]["ground_y"])))
        self.bounds = Rectangle(
            street_data["dynamic"]["l"],
            street_data["dynamic"]["t"],
            abs(street_data["dynamic"]["l"]) + abs(street_data["dynamic"]["r"]),
            abs(street_data["dynamic"]["t"] - street_data["dynamic"]["b"]),
        )

        # for each layer on the street...
        for layer in street_data["dynamic"]["layers"].values():
            for platform_line in layer["platformLines"]:
                self.platforms.append(CollisionPlatform(platform_line, layer, self.ground_y))

            self.platforms.sort(reverse=True)

            for wall in layer["walls"]:
                if wall.get("pc_perm") == 0:
                    continue
                self.walls.append(Wall(wall, layer, self.ground_y))

    async def load_entities(self, id: str):
        """Load the entities for the given street id. For all common streets,
        the id and tsid fields are the same. But for instanced streets, the
        id is the unique value and the tsid is a pointer to the street that the
        instance was modeled after.
        """

        async with db_session_context() as db_session:
            entities = (
                (await db_session.scalars(select(Entity)
                .filter(Entity.tsid == mapdata.tsid_l(id))))
                .all()
            )
            if not entities:
                return
            self.put_entities_in_memory(entities)

    def put_entities_in_memory(self, entities_to_load):
        for entity in entities_to_load:
            type = entity.type
            x = entity.x
            y = entity.y
            z = entity.z
            rotation = entity.rotation
            h_flip = entity.h_flip
            id = entity.id
            metadata = json.loads(entity.metadata_json)

            if type in [
                "Img",
                "Mood",
                "Energy",
                "Currant",
                "Mystery",
                "Favor",
                "Time",
                "Quarazy",
            ]:
                self.quoins[id] = Quoin(id, x, y, type.lower())
            else:
                try:
                    klass = getattr(entities, type)
                except AttributeError:
                    # this class isn't defined yet (code still under development)
                    LOGGER.debug(f"Could not load a class for {type}")
                    continue
                try:
                    kwargs = {
                        "street_name": self.label,
                        "id": id,
                        "x": x,
                        "y": y,
                        "z": z,
                        "rotation": rotation,
                        "h_flip": h_flip,
                    }
                    # Vendors and dust traps get a street name/TSID to check for collisions
                    if issubclass(klass, (Vendor,)):
                        kwargs["tsid"] = mapdata.tsid_l(self.tsid)
                    entity_instance = klass(**kwargs)
                    entity_instance.restore_state(metadata)
                    if issubclass(klass, NPC):
                        self.npcs[id] = entity_instance
                    elif issubclass(klass, Plant):
                        self.plants[id] = entity_instance
                    elif issubclass(klass, Door):
                        self.doors[id] = entity_instance
                except Exception:
                    LOGGER.exception(f"Unable to instantiate a class for {type}")
                    return False

        return True

    async def simulate(self):
        if not self.occupants:
            return False

        updates = {
            "label": self.label,
            "quoins": [],
            "npcs": [],
            "plants": [],
            "doors": [],
            "groundItems": [],
        }
        picked_up_items = []

        for plant in self.plants.values():
            plant.update(simulate_tick=True)
            updates["plants"].append(plant.get_map())
        for quoin in self.quoins.values():
            quoin.update(simulate_tick=True)
            updates["quoins"].append(quoin.get_map())
        for door in self.doors.values():
            updates["doors"].append(door.get_map())
        for id, ground_item in self.ground_items.items():
            updates["groundItems"].append(ground_item.get_map())
            # check if item was picked up and if so delete it
            # (after sending it to the client one more time)
            if not ground_item.onGround:
                picked_up_items.append(id)
        for npc in self.npcs.values():
            npc.update(simulate_tick=True)

        for id in picked_up_items:
            self.ground_items.pop(id, None)

        for username, web_socket in self.occupants.items():
            if not web_socket:
                continue

            custom_updates = dict(updates)
            custom_updates["npcs"] = []
            for npc in self.npcs.values():
                custom_updates["npcs"].append(npc.get_map(username))
            try:
                await web_socket.send(json.dumps(custom_updates))
            except Exception:
                pass

        return True

    def get_y_from_ground(self, current_x: float, current_y: float, width: float, height: float) -> float:
        """Find the y of the nearest platform"""

        return_y = current_y

        platform = self._get_best_platform(current_x, current_y, width, height)

        if platform is not None:
            going_to = current_y + self.ground_y
            slope = (platform.end.y - platform.start.y) / max(platform.end.x - platform.start.x, .00001)
            y_int = platform.start.y - slope * platform.start.x
            line_y = slope * current_x + y_int

            if return_y == current_y or going_to >= line_y:
                return_y = line_y - self.ground_y

        return return_y


    def _get_best_platform(self, pos_x: float, came_from: float, width: float, height: float) -> CollisionPlatform:
        """ONLY WORKS IF PLATFORMS ARE SORTED WITH THE HIGHEST
        (SMALLEST Y VALUE) FIRST IN THE LIST

        returns the platform line that the entity is currently standing
        [pos_x] is the current x position of the entity
        [width] and [height] are the width and height of their current animation
        """

        best_platform = None
        x = pos_x
        feet_y = came_from + self.ground_y
        best_diff_y = 10000000000000

        for platform in self.platforms:
            if platform.ceiling:
                continue

            if x >= platform.start.x and x <= platform.end.x:
                slope = (platform.end.y - platform.start.y) / max(platform.end.x - platform.start.x, .00001)
                y_int = platform.start.y - slope * platform.start.x
                line_y = slope * x + y_int
                diff_y = abs(feet_y - line_y)

                if best_platform is None:
                    best_platform = platform
                    best_diff_y = diff_y
                else:
                    if (line_y >= feet_y or (feet_y > line_y and feet_y - (height / 2) < line_y)) and diff_y < best_diff_y:
                        best_platform = platform
                        best_diff_y = diff_y

        return best_platform