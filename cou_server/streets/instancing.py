import asyncio
import dataclasses
import json

from quart import Blueprint
from quart_cors import cors
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.future import select
from sqlalchemy.orm import backref, reconstructor, relationship

from cou_server.common import get_logger
from cou_server.common.serializable import serializable
from cou_server.common.database import Base, db_session_context
from cou_server.endpoints import mapdata
from cou_server.entities.items.item import Item
from cou_server.models.user import User
from cou_server.streets.street_like import StreetLike


home_street_blueprint = Blueprint("home_street_blueprint", __name__)
cors(home_street_blueprint)

LOGGER = get_logger(__name__)


@serializable
@dataclasses.dataclass
class DBStreetInstance:
    tsid: str
    user_id: int
    is_home: bool = False
    id: str = dataclasses.field(init=False)
    items: str = None

    def __post_init__(self):
        self.id = f"{mapdata.tsid_l(self.tsid)}.{self.user_id}"

    @property
    def ground_items(self) -> list[Item]:
        return Item.from_json(self.items, many=True)

    @ground_items.setter
    def ground_items(self, items: list[Item]) -> None:
        self.items = json.dumps([i.to_json() for i in items])


class InstanceStreet(Base, StreetLike):
    __tablename__ = "home_streets"

    id = Column(Integer, primary_key=True)
    tsid = Column(String, nullable=False)
    hub_id = Column(String, nullable=False)
    label = Column(String, nullable=False)
    items = Column(String, default="[]")
    json = Column(String, default="{}")

    # relationships to other tables
    user_id = Column(Integer, ForeignKey("users.id"))
    user = relationship("User", backref=backref("user", uselist=False))

    @classmethod
    async def create(cls, tsid: str, user_id: int, label: str) -> "InstanceStreet":
        """Instance a street for a user. Returns null if the database edit fails.
        If an instance already exists, it will be returned instead.
        """

        tsid = mapdata.tsid_g(tsid)
        instance_tsid = f"{tsid}.{user_id}"
        street_json = await mapdata.get_and_cache_file(instance_tsid)
        street_json["label"] = label
        street_json["tsid"] = instance_tsid

        instance = InstanceStreet(
            tsid=instance_tsid, user_id=user_id, label=label, hub_id=street_json["hub_id"],
            json=json.dumps(street_json),
        )
        if await instance.save():
            return instance
        return None

    async def save(self) -> bool:
        """Save the state to the database. Returns database edit success as true/false"""

        try:
            async with db_session_context() as db_session:
                existing = (
                    await db_session.scalars(
                        select(InstanceStreet)
                        .filter(InstanceStreet.user_id == self.user_id)
                        .filter(InstanceStreet.tsid == self.tsid)
                    )
                ).one_or_none()
                if existing:
                    return False
                db_session.add(self)
                await db_session.commit()
                return True
        except Exception:
            LOGGER.exception(f"Could not save street instance <id={self.id}>")
            return False

    async def destroy(self) -> bool:
        """Delete a street instance. Will raise an error if the street is occupied.
        Returns database edit success as true/false
        """

        if self.occupants:
            raise RuntimeError("Cannot delete occupied street instance")

        try:
            async with db_session_context() as db_session:
                await db_session.delete(self)
                await db_session.commit()
                return True
        except Exception:
            LOGGER.exception(f"Could not delete street instance <id={self.id}>")


@home_street_blueprint.route("/homestreet/get/<string:username>")
async def get_for_player(username: str) -> str:
    try:
        async with db_session_context() as db_session:
            user_id = await User.get_id_from_username(username)
            home = (await db_session.scalars(select(InstanceStreet).filter(InstanceStreet.user_id == user_id))).first()
            if not home:
                return ""
            return home.tsid
    except Exception:
        LOGGER.exception("Failed")
        return ""


@home_street_blueprint.route("/homestreet/set/<string:username>/<string:tsid>")
async def set_for_player(username: str, tsid: str) -> bool:
    user_id = await User.get_id_from_username(username)
    return str((await InstanceStreet.create(tsid, user_id, f"{username}'s Home Street")) is not None).lower()
