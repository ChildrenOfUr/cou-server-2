from datetime import datetime, timedelta, timezone
from enum import Enum
import logging

from sqlalchemy.future import select
from sqlalchemy.sql import text

from cou_server.common.database import db_session_context, remote_engine
from cou_server.models.stat import Stat as StatModel
from cou_server.models.user import User


LOGGER = logging.getLogger(__name__)


Stat = Enum("Stat", [
    "awesome_pot_uses",
    "barnacles_scraped",
    "beaker_uses",
    "bean_trees_petted",
    "bean_trees_watered",
    "beans_harvested",
    "beans_seasoned",
    "blender_uses",
    "bubble_trees_petted",
    "bubble_trees_watered",
    "bubbles_harvested",
    "bubbles_transformed",
    "butterflies_massaged",
    "butterflies_milked",
    "cherries_harvested",
    "chickens_squeezed",
    "cocktail_shaker_uses",
    "crops_harvested",
    "crops_hoed",
    "crops_planted",
    "crops_watered",
    "cubimal_boxes_opened",
    "cubimals_set_free",
    "dirt_dug",
    "egg_plants_petted",
    "egg_plants_watered",
    "eggs_harveted",
    "eggs_seasoned",
    "emblems_caressed",
    "emblems_collected",
    "emblems_considered",
    "emblems_contemplated",
    "favor_earned",
    "fruit_converted",
    "fruit_trees_petted",
    "fruit_trees_watered",
    "frying_pan_uses",
    "gas_converted",
    "gas_harvested",
    "gas_plants_petted",
    "gas_plants_watered",
    "grapes_squished",
    "famous_pugilist_grill_uses",
    "heli_kitties_petted",
    "ice_scraped",
    "icons_collected",
    "icons_tithed",
    "icons_revered",
    "icons_ruminated",
    "items_dropped",
    "items_picked_up",
    "items_from_vendors",
    "jellisac_harvested",
    "jumps",
    "knife_board_uses",
    "paper_harvested",
    "peat_harvested",
    "piggies_nibbled",
    "piggies_petted",
    "piggies_fed",
    "planks_harvested",
    "quoins_collected",
    "rainbo_snocones_blended",
    "rocks_mined",
    "salmon_pocketed",
    "sauce_pan_uses",
    "shrine_donations",
    "smelter_uses",
    "spice_harvested",
    "spice_milled",
    "spice_plants_petted",
    "spice_plants_watered",
    "steps_taken",
    "test_tube_uses",
    "tinkertool_uses",
    "wood_trees_petted",
	"wood_trees_watered",
])


class BufferedStat:
    def __init__(self, stat: Stat, buffered_value: int, buffer_expirey: datetime = None) -> None:
        self.stat = stat
        self.buffered_value = buffered_value
        self.buffer_expirey = buffer_expirey
        if not self.buffer_expirey:
            self.buffer_expirey = datetime.now(timezone.utc) + timedelta(seconds=10)


class StatManager:
    stat_buffer: dict[str, BufferedStat] = {}

    @classmethod
    def _stat_to_string(cls, stat: Stat) -> str:
        return str(stat).split(".")[1]

    @classmethod
    def _string_to_stat(cls, stat: str) -> Stat:
        for value in Stat.__members__.values():
            if cls._stat_to_string(value) == stat:
                return value

        return None

    @classmethod
    async def get(cls, email: str, stat: Stat) -> int:
        """Returns the value of stat 'stat' for user with email 'email'"""

        return (await cls.get_all(email)).get(stat)

    @classmethod
    async def get_all(cls, email: str) -> dict[Stat, int]:
        """Returns the value of all stats for user with email 'email'"""

        try:
            async with db_session_context() as db_session:
                query = await db_session.scalars(select(StatModel).join(StatModel.user).filter(User.email == email))
                if not (stat_row := query.first()):
                    return {}
                # index by stat instead of string
                stat_keys: dict[Stat, int] = {}
                for stat_name in Stat.__members__.keys():
                    stat_keys[cls._string_to_stat(stat_name)] = getattr(stat_row, stat_name)
                return stat_keys
        except Exception:
            LOGGER.exception(f"Error reading stats for <email={email}>")
            return None

    @classmethod
    async def get_all_sums(cls) -> dict[str, int]:
        """Returns all combined stats from all players"""
        try:
            sums: list[str] = []
            for stat_name in Stat.__members__.keys():
                sums.append(f"SUM({stat_name}) AS {stat_name}")

            query = text(f"SELECT {', '.join(sums)} FROM stats")
            async with remote_engine.connect() as conn:
                return dict((await conn.execute(query)).mappings().first())
        except Exception:
            LOGGER.exception(f"Error summing stats")
            return {}

    @classmethod
    async def get_sum(cls, stat: Stat) -> float:
        """Returns a combined stat from all players"""
        stat_name = cls._stat_to_string(stat)
        try:
            query = text(f"SELECT SUM({stat_name}) AS {stat_name} FROM stats")
            async with remote_engine.connect() as conn:
                return (await conn.execute(query)).mappings.first()[stat_name]
        except Exception:
            LOGGER.exception(f"Error summing stat {stat_name}")
            return 0

    @classmethod
    async def add(cls, email: str, stat: Stat, increment: int = 1, buffer: bool = False) -> int:
        """Increments stat 'stat' for user with email 'email' by 'increment' (defaults to 1)
        Returns the new value of 'stat' for the user
        """

        if not buffer:
            return await cls._flush_stat_to_db(stat, increment, email)

        buffered_stat = cls.stat_buffer.get(email, BufferedStat(stat, increment))
        if buffered_stat.buffer_expirey < datetime.now(timezone.utc):
            ret = await cls._flush_stat_to_db(buffered_stat.stat, buffered_stat.buffered_value, email)
            if ret is not None:
                cls.stat_buffer.pop(email, None)
            return ret
        buffered_stat.buffered_value += increment
        cls.stat_buffer[email] = buffered_stat
        return buffered_stat.buffered_value

    @classmethod
    async def _flush_stat_to_db(cls, stat: Stat, increment: int, email: str) -> int:
        stat_name = cls._stat_to_string(stat)
        try:
            async with db_session_context() as db_session:
                user_stats = (await db_session.scalars(select(StatModel).join(StatModel.user).filter(User.email == email))).first()
                if user_stats is None:
                    db_session.add(StatModel(user_id=await User.get_id_from_email(email), **{stat_name: increment}))
                    return increment
                else:
                    setattr(user_stats, stat_name, getattr(user_stats, stat_name) + increment)
                    return getattr(user_stats, stat_name)
        except Exception:
            LOGGER.exception(f"Error writing stat {stat_name}")
            return None
