import dataclasses
from importlib import resources
import json

from sqlalchemy.sql import text

from cou_server.common import get_logger
from cou_server.common.database import remote_engine
from cou_server.common.global_gobbler import cou_globals
from cou_server.common.serializable import serializable


_ACHIEVEMENTS: dict[str, "Achievement"] = {}
LOGGER = get_logger(__name__)

class AchievementAward:
    def __init__(self, email: str, achieve_map: dict) -> None:
        self.email = email
        self.achieve_map = achieve_map


@serializable
@dataclasses.dataclass
class Achievement:
    id: str
    name: str
    description: str
    category: str
    image_url: str

    @classmethod
    def load(cls) -> int:
        for achievement_file in [
            i for i in resources.files("data.achievements").iterdir() if str(i).endswith(".json")
        ]:
            with resources.files("data.achievements").joinpath(achievement_file).open() as achievement_file:
                for id, data in json.loads(achievement_file.read()).items():
                    achievement = Achievement(
                        id=id, name=data["name"], description=data["description"],
                        category=data["category"], image_url=data["imageUrl"],
                    )
                    _ACHIEVEMENTS[id] = achievement
        LOGGER.debug(f"[Achievement] Loaded {len(_ACHIEVEMENTS)} achievements")
        return len(_ACHIEVEMENTS)

    @classmethod
    def find(cls, id: str):
        return _ACHIEVEMENTS.get(id)

    @property
    def is_set_up(self) -> bool:
        return (
            self.id is not None and
            self.name is not None and
            self.description is not None and
            self.category is not None and
            self.image_url is not None
        )

    async def awarded_to(self, email: str) -> bool:
        if not self.is_set_up:
            return False

        try:
            async with remote_engine.connect() as conn:
                query = text("SELECT achievements FROM users WHERE email = :email")
                return self.id in json.loads((await conn.execute(query, parameters={"email": email})).mappings().first()["achievements"])
        except Exception:
            LOGGER.exception(f"Error getting achievements for <email={email}>")
            return False

    async def award_to(self, email: str) -> bool:
        if not self.is_set_up or await self.awarded_to(email):
            return False

        result = False
        try:
            async with remote_engine.connect() as conn:
                query = text("SELECT achievements FROM users WHERE email = :email")
                existing_achievements = json.loads((await conn.execute(query, parameters={"email": email})).mappings().first()["achievements"])
                existing_achievements.append(self.id)

                update = text("UPDATE users SET achievements = :json WHERE email = :email")
                result = await conn.execute(update, parameters={"email": email, "json": json.dumps(existing_achievements)})
                if result.rowcount == 1:
                    # send to client
                    if socket := cou_globals.user_sockets.get(email):
                        await socket.send(json.dumps({
                            "achv_id": self.id,
                            "achv_name": self.name,
                            "achv_description": self.description,
                            "achv_imageUrl": self.image_url,
                        }))
                    result = True
                else:
                    result = False
                await conn.commit()
        except Exception:
            LOGGER.exception(f"Error setting achievements for <email={email}>")
            result = False

        return result
