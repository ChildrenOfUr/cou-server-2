import logging

from cou_server.achievements.achievements import Achievement
from cou_server.endpoints import mapdata


LOGGER = logging.getLogger(__name__)


class AchievementCheckers:
    @classmethod
    def get_completist_id_for_hub(cls, hub_id: str) -> Achievement:
        try:
            hub_name = mapdata.hubs()[hub_id]["name"]
            hub_name = hub_name.lower().replace(" ", "_")
            return Achievement.find(f"{hub_name}_completist")
        except Exception:
            LOGGER.exception(f"Failed getting completist achv id for <hubId={hub_id}>")
            return Achievement()

    @classmethod
    def hub_completion(cls, location_history: list[str], email: str, added_tsid: str) -> bool:
        def _check_streets_in_hub(hub_id: str) -> bool:
            for data in mapdata.get_streets_in_hub(hub_id):
                if data.get("tsid") is None:
                    LOGGER.warning(f"Missing TSID for {data}")
                    continue
                if not data.get("in_game", True):
                    # not possible to visit
                    continue
                if mapdata.tsid_l(data["tsid"]) not in location_history:
                    # not visited
                    return False

            # visited every street in hub
            return True

        try:
            added_tsid_hub_id = mapdata.get_street_by_tsid(added_tsid).get("hub_id")
            if added_tsid_hub_id is None:
                raise ValueError("added_tsid_hub_id may not be None")
        except Exception:
            LOGGER.warning(f"Cannot find hub id for {added_tsid}")
            return False

        if _check_streets_in_hub(added_tsid_hub_id):
            try:
                cls.get_completist_id_for_hub(added_tsid_hub_id).award_to(email)
                return True
            except Exception:
                LOGGER.warning(f"Awarding completist <added_tsid_hub_id={added_tsid_hub_id}> to <email={email}> failed")
                return False
        return False
