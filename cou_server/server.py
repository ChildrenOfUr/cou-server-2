from datetime import datetime, timezone
import logging
import signal

from quart import Quart
from quart_cors import cors

from cou_server.achievements.achievements import Achievement
from cou_server.buffs.buff_manager import BuffManager
from cou_server.common import get_logger
from cou_server.common.console import Console
from cou_server.common.constants import const_blueprint
from cou_server.common.database import db_create
from cou_server.common.file_cache import FileCache
from cou_server.common.message_bus import MessageBus
from cou_server.endpoints.achievements import achievements_endpoint
from cou_server.endpoints.buffs import buffs_endpoint
from cou_server.endpoints.chat import chat_blueprint, listen_to_discord
from cou_server.endpoints.entities import entity_blueprint
from cou_server.endpoints.evelavtion import elevation_endpoint
from cou_server.endpoints.friends import friends_blueprint
from cou_server.endpoints.images import images_blueprint
from cou_server.endpoints.inventory import inventory_endpoint
from cou_server.endpoints.items import items_endpoint, load_items, load_consume_values
from cou_server.endpoints.letters import letters_blueprint
from cou_server.endpoints.mapdata import map_blueprint, read_map_data
from cou_server.endpoints.metabolics import metabolics_endpoint, simulate_timer, energy_timer, mood_timer, track_new_days
from cou_server.endpoints.player_update import player_blueprint
from cou_server.endpoints.quest import quest_blueprint, load_quests
from cou_server.endpoints.stats import stats_endpoint
from cou_server.endpoints.status import ServerStatus, status_blueprint
from cou_server.endpoints.street_update import street_blueprint, simulate_streets, update_npcs
from cou_server.endpoints.user import users_blueprint
from cou_server.endpoints.usernamecolors import usernamecolors_blueprint
from cou_server.endpoints.weather import weather_blueprint, simulate_weather
from cou_server.entities.items.actions.note import notes_blueprint
from cou_server.entities.npcs.vendors.vendor import Vendor
from cou_server.skills.skill_manager import skills_blueprint, load_skills
from cou_server.streets.instancing import home_street_blueprint


logger = get_logger(__name__)

app = Quart(__name__)
cors(app)
app.register_blueprint(achievements_endpoint)
app.register_blueprint(buffs_endpoint)
app.register_blueprint(chat_blueprint)
app.register_blueprint(const_blueprint)
app.register_blueprint(entity_blueprint)
app.register_blueprint(elevation_endpoint)
app.register_blueprint(friends_blueprint)
app.register_blueprint(home_street_blueprint)
app.register_blueprint(images_blueprint)
app.register_blueprint(inventory_endpoint)
app.register_blueprint(items_endpoint)
app.register_blueprint(letters_blueprint)
app.register_blueprint(map_blueprint)
app.register_blueprint(metabolics_endpoint)
app.register_blueprint(notes_blueprint)
app.register_blueprint(player_blueprint)
app.register_blueprint(quest_blueprint)
app.register_blueprint(skills_blueprint)
app.register_blueprint(stats_endpoint)
app.register_blueprint(status_blueprint)
app.register_blueprint(street_blueprint)
app.register_blueprint(users_blueprint)
app.register_blueprint(usernamecolors_blueprint)
app.register_blueprint(weather_blueprint)


def start_background_tasks():
    app.add_background_task(simulate_streets)
    app.add_background_task(update_npcs)
    app.add_background_task(simulate_timer)
    app.add_background_task(energy_timer)
    app.add_background_task(mood_timer)
    app.add_background_task(simulate_weather)
    app.add_background_task(listen_to_discord)


@app.before_serving
async def startup():
    await db_create()

    # Keep track of when the server was started
    ServerStatus.server_start = datetime.now(timezone.utc)

    await read_map_data()
    load_quests()
    load_items()
    load_consume_values()
    FileCache.load_caches()
    load_skills()
    Achievement.load()
    Vendor.load_vendor_types()
    track_new_days()
    BuffManager.load_buffs()
    start_background_tasks()
    console = Console()

    # need to listen for ctrl+c and close all websockets otherwise
    # the server is getting stuck shutting down forever
    def handler(sig, frame):
        MessageBus.publish("server_shutdown")
    signal.signal(signal.SIGINT, handler)


def main():
    logging.getLogger("hypercorn.access").disabled = True
    server_port = 8000
    logger.info(f"Game server on port {server_port}")
    app.run(port=server_port, debug=True)


if __name__ == "__main__":
    main()
