#! /bin/bash

# Exit at first error
set -e

# Prerequisites:
# - sudo apt install tmux
# - sudo apt install python3 python3-pip pipenv

SESSION_NAME="server2"
START_CMD="PYTHONPATH=. pipenv run hypercorn --bind 0.0.0.0:8000 --certfile certs/fullchain.pem --keyfile certs/privkey.pem cou_server/server.py:app"

echo "Installing dependencies..."
pipenv install
echo "...done installing dependencies."

echo "Killing old server process (if any)..."
tmux kill-session -t $SESSION_NAME || true
echo "...done killing old server process (if any)."

echo "Starting server..."
pipenv run tmuxp load -d tmuxp-session.yaml
echo "...done starting server."
